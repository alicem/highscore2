// This file is part of Highscore. License: GPL-3.0-or-later

[DBus (name = "org.gnome.World.Highscore2.Runner")]
public interface Highscore.RunnerProxy : Object {
    public signal void redraw ();

    public abstract async void load_rom (string rom_path, string save_path) throws Error;
    public abstract async void start () throws Error;
    public abstract async void stop () throws Error;
    public abstract async void reset () throws Error;

    public abstract async void pause () throws Error;
    public abstract async void resume () throws Error;

    public abstract async void reload_save (string path) throws Error;
    public abstract async void sync_save () throws Error;
    public abstract async void save_state (string path) throws Error;
    public abstract async void load_state (string path) throws Error;

    public abstract async UnixInputStream get_input_buffer () throws Error;
    public abstract async UnixInputStream get_video_buffer () throws Error;
}
