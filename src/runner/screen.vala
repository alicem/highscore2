// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Screen : Object {
    public int id { get; construct; }
    public Graphene.Rect? area { get; construct; }

    public Screen (int id, Graphene.Rect? area) {
        Object (id: id, area: area);
    }
}
