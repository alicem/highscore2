// This file is part of Highscore. License: GPL-3.0+.

public abstract class Highscore.Control : Object {
    public enum ControlType {
        BUTTON,
        DPAD,
        ANALOG,
    }

    public string id { get; construct; }
    public string name { get; construct; }

    public abstract ControlType get_control_type ();
}
