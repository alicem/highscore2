// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.PlayerHandler {
    private unowned InputHandler input_handler;
    private unowned Runner runner;
    private PlatformControls controls;
    private uint player;
    private string controller_type;

    private bool enable_keyboard;
    private Gamepad gamepad;

    private class ButtonData {
        public ControlHandler? handler;
        public HashTable<Gamepad.Button, ControlHandler>? modifiers;
    }

    private class AnalogData {
        public ControlHandler? handler;
        public ControlHandler? direction_handlers[InputDirection.N_DIRECTIONS];

        public string to_string () {
            if (handler != null)
                return handler.get_id ();

            string ids[InputDirection.N_DIRECTIONS];

            for (int i = 0; i < InputDirection.N_DIRECTIONS; i++) {
                var handler = direction_handlers[i];

                if (handler != null)
                    ids[i] = direction_handlers[i].get_id ();
                else
                    ids[i] = "";
            }

            return string.joinv ("|", ids);
        }
    }

    private HashTable<Control, ControlHandler> control_handlers;
    private HashTable<int, ControlHandler> key_handlers;
    private HashTable<Gamepad.Button, ButtonData> button_data;
    private Gee.HashMultiMap<Gamepad.Button, Gamepad.Button> modifier_buttons;
    private HashTable<Gamepad.Analog, AnalogData> analog_data;

    public PlayerHandler (
        InputHandler input_handler,
        PlatformControls controls,
        string controller_type,
        uint player,
        bool enable_keyboard,
        Gamepad? gamepad
    ) {
        this.input_handler = input_handler;
        this.runner = input_handler.runner;
        this.controls = controls;
        this.player = player;
        this.controller_type = controller_type;

        this.enable_keyboard = enable_keyboard;
        this.gamepad = gamepad;

        control_handlers = new HashTable<Control, ControlHandler> (direct_hash, direct_equal);
        key_handlers = new HashTable<int, ControlHandler> (direct_hash, direct_equal);
        button_data = new HashTable<Gamepad.Button, ButtonData> (direct_hash, direct_equal);
        modifier_buttons = new Gee.HashMultiMap<Gamepad.Button, Gamepad.Button> ();
        analog_data = new HashTable<Gamepad.Analog, AnalogData> (direct_hash, direct_equal);

        var controller = controls.get_controller (controller_type);

        if (controller != null) {
            var keyboard_mapping = controller.default_keyboard_mapping;
            if (keyboard_mapping != null) {
                try {
                    load_keyboard_mapping (controller, keyboard_mapping);
                } catch (MappingError e) {
                    error ("Failed to load default keyboard mapping: %s", e.message);
                }
            }

            var gamepad_mapping = controller.default_gamepad_mapping;
            if (gamepad_mapping != null) {
                try {
                    load_gamepad_mapping (controller, gamepad_mapping);
                } catch (MappingError e) {
                    error ("Failed to load default gamepad mapping: %s", e.message);
                }
            }
        }

        controller = controls.global_controller;

        if (controller != null) {
            var keyboard_mapping = controller.default_keyboard_mapping;
            if (keyboard_mapping != null) {
                try {
                    load_keyboard_mapping (controller, keyboard_mapping);
                } catch (MappingError e) {
                    error ("Failed to load default keyboard mapping: %s", e.message);
                }
            }

            var gamepad_mapping = controller.default_gamepad_mapping;
            if (gamepad_mapping != null) {
                try {
                    load_gamepad_mapping (controller, gamepad_mapping);
                } catch (MappingError e) {
                    error ("Failed to load default gamepad mapping: %s", e.message);
                }
            }
        }
    }

    private ControlHandler ensure_handler (Control control) {
        if (!(control in control_handlers)) {
            var handler = ControlHandler.create (runner, player, control);

            control_handlers[control] = handler;
        }

        return control_handlers[control];
    }

    private void load_keyboard_mapping (
        PlatformController controller,
        PlatformKeyboardMapping mapping
    ) throws MappingError {
        if (!enable_keyboard)
            return;

        foreach (var key in mapping.list_key_mappings ()) {
            var parts = key.control.split (":", 2);

            if (key.keycode in key_handlers) {
                var control1 = key_handlers[key.keycode].get_id ();
                var control2 = parts[0];
                throw new MappingError.DUPLICATE_MAPPING (
                    "Key %d cannot be mapped to both '%s' and '%s'",
                    key.keycode, control1, control2
                );
            }

            var control = controller.get_control (parts[0]);
            var qualifier = parts.length > 1 ? parts[1] : null;

            if (control == null) {
                throw new MappingError.INVALID_CONTROL (
                    "No such control: '%s'", key.control
                );
            }

            var handler = ensure_handler (control);
            if (!handler.add_key (key.keycode, qualifier)) {
                if (qualifier == null) {
                    throw new MappingError.INVALID_QUALIFIER (
                        "'%s' needs a qualifier", handler.get_id ()
                    );
                } else {
                    throw new MappingError.INVALID_QUALIFIER (
                        "Qualifier '%s' is not valid for '%s'",
                        qualifier, handler.get_id ()
                    );
                }
            }

            key_handlers[key.keycode] = handler;
        }
    }

    private void load_gamepad_mapping (
        PlatformController controller,
        PlatformGamepadMapping mapping
    ) throws MappingError {
        if (gamepad == null)
            return;

        foreach (var button in mapping.list_button_mappings ())
            load_button_mapping (controller, button.pair, button.control);

        foreach (var analog in mapping.list_analog_mappings ()) {
            if (analog.analog in analog_data) {
                var analog_id = analog.analog.get_id ();
                var control1 = analog_data[analog.analog].to_string ();
                var control2 = analog.control;
                throw new MappingError.DUPLICATE_MAPPING (
                    "Analog '%s' cannot be mapped to both '%s' and '%s'",
                    analog_id, control1, control2
                );
            }

            var mapping_parts = analog.control.split ("|");

            if (mapping_parts.length != 1 && mapping_parts.length != InputDirection.N_DIRECTIONS) {
                var analog_id = analog.analog.get_id ();
                throw new MappingError.INVALID_MAPPING (
                    "Analog %s can only be mapped to a single analog, single d-pad or %d buttons",
                    analog_id, InputDirection.N_DIRECTIONS
                );
            }

            if (mapping_parts.length == 1)
                load_analog_to_single_control_mapping (controller, analog.analog, mapping_parts[0]);
            else
                load_analog_to_buttons_mapping (controller, analog.analog, mapping_parts);
        }
    }

    private void load_button_mapping (
        PlatformController controller,
        GamepadButtonPair pair,
        string dest
    ) throws MappingError {
        var parts = dest.split (":", 2);

        var control = controller.get_control (parts[0]);
        var qualifier = parts.length > 1 ? parts[1] : null;

        if (control == null) {
            throw new MappingError.INVALID_CONTROL (
                "No such control: '%s'", parts[0]
            );
        }

        var button = pair.button;
        var modifier = pair.modifier;

        var handler = ensure_handler (control);
        if (!handler.add_button (pair, qualifier)) {
            if (qualifier == null) {
                throw new MappingError.INVALID_QUALIFIER (
                    "'%s' needs a qualifier", handler.get_id ()
                );
            } else {
                throw new MappingError.INVALID_QUALIFIER (
                    "Qualifier '%s' is not valid for '%s'",
                    qualifier, handler.get_id ()
                );
            }
        }

        if (button in button_data) {
            var data = button_data[button];

            if (modifier == null && data.handler != null) {
                var button_id = button.get_id ();
                var control1 = data.handler.get_id ();
                var control2 = handler.get_id ();
                throw new MappingError.DUPLICATE_MAPPING (
                    "Button '%s' cannot be mapped to both '%s' and '%s'",
                    button_id, control1, control2
                );
            } else if (data.modifiers != null && modifier in data.modifiers) {
                var button_id = button.get_id ();
                var modifier_id = modifier.get_id ();
                var control1 = data.handler.get_id ();
                var control2 = handler.get_id ();
                throw new MappingError.DUPLICATE_MAPPING (
                    "Combo '%s' + '%s' cannot be mapped to both '%s' and '%s'",
                    modifier_id, button_id, control1, control2
                );
            }
        } else {
            button_data[button] = new ButtonData ();
        }

        if (modifier != null && modifier in button_data) {
            var modifier_id = modifier.get_id ();
            throw new MappingError.INVALID_MAPPING (
                "Button '%s' is already in use and cannot be a modifier",
                modifier_id
            );
        }

        if (button in modifier_buttons) {
            var button_id = button.get_id ();
            throw new MappingError.INVALID_MAPPING (
                "Button '%s' is already used as a modifier", button_id
            );
        }

        var data = button_data[button];

        if (modifier == null) {
            data.handler = handler;
        } else {
            if (data.modifiers == null) {
                data.modifiers = new HashTable<Gamepad.Button, ControlHandler> (
                    direct_hash, direct_equal
                );
            }
            data.modifiers[modifier] = handler;

            modifier_buttons[modifier] = button;
        }
    }

    private void load_analog_to_single_control_mapping (
        PlatformController controller,
        Gamepad.Analog analog,
        string dest
    ) throws MappingError {
        var parts = dest.split (":", 2);

        var control = controller.get_control (parts[0]);
        var qualifier = parts.length > 1 ? parts[1] : null;

        if (control == null) {
            throw new MappingError.INVALID_CONTROL (
                "No such control: '%s'", parts[0]
            );
        }

        if (control.get_control_type () == BUTTON) {
            var analog_id = analog.get_id ();
            throw new MappingError.INVALID_MAPPING (
                "Analog %s cannot be mapped to a single button", analog_id
            );
        }

        var handler = ensure_handler (control);
        if (!handler.add_analog (analog, null, qualifier)) {
            if (qualifier == null) {
                throw new MappingError.INVALID_QUALIFIER (
                    "'%s' needs a qualifier", handler.get_id ()
                );
            } else {
                throw new MappingError.INVALID_QUALIFIER (
                    "Qualifier '%s' is not valid for '%s'",
                    qualifier, handler.get_id ()
                );
            }
        }

        AnalogData data = new AnalogData ();
        data.handler = handler;

        analog_data[analog] = data;
    }

    private void load_analog_to_buttons_mapping (
        PlatformController controller,
        Gamepad.Analog analog,
        string[] dest
    ) throws MappingError {
        assert (dest.length == InputDirection.N_DIRECTIONS);

        AnalogData data = new AnalogData ();

        for (int i = 0; i < InputDirection.N_DIRECTIONS; i++) {
            if (dest[i] == "") {
                data.direction_handlers[i] = null;
                continue;
            }

            var parts = dest[i].split (":", 2);

            var control = controller.get_control (parts[0]);
            var qualifier = parts.length > 1 ? parts[1] : null;

            if (control == null) {
                throw new MappingError.INVALID_CONTROL (
                    "No such control: '%s'", parts[0]
                );
            }

            var handler = ensure_handler (control);
            if (!handler.add_analog (analog, i, qualifier)) {
                if (qualifier == null) {
                    throw new MappingError.INVALID_QUALIFIER (
                        "'%s' needs a qualifier", handler.get_id ()
                    );
                } else {
                    throw new MappingError.INVALID_QUALIFIER (
                        "Qualifier '%s' is not valid for '%s'",
                        qualifier, handler.get_id ()
                    );
                }
            }

            data.direction_handlers[i] = handler;
        }

        analog_data[analog] = data;
    }

    public void button_pressed (Gamepad.Button button) {
        if (button in modifier_buttons) {
            modifier_pressed (button);
            return;
        }

        var data = button_data[button];

        if (data == null)
            return;

        int n_modifiers_pressed = 0;

        if (data.modifiers != null) {
            foreach (var key in data.modifiers.get_keys ()) {
                if (gamepad.get_button_pressed (key))
                    n_modifiers_pressed++;
            }

            foreach (var key in data.modifiers.get_keys ()) {
                var handler = data.modifiers[key];

                if (n_modifiers_pressed == 1 && gamepad.get_button_pressed (key))
                    handler.button_pressed (new GamepadButtonPair (button, key));
                else
                    handler.button_released (new GamepadButtonPair (button, key));
            }
        }

        if (data.handler != null && n_modifiers_pressed == 0)
            data.handler.button_pressed (new GamepadButtonPair (button, null));
    }

    public void button_released (Gamepad.Button button) {
        if (button in modifier_buttons) {
            modifier_released (button);
            return;
        }

        var data = button_data[button];

        if (data == null)
            return;

        if (data.handler != null)
            data.handler.button_released (new GamepadButtonPair (button, null));

        if (data.modifiers != null) {
            foreach (var key in data.modifiers.get_keys ()) {
                var handler = data.modifiers[key];

                if (gamepad.get_button_pressed (key))
                    handler.button_released (new GamepadButtonPair (button, key));
            }
        }
    }

    private void modifier_pressed (Gamepad.Button modifier) {
        foreach (var button in modifier_buttons[modifier]) {
            if (!gamepad.get_button_pressed (button))
                continue;

            var data = button_data[button];

            assert (data.modifiers != null);
            assert (modifier in data.modifiers);

            if (data.handler != null)
                data.handler.button_released (new GamepadButtonPair (button, null));

            int n_pressed = 0;
            foreach (var key in data.modifiers.get_keys ()) {
                if (gamepad.get_button_pressed (key))
                    n_pressed++;
            }

            foreach (var key in data.modifiers.get_keys ()) {
                var handler = data.modifiers[key];

                if (n_pressed == 1 && gamepad.get_button_pressed (key))
                    handler.button_pressed (new GamepadButtonPair (button, key));
                else
                    handler.button_released (new GamepadButtonPair (button, key));
            }
        }
    }

    private void modifier_released (Gamepad.Button modifier) {
        foreach (var button in modifier_buttons[modifier]) {
            if (!gamepad.get_button_pressed (button))
                continue;

            var data = button_data[button];

            assert (data.modifiers != null);
            assert (modifier in data.modifiers);

            int n_pressed = 0;
            foreach (var key in data.modifiers.get_keys ()) {
                if (gamepad.get_button_pressed (key))
                    n_pressed++;
            }

            foreach (var key in data.modifiers.get_keys ()) {
                var handler = data.modifiers[key];

                if (n_pressed == 1 && gamepad.get_button_pressed (key))
                    handler.button_pressed (new GamepadButtonPair (button, key));
                else
                    handler.button_released (new GamepadButtonPair (button, key));
            }

            if (data.handler != null && n_pressed == 0)
                data.handler.button_pressed (new GamepadButtonPair (button, null));
        }
    }

    public void analog_moved (Gamepad.Analog analog, double x, double y) {
        var data = analog_data[analog];

        if (data == null)
            return;

        if (data.handler != null) {
            data.handler.analog_moved (analog, x, y);
            return;
        }

        for (int i = 0; i < InputDirection.N_DIRECTIONS; i++) {
            if (data.direction_handlers[i] == null)
                continue;

            data.direction_handlers[i].analog_moved (analog, x, y);
        }
    }

    public bool key_pressed (int keycode) {
        var handler = key_handlers[keycode];

        if (handler != null) {
            handler.key_pressed (keycode);
            return Gdk.EVENT_STOP;
        }

        return Gdk.EVENT_PROPAGATE;
    }

    public void key_released (int keycode) {
        var handler = key_handlers[keycode];

        if (handler != null)
            handler.key_released (keycode);
    }

    private void ensure_state (bool active) {
        if (enable_keyboard) {
            foreach (int keycode in key_handlers.get_keys ()) {
                var handler = key_handlers[keycode];

                if (!input_handler.get_key_pressed (keycode))
                    continue;

                if (active)
                    handler.key_pressed (keycode);
                else
                    handler.key_released (keycode);
            }
        }

        if (gamepad != null) {
            foreach (var button in button_data.get_keys ()) {
                var data = button_data[button];

                if (!gamepad.get_button_pressed (button))
                    continue;

                if (data.handler != null) {
                    var pair = new GamepadButtonPair (button, null);

                    if (active)
                        data.handler.button_pressed (pair);
                    else
                        data.handler.button_released (pair);
                }

                if (data.modifiers != null) {
                    foreach (var key in data.modifiers.get_keys ()) {
                        var handler = data.modifiers[key];
                        var pair = new GamepadButtonPair (button, key);

                        if (active)
                            handler.button_pressed (pair);
                        else
                            handler.button_released (pair);
                    }
                }
            }

            foreach (var analog in analog_data.get_keys ()) {
                var data = analog_data[analog];

                if (data.handler != null)
                    data.handler.analog_moved (analog, 0, 0);

                if (data.direction_handlers != null) {
                    for (int i = 0; i < InputDirection.N_DIRECTIONS; i++) {
                        var handler = data.direction_handlers[i];

                        if (handler == null)
                            continue;

                        double x = active ? gamepad.get_analog_x (analog) : 0;
                        double y = active ? gamepad.get_analog_y (analog) : 0;

                        handler.analog_moved (analog, x, y);
                    }
                }
            }
        }
    }

    public void ensure_released () {
        ensure_state (false);
    }

    public void ensure_pressed () {
        ensure_state (true);
    }

    public string get_controller_type () {
        return controller_type;
    }
}
