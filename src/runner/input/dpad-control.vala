// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.DpadControl : Control {
    public signal void activate (Runner runner, uint player, InputDirection direction, bool pressed);

    public DpadControl (string id, string name) {
        Object (id: id, name: name);
    }

    public override ControlType get_control_type () {
        return DPAD;
    }
}
