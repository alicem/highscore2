// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.PlatformGamepadMapping : Object {
    public struct Button {
        GamepadButtonPair pair;
        string control;
    }

    public struct Analog {
        Gamepad.Analog analog;
        string control;
    }

    private Button[] buttons;
    private Analog[] analogs;

    construct {
        buttons = {};
        analogs = {};
    }

    public void map_button (Gamepad.Button button, string control) {
        Button mapping = { new GamepadButtonPair (button, null), control };

        buttons += mapping;
    }

    public void map_button_with_modifier (
        Gamepad.Button modifier,
        Gamepad.Button button,
        string control
    ) {
        Button mapping = { new GamepadButtonPair (button, modifier), control };

        buttons += mapping;
    }

    public void map_analog (Gamepad.Analog analog, string control) {
        Analog mapping = { analog, control };

        analogs += mapping;
    }

    public void map_analog_to_buttons (
        Gamepad.Analog analog,
        string? up,
        string? down,
        string? left,
        string? right
    ) {
        string dest = "%s|%s|%s|%s".printf (
            up ?? "",
            down ?? "",
            left ?? "",
            right ?? ""
        );

        Analog mapping = { analog, dest };

        analogs += mapping;
    }

    public Button[] list_button_mappings () {
        return buttons;
    }

    public Analog[] list_analog_mappings () {
        return analogs;
    }
}
