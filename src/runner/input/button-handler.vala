// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.ButtonHandler : ControlHandler {
    private const double ANALOG_DEADZONE = 0.5;
    private const double DIAGONAL_AREA = Math.PI / 6;

    private struct AnalogData {
        bool allowed_directions[InputDirection.N_DIRECTIONS];
        double x;
        double y;
        bool active;

        public bool is_direction_allowed (InputDirection? direction) {
            if (direction == null)
                return false;

            return allowed_directions[(InputDirection) direction];
        }
    }

    private Gee.HashSet<int>? pressed_keys;
    private Gee.HashSet<GamepadButtonPair>? pressed_buttons;
    private AnalogData? analogs[Gamepad.N_ANALOGS];

    public ButtonHandler (Runner runner, uint player, Control control) {
        this.runner = runner;
        this.player = player;
        this.control = control;
    }

    public override bool add_key (int keycode, string? qualifier) {
        if (qualifier != null)
            return false;

        if (pressed_keys == null)
            pressed_keys = new Gee.HashSet<int> ();

        return true;
    }

    public override bool add_button (GamepadButtonPair pair, string? qualifier) {
        if (qualifier != null)
            return false;

        if (pressed_buttons == null) {
            pressed_buttons = new Gee.HashSet<GamepadButtonPair> (
                GamepadButtonPair.hash, GamepadButtonPair.equal
            );
        }

        return true;
    }

    public override bool add_analog (Gamepad.Analog analog, InputDirection? direction, string? qualifier) {
        assert (direction != null);

        if (analogs[analog] == null)
            analogs[analog] = {};

        InputDirection dir = direction;
        analogs[analog].allowed_directions[dir] = true;

        return true;
    }

    public override void key_pressed (int keycode) {
        if (pressed_keys == null)
            return;

        bool was_active = is_active ();

        pressed_keys.add (keycode);

        if (!was_active)
            notify_control ();
    }

    public override void key_released (int keycode) {
        if (pressed_keys == null)
            return;

        pressed_keys.remove (keycode);

        if (!is_active ())
            notify_control ();
    }

    public override void button_pressed (GamepadButtonPair pair) {
        if (pressed_buttons == null)
            return;

        bool was_active = is_active ();

        pressed_buttons.add (pair);

        if (!was_active)
            notify_control ();
    }

    public override void button_released (GamepadButtonPair pair) {
        if (pressed_buttons == null)
            return;

        pressed_buttons.remove (pair);

        if (!is_active ())
            notify_control ();
    }

    public override void analog_moved (Gamepad.Analog analog, double x, double y) {
        if (analogs[analog] == null)
            return;

        analogs[analog].x = x;
        analogs[analog].y = y;

        double length = Math.sqrt (x * x + y * y);
        double angle = Math.atan2 (y, x);

        bool was_active = analogs[analog].active;
        bool active = false;

        if (length > ANALOG_DEADZONE) {
            for (int i = 0; i < InputDirection.N_DIRECTIONS; i++) {
                if (AnalogUtils.is_pointing_at_direction (angle, DIAGONAL_AREA, i) &&
                    analogs[analog].is_direction_allowed (i)) {
                    active = true;
                    break;
                }
            }
        }

        if (was_active == active)
            return;

        analogs[analog].active = active;

        notify_control ();
    }

    private bool is_active () {
        for (int i = 0; i < Gamepad.N_ANALOGS; i++) {
            if (analogs[i] == null)
                continue;

            if (analogs[i].active)
                return true;
        }

        return (pressed_keys != null && pressed_keys.size > 0) ||
               (pressed_buttons != null && pressed_buttons.size > 0);
    }

    private void notify_control () {
        var btn = control as ButtonControl;
        btn.activate (runner, player, is_active ());
    }
}
