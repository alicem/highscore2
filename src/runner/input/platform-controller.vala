// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.PlatformController : Object {
    public string id { get; construct; }
    public string name { get; construct; }
    public PlatformKeyboardMapping default_keyboard_mapping { get; set; }
    public PlatformGamepadMapping default_gamepad_mapping { get; set; }

    private HashTable<string, Control> controls;

    public PlatformController (string id, string name) {
        Object (id: id, name: name);

        controls = new HashTable<string, Control> (str_hash, str_equal);
    }

    public Control? get_control (string id) {
        return controls[id];
    }

    public void add_control (Control control) requires (!(control.id in controls)) {
        controls[control.id] = control;
    }
}
