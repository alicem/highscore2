// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.DpadHandler : ControlHandler {
    private const double ANALOG_DEADZONE = 0.5;
    private const double DIAGONAL_AREA = Math.PI / 4;

    private struct AnalogData {
        bool active_directions[InputDirection.N_DIRECTIONS];
    }

    private Gee.HashSet<int>? up_keys;
    private Gee.HashSet<int>? down_keys;
    private Gee.HashSet<int>? left_keys;
    private Gee.HashSet<int>? right_keys;
    private HashTable<int, InputDirection> key_directions;

    private Gee.HashSet<GamepadButtonPair>? up_buttons;
    private Gee.HashSet<GamepadButtonPair>? down_buttons;
    private Gee.HashSet<GamepadButtonPair>? left_buttons;
    private Gee.HashSet<GamepadButtonPair>? right_buttons;
    private HashTable<GamepadButtonPair, InputDirection> button_directions;
    private AnalogData? analogs[Gamepad.N_ANALOGS];

    public DpadHandler (Runner runner, uint player, Control control) {
        this.runner = runner;
        this.player = player;
        this.control = control;
    }

    private Gee.HashSet<int>? get_keys_for_direction (InputDirection direction) {
        switch (direction) {
            case UP:
                return up_keys;
            case DOWN:
                return down_keys;
            case LEFT:
                return left_keys;
            case RIGHT:
                return right_keys;
            default:
                assert_not_reached ();
        }
    }

    private Gee.HashSet<GamepadButtonPair>? get_buttons_for_direction (InputDirection direction) {
        switch (direction) {
            case UP:
                return up_buttons;
            case DOWN:
                return down_buttons;
            case LEFT:
                return left_buttons;
            case RIGHT:
                return right_buttons;
            default:
                assert_not_reached ();
        }
    }

    public override bool add_key (int keycode, string? qualifier) {
        var direction = InputDirection.from_string (qualifier);
        if (direction == null)
            return false;

        if (up_keys == null) {
            up_keys = new Gee.HashSet<int> ();
            down_keys = new Gee.HashSet<int> ();
            left_keys = new Gee.HashSet<int> ();
            right_keys = new Gee.HashSet<int> ();
            key_directions = new HashTable<int, InputDirection> (direct_hash, direct_equal);
        }

        key_directions[keycode] = direction;
        return true;
    }

    public override bool add_button (GamepadButtonPair pair, string? qualifier) {
        var direction = InputDirection.from_string (qualifier);
        if (direction == null)
            return false;

        if (up_buttons == null) {
            up_buttons = new Gee.HashSet<GamepadButtonPair> (
                GamepadButtonPair.hash, GamepadButtonPair.equal
            );
            down_buttons = new Gee.HashSet<GamepadButtonPair> (
                GamepadButtonPair.hash, GamepadButtonPair.equal
            );
            left_buttons = new Gee.HashSet<GamepadButtonPair> (
                GamepadButtonPair.hash, GamepadButtonPair.equal
            );
            right_buttons = new Gee.HashSet<GamepadButtonPair> (
                GamepadButtonPair.hash, GamepadButtonPair.equal
            );
            button_directions = new HashTable<GamepadButtonPair, InputDirection> (
                GamepadButtonPair.hash, GamepadButtonPair.equal
            );
        }

        button_directions[pair] = direction;
        return true;
    }

    public override bool add_analog (Gamepad.Analog analog, InputDirection? direction, string? qualifier) {
        assert (direction == null);

        if (analogs[analog] == null)
            analogs[analog] = {};

        return true;
    }

    public override void key_pressed (int keycode) {
        if (!(keycode in key_directions))
            return;

        var direction = key_directions[keycode];
        bool was_active = is_active (direction);
        var keys = get_keys_for_direction (direction);

        keys.add (keycode);

        if (!was_active)
            notify_control (direction);
    }

    public override void key_released (int keycode) {
        if (!(keycode in key_directions))
            return;

        var direction = key_directions[keycode];
        var keys = get_keys_for_direction (direction);

        keys.remove (keycode);

        if (!is_active (direction))
            notify_control (direction);
    }

    public override void button_pressed (GamepadButtonPair pair) {
        if (!(pair in button_directions))
            return;

        var direction = button_directions[pair];
        bool was_active = is_active (direction);
        var buttons = get_buttons_for_direction (direction);

        buttons.add (pair);

        if (!was_active)
            notify_control (direction);
    }

    public override void button_released (GamepadButtonPair pair) {
        if (!(pair in button_directions))
            return;

        var direction = button_directions[pair];
        var buttons = get_buttons_for_direction (direction);

        buttons.remove (pair);

        if (!is_active (direction))
            notify_control (direction);
    }

    public override void analog_moved (Gamepad.Analog analog, double x, double y) {
        var old_directions = analogs[analog].active_directions;
        double length = Math.sqrt (x * x + y * y);
        double angle = Math.atan2 (y, x);

        for (int i = 0; i < InputDirection.N_DIRECTIONS; i++) {
            bool active = length > ANALOG_DEADZONE &&
                AnalogUtils.is_pointing_at_direction (angle, DIAGONAL_AREA, i);

            analogs[analog].active_directions[i] = active;
        }

        // We have to calculate all directions first, otherwise we might miss
        // the opposite direction being active
        for (int i = 0; i < InputDirection.N_DIRECTIONS; i++) {
            if (analogs[analog].active_directions[i] == old_directions[i])
                continue;

            notify_control (i);
        }
    }

    private bool is_active (InputDirection direction) {
        for (int i = 0; i < Gamepad.N_ANALOGS; i++) {
            if (analogs[i] == null)
                continue;

            if (analogs[i].active_directions[direction])
                return true;
        }

        var keys = get_keys_for_direction (direction);
        var buttons = get_buttons_for_direction (direction);

        return (keys != null && keys.size > 0) ||
               (buttons != null && buttons.size > 0);
    }

    private void notify_control (InputDirection direction) {
        var dpad = control as DpadControl;

        bool active = is_active (direction);
        var opposite = direction.opposite ();

        if (is_active (opposite))
            dpad.activate (runner, player, opposite, !active);
        else
            dpad.activate (runner, player, direction, active);
    }
}
