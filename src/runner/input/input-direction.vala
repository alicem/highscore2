// This file is part of Highscore. License: GPL-3.0+.

public enum Highscore.InputDirection {
    UP,
    DOWN,
    LEFT,
    RIGHT;

    public const int N_DIRECTIONS = 4;

    public static InputDirection? from_string (string str) {
        if (str == "up")
            return UP;
        if (str == "down")
            return DOWN;
        if (str == "left")
            return LEFT;
        if (str == "right")
            return RIGHT;
        return null;
    }

    public string to_string () {
        switch (this) {
            case UP:
                return "up";
            case DOWN:
                return "down";
            case LEFT:
                return "left";
            case RIGHT:
                return "right";
            default:
                assert_not_reached ();
        }
    }

    public double get_x () {
        switch (this) {
            case UP:
            case DOWN:
                return 0;
            case LEFT:
                return -1;
            case RIGHT:
                return 1;
            default:
                assert_not_reached ();
        }
    }

    public double get_y () {
        switch (this) {
            case UP:
                return -1;
            case DOWN:
                return 1;
            case LEFT:
            case RIGHT:
                return 0;
            default:
                assert_not_reached ();
        }
    }

    public InputDirection opposite () {
        switch (this) {
            case UP:
                return DOWN;
            case DOWN:
                return UP;
            case LEFT:
                return RIGHT;
            case RIGHT:
                return LEFT;
            default:
                assert_not_reached ();
        }
    }

    public static InputDirection[] all () {
        return { UP, DOWN, LEFT, RIGHT };
    }
}
