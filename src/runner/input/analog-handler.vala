// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.AnalogHandler : ControlHandler {
    private Gee.HashSet<int>? pressed_keys;
    private HashTable<int, InputDirection> key_directions;

    private Gee.HashSet<GamepadButtonPair>? pressed_buttons;
    private HashTable<GamepadButtonPair, InputDirection> button_directions;
    private double analog_x[Gamepad.N_ANALOGS];
    private double analog_y[Gamepad.N_ANALOGS];

    public AnalogHandler (Runner runner, uint player, Control control) {
        this.runner = runner;
        this.player = player;
        this.control = control;
    }

    public override bool add_key (int keycode, string? qualifier) {
        var direction = InputDirection.from_string (qualifier);
        if (direction == null)
            return false;

        var analog = control as AnalogControl;
        if (!analog.enable_x && (direction == LEFT || direction == RIGHT))
            return false;
        if (!analog.enable_y && (direction == UP || direction == DOWN))
            return false;

        if (pressed_keys == null) {
            pressed_keys = new Gee.HashSet<int> ();
            key_directions = new HashTable<int, InputDirection> (direct_hash, direct_equal);
        }

        key_directions[keycode] = direction;
        return true;
    }

    public override bool add_button (GamepadButtonPair pair, string? qualifier) {
        var direction = InputDirection.from_string (qualifier);
        if (direction == null)
            return false;

        var analog = control as AnalogControl;
        if (!analog.enable_x && (direction == LEFT || direction == RIGHT))
            return false;
        if (!analog.enable_y && (direction == UP || direction == DOWN))
            return false;

        if (pressed_buttons == null) {
            pressed_buttons = new Gee.HashSet<GamepadButtonPair> (
                GamepadButtonPair.hash, GamepadButtonPair.equal
            );
            button_directions = new HashTable<GamepadButtonPair, InputDirection> (
                GamepadButtonPair.hash, GamepadButtonPair.equal
            );
        }

        button_directions[pair] = direction;
        return true;
    }

    public override bool add_analog (Gamepad.Analog analog, InputDirection? direction, string? qualifier) {
        assert (direction == null);

        return true;
    }

    public override void key_pressed (int keycode) {
        pressed_keys.add (keycode);

        notify_control ();
    }

    public override void key_released (int keycode) {
        pressed_keys.remove (keycode);

        notify_control ();
    }

    public override void button_pressed (GamepadButtonPair pair) {
        pressed_buttons.add (pair);

        notify_control ();
    }

    public override void button_released (GamepadButtonPair pair) {
        pressed_buttons.remove (pair);

        notify_control ();
    }

    public override void analog_moved (Gamepad.Analog analog, double x, double y) {
        analog_x[analog] = x;
        analog_y[analog] = y;

        notify_control ();
    }

    private void notify_control () {
        var analog = control as AnalogControl;

        double x = 0, y = 0;

        if (pressed_keys != null) {
            foreach (var key in pressed_keys) {
                var direction = key_directions[key];

                x += direction.get_x ();
                y += direction.get_y ();
            }
        }

        if (pressed_buttons != null) {
            foreach (var pair in pressed_buttons) {
                var direction = button_directions[pair];

                x += direction.get_x ();
                y += direction.get_y ();
            }
        }

        double length = Math.sqrt (x * x + y * y);
        if (length > 1) {
            x /= length;
            y /= length;
        }

        for (int i = 0; i < Gamepad.N_ANALOGS; i++) {
            x += analog_x[i];
            y += analog_y[i];
        }

        length = Math.sqrt (x * x + y * y);
        if (length > 1) {
            x /= length;
            y /= length;
        }

        if (!analog.enable_x)
            x = 0;
        if (!analog.enable_y)
            y = 0;

        analog.moved (runner, player, x, y);
    }
}
