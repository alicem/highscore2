// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.ButtonControl : Control {
    public signal void activate (Runner runner, uint player, bool pressed);

    public ButtonControl (string id, string name) {
        Object (id: id, name: name);
    }

    public override ControlType get_control_type () {
        return BUTTON;
    }
}
