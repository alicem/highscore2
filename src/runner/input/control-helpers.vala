// This file is part of Highscore. License: GPL-3.0+.

namespace Highscore.ControlHelpers {
    public delegate void SetButtonPressedFunc (Runner runner, uint player, int button, bool pressed);

    public void add_buttons (
        PlatformController controller,
        SetButtonPressedFunc callback,
        ...
    ) {
        var l = va_list ();

        while (true) {
            string? id = l.arg ();
            if (id == null)
                break;

            string name = l.arg ();
            int button = l.arg ();

            var control = new ButtonControl (id, name);
            control.activate.connect ((runner, player, pressed) => callback (runner, player, button, pressed));
            controller.add_control (control);
        }
    }

    public void add_dpad (
        PlatformController controller,
        SetButtonPressedFunc callback,
        string id,
        string name,
        int up_button,
        int down_button,
        int left_button,
        int right_button
    ) {
        var control = new DpadControl (id, name);
        control.activate.connect ((runner, player, direction, pressed) => {
            switch (direction) {
                case UP:
                    callback (runner, player, up_button, pressed);
                    break;
                case DOWN:
                    callback (runner, player, down_button, pressed);
                    break;
                case LEFT:
                    callback (runner, player, left_button, pressed);
                    break;
                case RIGHT:
                    callback (runner, player, right_button, pressed);
                    break;
                default:
                    assert_not_reached ();
            }
        });
        controller.add_control (control);
    }
}
