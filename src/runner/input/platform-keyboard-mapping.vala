// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.PlatformKeyboardMapping : Object {
    public struct Key {
        int keycode;
        string control;
    }

    private Key[] keys;

    construct {
        keys = {};
    }

    public void map (int keycode, string control) {
        Key mapping = { keycode, control };

        keys += mapping;
    }

    public Key[] list_key_mappings () {
        return keys;
    }
}
