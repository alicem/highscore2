// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.GamepadButtonPair {
    public Gamepad.Button button;
    public Gamepad.Button? modifier;

    public GamepadButtonPair (Gamepad.Button button, Gamepad.Button? modifier) {
        this.button = button;
        this.modifier = modifier;
    }

    public string to_string () {
        var button_id = button.get_id ();

        if (modifier == null)
            return button_id;

        var modifier_id = modifier.get_id ();

        return @"'$modifier_id' + '$button_id'";
    }

    public static uint hash (GamepadButtonPair pair) {
        if (pair.modifier == null)
            return pair.button;
        return pair.button + (Gamepad.N_BUTTONS + 1) * (pair.modifier + 1);
    }

    public static bool equal (GamepadButtonPair a, GamepadButtonPair b) {
        return a.button == b.button && a.modifier == b.modifier;
    }
}
