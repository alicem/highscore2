// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.InputHandler : Object {
    public unowned Runner runner { get; construct; }

    private Gee.HashSet<int> pressed_keys;

    private PlatformControls controls;

    private Gamepad? gamepad;
    private bool running;

    private HashTable<uint, PlayerHandler> player_handlers;

    public InputHandler (Runner runner) {
        Object (runner: runner);

        pressed_keys = new Gee.HashSet<int> ();
        player_handlers = new HashTable<uint, PlayerHandler> (direct_hash, direct_equal);

        var gamepads = GamepadManager.get_instance ().get_gamepads ();

        if (gamepads.length > 0)
            gamepad = gamepads[0];
        else
            gamepad = null;

        controls = PlatformControls.create (runner.game.platform);

        set_controller_type (0, controls.default_controller);

        if (gamepad != null)
            connect_gamepad (gamepad);
    }

    public void set_controller_type (uint player, string? type) {
        // TODO: support multiple players
        if (player != 0)
            return;

        var handler = player_handlers[player];

        if (handler != null) {
            if (type == handler.get_controller_type ())
                return;

            if (running)
                handler.ensure_released ();
        }

        handler = new PlayerHandler (this, controls, type, 0, true, gamepad);
        player_handlers[player] = handler;

        if (running)
            handler.ensure_pressed ();
    }

    private void connect_gamepad (Gamepad gamepad) {
        gamepad.button_pressed.connect (gamepad_button_pressed_cb);
        gamepad.button_released.connect (gamepad_button_released_cb);
        gamepad.analog_moved.connect (gamepad_analog_moved_cb);
    }

    private void gamepad_button_pressed_cb (Gamepad gamepad, Gamepad.Button button) {
        if (!running)
            return;

        var handler = player_handlers[0];
        handler.button_pressed (button);
    }

    private void gamepad_button_released_cb (Gamepad gamepad, Gamepad.Button button) {
        if (!running)
            return;

        var handler = player_handlers[0];
        handler.button_released (button);
    }

    private void gamepad_analog_moved_cb (Gamepad gamepad, Gamepad.Analog analog, double x, double y) {
        if (!running)
            return;

        var handler = player_handlers[0];
        handler.analog_moved (analog, x, y);
    }

    public bool key_pressed (int keycode) {
        /* Prevent key repeat */
        if (keycode in pressed_keys)
            return Gdk.EVENT_PROPAGATE;

        pressed_keys.add (keycode);

        if (running) {
            var handler = player_handlers[0];
            return handler.key_pressed (keycode);
        }

        return Gdk.EVENT_PROPAGATE;
    }

    public void key_released (int keycode) {
        if (running) {
            var handler = player_handlers[0];
            handler.key_released (keycode);
        }

        pressed_keys.remove (keycode);
    }

    public bool drag_begin (int screen_id, double x, double y) {
        return runner.delegate.drag_begin (screen_id, x, y);
    }

    public void drag_update (double x, double y) {
        runner.delegate.drag_update (x, y);
    }

    public void drag_end (double x, double y) {
        runner.delegate.drag_end (x, y);
    }

    public bool get_key_pressed (int keycode) {
        return keycode in pressed_keys;
    }

    public void start () {
        running = true;

        foreach (var handler in player_handlers.get_values ())
            handler.ensure_pressed ();
    }
}
