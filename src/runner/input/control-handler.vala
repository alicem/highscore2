// This file is part of Highscore. License: GPL-3.0+.

public abstract class Highscore.ControlHandler {
    protected unowned Runner runner;
    protected uint player;
    protected Control control;

    public string get_id () {
        return control.id;
    }

    public abstract bool add_key (int keycode, string? qualifier);
    public abstract bool add_button (GamepadButtonPair pair, string? qualifier);
    public abstract bool add_analog (Gamepad.Analog analog, InputDirection? direction, string? qualifier);

    public abstract void key_pressed (int keycode);
    public abstract void key_released (int keycode);

    public abstract void button_pressed (GamepadButtonPair pair);
    public abstract void button_released (GamepadButtonPair pair);

    public abstract void analog_moved (Gamepad.Analog analog, double x, double y);

    public static ControlHandler create (Runner runner, uint player, Control control) {
        switch (control.get_control_type ()) {
            case BUTTON:
               return new ButtonHandler (runner, player, control);
            case DPAD:
                return new DpadHandler (runner, player, control);
            case ANALOG:
                return new AnalogHandler (runner, player, control);
            default:
                assert_not_reached ();
        }
    }
}
