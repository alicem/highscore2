// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.AnalogControl : Control {
    public signal void moved (Runner runner, uint player, double x, double y);

    public bool enable_x { get; construct; }
    public bool enable_y { get; construct; }

    public AnalogControl (
        string id,
        string name,
        bool enable_x = true,
        bool enable_y = true
    ) requires (enable_x || enable_y) {
        Object (id: id, name: name, enable_x: enable_x, enable_y: enable_y);
    }

    public override ControlType get_control_type () {
        return ANALOG;
    }
}
