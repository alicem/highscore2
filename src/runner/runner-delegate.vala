// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.RunnerDelegate : Object {
    public unowned Runner runner { get; construct; }

    protected override void constructed () {
        base.constructed ();

        assert (runner != null);
    }

    public virtual async void before_load () throws Error {}
    public virtual async void after_load () throws Error {}
    public virtual async void before_reset () throws Error {}
    public virtual async void after_reset () throws Error {}
    public virtual async void load_state (KeyFile metadata) throws Error {}
    public virtual async void save_state (KeyFile metadata) throws Error {}
    public virtual async void pause () {}
    public virtual async void resume () {}

    public virtual bool drag_begin (int screen_id, double x, double y) { return false; }
    public virtual void drag_update (double x, double y) {}
    public virtual void drag_end (double x, double y) {}

    public virtual Gtk.Widget? create_header_widget () {
        return null;
    }

    public virtual void secondary_view_closed () {}
}
