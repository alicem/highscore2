// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Runner : Object {
    public enum CursorType {
        DEFAULT,
        CROSSHAIR,
    }

    public signal void stopped (string? error);
    public signal void redraw ();
    public signal void loading_snapshot (Snapshot snapshot);

    public bool block_redraw { get; set; }

    public Game game { get; construct; }

    public bool uses_pointer { get; set; }
    public bool running { get; private set; }
    public ScreenSet screen_set { get; private set; }

    public bool flipped { get; private set; }
    public Gdk.Texture? texture { get; private set; }

    private string core_name;
    private RunnerProcess process;
    private RunnerProxy proxy;
    private InputHandler input_handler;

    public Object? platform_proxy { get; private set; }
    public Object? addon_proxy { get; private set; }
    public SharedInputBuffer input_buffer { get; private set; }
    public RunnerDelegate delegate { get; private set; }
    public string view_name { get; set; }
    public string secondary_view_name { get; set; }

    private SharedVideoBuffer video_buffer;
    private double aspect_ratio;

    private File save_location;
    private bool loading_state;

    private Screen[] screens;
    private CursorType[] screen_cursors;

    private Snapshot? initial_snapshot;

    private uint redraw_idle_id;

    public Runner (string core_name, Game game, Snapshot? initial_snapshot) {
        Object (game: game);

        this.core_name = core_name;
        this.initial_snapshot = initial_snapshot;

        process = new RunnerProcess (
            core_name, game.platform.platform.get_name ()
        );

        process.stopped.connect (error => {
            running = false;
            stopped (error);
        });

        input_handler = new InputHandler (this);

        delegate = create_delegate ();

        screen_set = ScreenSet.create (this, initial_snapshot);

        screens = screen_set.get_screens ();

        screen_cursors = {};
        foreach (var screen in screens)
            screen_cursors += CursorType.DEFAULT;
    }

    ~Runner () {
        if (redraw_idle_id > 0)
            Source.remove (redraw_idle_id);
    }

    private RunnerDelegate create_delegate () {
        var type = game.platform.runner_delegate_type;

        if (type == Type.NONE && game.platform.parent != null)
            type = game.platform.parent.runner_delegate_type;

        if (type == Type.NONE)
            type = typeof (RunnerDelegate);

        return Object.new (type, runner: this) as RunnerDelegate;
    }

    private async void start_process () throws Error {
        yield process.start ();

        proxy = process.proxy;
        platform_proxy = process.platform_proxy;
        addon_proxy = process.addon_proxy;

        input_buffer = new SharedInputBuffer (yield proxy.get_input_buffer ());
        video_buffer = new SharedVideoBuffer (yield proxy.get_video_buffer ());

        running = true;

        proxy.redraw.connect (() => {
            if (redraw_idle_id > 0)
                return;

            redraw_idle_id = Idle.add_once (handle_redraw);
        });

        video_buffer.lock ();
        bool empty = video_buffer.is_empty ();
        video_buffer.unlock ();

        if (!empty)
            handle_redraw ();
    }

    private void handle_redraw () {
        redraw_idle_id = 0;

        if (loading_state)
            return;

        video_buffer.lock ();

        if (!video_buffer.get_invalidated ()) {
            video_buffer.unlock ();
            return;
        }

        aspect_ratio = video_buffer.get_aspect_ratio ();
        flipped = video_buffer.get_flipped ();

        var area = video_buffer.get_area ();
        var row_stride = video_buffer.get_row_stride ();
        var format = video_buffer.get_pixel_format ();
        var pixels = video_buffer.get_pixels ();

        int pixel_size = format.get_pixel_size ();
        size_t offset = area.y * row_stride + area.x * pixel_size;
        size_t size = row_stride * (area.height - 1) + area.width * pixel_size;

        var bytes = create_bytes (pixels + offset, size);
        video_buffer.unlock ();

        Gdk.MemoryFormat gdk_format;

        switch (format) {
            case RGB888:
                gdk_format = R8G8B8;
                break;
            case XRGB8888:
                gdk_format = R8G8B8A8_PREMULTIPLIED;
                break;
            case XRGB8888_REV:
                gdk_format = B8G8R8A8_PREMULTIPLIED;
                break;
            default:
                assert_not_reached ();
        }

        texture = new Gdk.MemoryTexture (
            (int) area.width,
            (int) area.height,
            gdk_format,
            bytes,
            row_stride
        );

        if (!block_redraw)
            redraw ();
    }

    private async File create_save_location () throws Error {
        var tmp_dir = yield File.new_tmp_dir_async ("highscore_save_XXXXXX");

        return tmp_dir.get_child ("save");
    }

    public async void start () throws Error requires (!running) {
        yield start_process ();

        yield delegate.before_load ();

        save_location = yield create_save_location ();

        if (initial_snapshot != null)
            yield initial_snapshot.load_data (save_location);

        yield proxy.load_rom (game.file.get_path (), save_location.get_path ());

        yield delegate.after_load ();

        yield proxy.start ();

        input_handler.start ();
    }

    public async void stop () throws Error {
        if (!running)
            return;

        yield process.stop ();
        proxy = null;
        stopped (null);

        if (save_location != null) {
            yield FileUtils.delete_recursively (save_location.get_parent ());
            save_location = null;
        }
    }

    public async void reset () throws Error requires (running) {
        yield delegate.before_reset ();
        yield proxy.reset ();
        yield delegate.after_reset ();
    }

    public async void pause () throws Error requires (running) {
        yield proxy.pause ();
        yield delegate.pause ();
    }

    public async void resume () throws Error requires (running) {
        try {
            yield delegate.resume ();
            yield proxy.resume ();
        } catch (Error e) {
            stopped (e.message);
        }
    }

    public async bool load_snapshot (Snapshot snapshot, bool reload_data) throws Error requires (running) {
        if (!snapshot.is_valid ())
            return false;

        loading_state = true;

        if (!block_redraw)
            loading_snapshot (snapshot);

        if (reload_data) {
            var old_save_location = save_location;

            save_location = yield create_save_location ();
            yield proxy.reload_save (save_location.get_path ());

            yield FileUtils.delete_recursively (old_save_location.get_parent ());
        }

        yield snapshot.load_data (save_location);

        var savestate_path = snapshot.get_savestate_path ();
        yield proxy.load_state (savestate_path);

        var metadata = snapshot.get_metadata ();
        yield delegate.load_state (metadata);

        loading_state = false;

        return true;
    }

    public async Snapshot save_snapshot () throws Error requires (running) {
        var snapshot = new Snapshot (game);
        snapshot.ensure_dir ();

        // Save
        yield proxy.sync_save ();
        yield snapshot.save_data (save_location);

        // Savestate
        var savestate_path = snapshot.get_savestate_path ();
        yield proxy.save_state (savestate_path);

        // Screenshot
        snapshot.save_screenshot (texture);

        // Metadata
        var keyfile = new KeyFile ();

        keyfile.set_string ("Metadata", "Creation Date", new DateTime.now ().to_string ());
        keyfile.set_string ("Metadata", "Platform", game.platform.id);
        keyfile.set_string ("Metadata", "Core", core_name);

        keyfile.set_boolean ("Screenshot", "Flipped", flipped);
        keyfile.set_double ("Screenshot", "Aspect Ratio", get_aspect_ratio ());

        yield delegate.save_state (keyfile);

        snapshot.save_metadata (keyfile);

        return snapshot;
    }

    public double get_aspect_ratio () requires (running) {
        return aspect_ratio;
    }

    public void set_controller_type (uint player, string? type) {
        input_handler.set_controller_type (player, type);
    }

    public bool key_pressed (int keycode) {
        if (running)
            return input_handler.key_pressed (keycode);

        return false;
    }

    public void key_released (int keycode) {
        if (running)
            input_handler.key_released (keycode);
    }

    public bool drag_begin (int screen_id, double x, double y) {
        if (running)
            return input_handler.drag_begin (screen_id, x, y);

        return false;
    }

    public void drag_update (double x, double y) {
        if (running)
            input_handler.drag_update (x, y);
    }

    public void drag_end (double x, double y) {
        if (running)
            input_handler.drag_end (x, y);
    }

    public Gtk.Widget? create_header_widget () {
        return delegate.create_header_widget ();
    }

    public void secondary_view_closed () {
        delegate.secondary_view_closed ();
    }

    public CursorType get_cursor (Screen screen) {
        int index = -1;

        for (int i = 0; i < screens.length; i++) {
            if (screens[i].id == screen.id) {
                index = i;
                break;
            }
        }

        return screen_cursors[index];
    }

    public void set_cursor (int screen_id, CursorType cursor) {
        int index = -1;

        for (int i = 0; i < screens.length; i++) {
            if (screens[i].id == screen_id) {
                index = i;
                break;
            }
        }

        assert (index >= 0);

        screen_cursors[index] = cursor;
    }
}
