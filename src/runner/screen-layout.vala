// This file is part of Highscore. License: GPL-3.0-or-later

public abstract class Highscore.ScreenLayout : Object {
    public struct Result {
        bool visible;
        float width;
        float height;
        float angle;
        Gsk.Transform transform;
    }

    public struct ViewDimensions {
        float width;
        float height;
        float top_inset;
        float bottom_inset;
        float left_inset;
        float right_inset;
    }

    public abstract void measure (
        float source_width,
        float source_height,
        Screen[] screens,
        out float width,
        out float height
    );

    public abstract Result[] layout (ViewDimensions dimensions, float aspect_ratio, Screen[] screens);

    private static Gsk.Transform interpolate_transform (Gsk.Transform a, Gsk.Transform b, double value) {
        var transform = new Gsk.Transform ();

        float a_skew_x, a_skew_y, a_scale_x, a_scale_y, a_angle, a_dx, a_dy;
        float b_skew_x, b_skew_y, b_scale_x, b_scale_y, b_angle, b_dx, b_dy;

        a.to_2d_components (
            out a_skew_x, out a_skew_y,
            out a_scale_x, out a_scale_y,
            out a_angle, out a_dx, out a_dy
        );

        b.to_2d_components (
            out b_skew_x, out b_skew_y,
            out b_scale_x, out b_scale_y,
            out b_angle, out b_dx, out b_dy
        );

        float skew_x  = (float) Adw.lerp (a_skew_x,  b_skew_x,  value);
        float skew_y  = (float) Adw.lerp (a_skew_y,  b_skew_y,  value);
        float scale_x = (float) Adw.lerp (a_scale_x, b_scale_x, value);
        float scale_y = (float) Adw.lerp (a_scale_y, b_scale_y, value);
        float angle   = (float) Adw.lerp (a_angle,   b_angle,   value);
        float dx      = (float) Adw.lerp (a_dx,      b_dx,      value);
        float dy      = (float) Adw.lerp (a_dy,      b_dy,      value);

        transform = transform.translate ({ dx, dy });
        transform = transform.rotate (angle);
        transform = transform.scale (scale_x, scale_y);
        transform = transform.skew (skew_x, skew_y);

        return transform;
    }

    public static Result[] interpolate (
        ScreenLayout? a_layout,
        ScreenLayout? b_layout,
        double value,
        ViewDimensions dimensions,
        float aspect_ratio,
        Screen[] screens,
        out double[] opacities
    ) {
        assert (a_layout != null || b_layout != null);

        double[] ret_opacities = {};

        if (a_layout == null) {
            var ret = b_layout.layout (dimensions, aspect_ratio, screens);

            foreach (var res in ret)
                ret_opacities += res.visible ? 1 : 0;

            opacities = ret_opacities;
            return ret;
        }

        if (b_layout == null) {
            var ret = a_layout.layout (dimensions, aspect_ratio, screens);

            foreach (var res in ret)
                ret_opacities += res.visible ? 1 : 0;

            opacities = ret_opacities;
            return ret;
        }

        var a = a_layout.layout (dimensions, aspect_ratio, screens);
        var b = b_layout.layout (dimensions, aspect_ratio, screens);

        assert (a.length == b.length);

        Result[] ret = {};

        for (int i = 0; i < a.length; i++) {
            Gsk.Transform transform;
            float w, h, angle;
            double opacity;

            if (a[i].visible && b[i].visible) {
                float a_angle = a[i].angle;
                float b_angle = b[i].angle;

                if (b_angle - a_angle > 180)
                    b_angle -= 360;
                else if (b_angle - a_angle <= -180)
                    b_angle += 360;

                w = (float) Adw.lerp (a[i].width, b[i].width, value);
                h = (float) Adw.lerp (a[i].height, b[i].height, value);
                angle = (float) Adw.lerp (a_angle, b_angle, value);
                opacity = 1;
                transform = interpolate_transform (
                    a[i].transform ?? new Gsk.Transform (),
                    b[i].transform ?? new Gsk.Transform (),
                    value
                );
            } else if (a[i].visible) {
                w = a[i].width;
                h = a[i].height;
                angle = a[i].angle;
                transform = a[i].transform ?? new Gsk.Transform ();
                opacity = 1 - value;
            } else if (b[i].visible) {
                w = b[i].width;
                h = b[i].height;
                angle = b[i].angle;
                transform = b[i].transform ?? new Gsk.Transform ();
                opacity = value;
            } else {
                w = h = angle = 0;
                opacity = 0;
                transform = new Gsk.Transform ();
            }

            Result result = {
                a[i].visible || b[i].visible,
                w, h, angle, transform,
            };

            ret += result;
            ret_opacities += opacity;
        }

        opacities = ret_opacities;

        return ret;
    }
}
