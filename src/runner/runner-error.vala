// This file is part of Highscore. License: GPL-3.0+.

public errordomain Highscore.RunnerError {
    INVALID_SAVE_LOCATION,
}
