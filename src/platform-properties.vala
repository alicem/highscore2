// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.PlatformProperties : Adw.Bin {
    public PropertiesDialog dialog { get; construct; }
    public Game game { get; construct; }
}
