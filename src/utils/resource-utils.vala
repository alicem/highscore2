// This file is part of Highscore. License: GPL-3.0+.

namespace Highscore.ResourceUtils {
    public static string load_to_string (string path) {
        try {
            var bytes = resources_lookup_data (path, NONE);

            return (string) bytes.get_data ();
        } catch (Error e) {
            error ("Can't load resource '%s': %s", path, e.message);
        }
    }
}
