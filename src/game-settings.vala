// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.GameSettings : Object {
    public Game game { get; construct; }

    private SettingsBackend backend;

    public GameSettings (Game game) {
        Object (game: game);
    }

    construct {
        var settings_dir = get_settings_dir ();

        try {
            if (!settings_dir.query_exists ())
                settings_dir.make_directory_with_parents ();
        } catch (Error e) {
            error ("Failed to initialize settings dir: %s", e.message);
        }

        var settings_file = settings_dir.get_child (game.uid);

        backend = SettingsBackend.keyfile_settings_backend_new (
            settings_file.peek_path (),
            "/org/gnome/World/Highscore2/game/",
            "common"
        );
    }

    private File get_settings_dir () {
        var data_dir = Environment.get_user_data_dir ();
        var save_dir_path = Path.build_filename (data_dir, "highscore", "settings");

        return File.new_for_path (save_dir_path);
    }

    public Settings get_common () {
        return new Settings.with_backend (
            "org.gnome.World.Highscore2.game", backend
        );
    }

    public Settings? get_platform () {
        var platform = game.platform;

        if (platform.parent != null)
            platform = platform.parent;

        var id = platform.id;
        var schema = @"org.gnome.World.Highscore2.game.platforms.$id";

        return new Settings.with_backend (schema, backend);
    }

    public Settings? get_addon_platform () {
        if (game.platform.parent == null)
            return null;

        var id = game.platform.id;
        var schema = @"org.gnome.World.Highscore2.game.platforms.$id";

        return new Settings.with_backend (schema, backend);
    }
}
