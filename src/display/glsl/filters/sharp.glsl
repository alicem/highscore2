#ifdef VERTEX

void hs_main() {
}

#else // FRAGMENT

vec4 hs_main() {
  return hs_texture(uv);
}

#endif