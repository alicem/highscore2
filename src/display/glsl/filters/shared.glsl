#ifdef CORE
#  define _OUT_ out
#  define _IN_ in
#else
#  define _OUT_ varying

#  ifdef VERTEX
#    define _IN_ attribute
#  else
#    define _IN_ varying
#  endif

#  define texture texture2D
#endif

#if !defined(LEGACY)
precision mediump float;
#endif

#ifdef VERTEX

_IN_ vec2 position;
_IN_ vec2 texCoord;

_OUT_ vec2 uv;

uniform mat4 u_mvp;
uniform vec2 u_textureSize;
uniform vec2 u_targetSize;

void hs_main();

void main() {
  uv = vec2(0, 1) + vec2(1, -1) * texCoord;

  gl_Position = u_mvp * vec4(position, 0, 1);

  hs_main();
}

#else // FRAGMENT

_IN_ vec2 uv;

#ifdef CORE
_OUT_ vec4 outputColor;
#endif

uniform sampler2D u_textures[2];
uniform int u_nTextures;
uniform vec2 u_textureSize;
uniform vec2 u_targetSize;

uniform float u_opacity;

vec4 hs_main();

vec4 hs_texture(vec2 uv) {
  if (u_nTextures == 2)
    return mix(texture(u_textures[0], uv), texture(u_textures[1], uv), 0.5);

  return texture(u_textures[0], uv);
}

void main() {
  vec4 result = hs_main() * u_opacity;

#ifdef CORE
  outputColor = result;
#else
  gl_FragColor = result;
#endif
}

#endif