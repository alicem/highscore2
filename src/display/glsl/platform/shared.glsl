#ifdef CORE
#  define _OUT_ out
#  define _IN_ in
#else
#  define _OUT_ varying

#  ifdef VERTEX
#    define _IN_ attribute
#  else
#    define _IN_ varying
#  endif

#  define texture texture2D
#endif

#if !defined(LEGACY)
precision mediump float;
#endif

#ifdef VERTEX

_IN_ vec2 position;
_IN_ vec2 texCoord;

_OUT_ vec2 uv;
_OUT_ vec2 screenUv;

uniform vec2 u_screenPosition;
uniform vec2 u_screenSize;
uniform bool u_flipped;
uniform mat4 u_mvp;

void hs_main();

void main() {
  if (!u_flipped)
    uv = vec2(0, 1) + vec2(1, -1) * texCoord;
  else
    uv = texCoord;

  uv = u_screenPosition + uv * u_screenSize;

  gl_Position = u_mvp * vec4(position, 0, 1);

  hs_main();
}

#else // FRAGMENT

_IN_ vec2 uv;

#ifdef CORE
_OUT_ vec4 outputColor;
#endif

uniform sampler2D u_texture;
uniform vec2 u_textureSize;

vec4 hs_main();

vec4 hs_texture(vec2 uv) {
  return texture(u_texture, uv);
}

void main() {
#ifdef CORE
  outputColor = hs_main();
#else
  gl_FragColor = hs_main();
  gl_FragColor = vec4(gl_FragColor.rgb, 1);
#endif
}

#endif