// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/org/gnome/World/Highscore2/display/secondary-window.ui")]
public class Highscore.SecondaryWindow : Adw.ApplicationWindow {
    [GtkChild]
    private unowned Gtk.Stack stack;
    [GtkChild]
    private unowned GameView view;
    [GtkChild]
    private unowned Adw.StatusPage error_page;

    public unowned Runner runner { get; construct; }
    public string view_title { get; set; }
    public string subtitle { get; set; }

    public SecondaryWindow (Runner runner) {
        Object (runner: runner);
    }

    construct {
        var settings = new Settings ("org.gnome.World.Highscore2.state");

        settings.bind ("secondary-window-maximized", this, "maximized",      GET | SET | GET_NO_CHANGES);
        settings.bind ("secondary-window-width",     this, "default-width",  GET | SET | GET_NO_CHANGES);
        settings.bind ("secondary-window-height",    this, "default-height", GET | SET | GET_NO_CHANGES);

        view.runner = runner;

        if (runner != null)
            runner.stopped.connect (game_stopped);

        runner.game.bind_property ("title", this, "view-title", SYNC_CREATE);

        runner.notify["secondary-view-name"].connect (update_title);

        update_title ();
    }

    private void update_title () {
        subtitle = runner.secondary_view_name;

        if (subtitle == null || subtitle == "")
            title = view_title;
        else
            title = _("%s (%s)").printf (view_title, subtitle);
    }

    static construct {
        install_action ("game.screenshot", null, widget => {
            var self = widget as SecondaryWindow;

            Screenshot.take_screenshot.begin (self.view);
        });
    }

    protected override bool close_request () {
        view.runner = null;

        return base.close_request ();
    }

    protected override void map () {
        base.map ();

        view.grab_focus ();
    }

    private void game_stopped (string? error) {
        if (error != null) {
            error_page.title = _("Game Crashed");
            error_page.description = error;
            stack.visible_child_name = "error";
        }

        runner.stopped.disconnect (game_stopped);

        view.runner = null;
    }
}
