// This file is part of Highscore. License: GPL-3.0+.

using GL;

public class Highscore.GLDisplay : Gtk.GLArea {
    public bool secondary { get; set; }

    public int top_inset { get; set; }
    public int bottom_inset { get; set; }
    public int left_inset { get; set; }
    public int right_inset { get; set; }

    private Runner _runner;
    public Runner runner {
        get { return _runner; }
        set {
            if (runner != null) {
                runner.weak_unref (runner_weak_notify_cb);

                runner.disconnect (redraw_id);
                redraw_id = 0;

                runner.disconnect (load_id);
                load_id = 0;

                game_settings = null;
            }

            _runner = value;

            if (runner != null) {
                runner.weak_ref (runner_weak_notify_cb);

                if (screen_set != null) {
                    screen_set.disconnect (screen_layout_id);
                    screen_layout_id = 0;
                }

                screen_set = runner.screen_set;

                if (secondary) {
                    set_screen_layout (screen_set.secondary_layout ?? new SingleLayout (0), false);

                    screen_layout_id = screen_set.secondary_layout_changed.connect (animate => {
                        set_screen_layout (screen_set.secondary_layout ?? new SingleLayout (0), animate);
                    });
                } else {
                    set_screen_layout (screen_set.layout, false);

                    screen_layout_id = screen_set.layout_changed.connect (animate => {
                        set_screen_layout (screen_set.layout, animate);
                    });
                }

                screens = screen_set.get_screens ();
                screens_changed = true;

                redraw_id = runner.redraw.connect (() => {
                    last_frame = current_frame;

                    aspect_ratio = runner.get_aspect_ratio ();
                    flipped = runner.flipped;
                    current_frame = runner.texture;

                    frame_changed = true;

                    queue_draw ();
                });

                load_id = runner.loading_snapshot.connect (show_snapshot);

                if (runner.texture != null) {
                    last_frame = current_frame;

                    aspect_ratio = runner.get_aspect_ratio ();
                    flipped = runner.flipped;
                    current_frame = runner.texture;

                    frame_changed = true;
                }

                screen_set.filter_changed.connect (() => {
                    platform_filter_changed = true;

                    queue_draw ();
                });

                platform_filter_recreated = true;
                platform_filter_changed = true;

                game_settings = new GameSettings (runner.game).get_common ();
                game_settings.bind ("interframe-blending", this, "interframe-blending", GET);
            }

            queue_draw ();
        }
    }

    private Gdk.Texture? current_frame;
    private Gdk.Texture? last_frame;

    private ScreenSet screen_set;
    private Screen[]? screens;

    private bool screens_changed;

    private double aspect_ratio;
    private bool flipped;

    private int last_width;
    private int last_height;

    private bool context_recreated;

    private GLVertexArray vertex_array;
    private GLVertexBuffer vertex_buffer;
    private GLElementBuffer element_buffer;
    private GLShader platform_shader;
    private GLShader shader;
    private GLTexture texture;
    private GLTexture last_texture;
    private GLFramebuffer[] framebuffers;
    private GLFramebuffer[] last_framebuffers;

    private bool frame_changed;

    private ScreenLayout screen_layout;
    private ScreenLayout? prev_screen_layout;
    private Adw.SpringAnimation screen_layout_animation;

    private ulong redraw_id;
    private ulong load_id;
    private ulong screen_layout_id;

    private Settings settings;
    private Settings game_settings;

    private bool filter_changed;
    private bool platform_filter_changed;
    private bool platform_filter_recreated;

    public bool bilinear_filtering { get; set; }
    public bool interframe_blending { get; set; }

    construct {
        settings = new Settings ("org.gnome.World.Highscore2");

        settings.bind ("bilinear-filtering", this, "bilinear-filtering", GET);

        notify["bilinear-filtering"].connect (() => {
            filter_changed = true;
            queue_draw ();
        });
        notify["interframe-blending"].connect (queue_draw);
        notify["top-inset"].connect (queue_draw);
        notify["bottom-inset"].connect (queue_draw);
        notify["left-inset"].connect (queue_draw);
        notify["right-inset"].connect (queue_draw);

        screen_layout_animation = new Adw.SpringAnimation (
            this, 0, 1,
            new Adw.SpringParams (1, 1, 800),
            new Adw.CallbackAnimationTarget (value => {
                queue_draw ();

                // FIXME: If the drag gesture is active, runner needs a notify
            })
        );
        screen_layout_animation.epsilon /= 10;
        screen_layout_animation.done.connect (() => {
            prev_screen_layout = null;
        });
    }

    ~GLDisplay () {
        if (runner != null)
            runner.weak_unref (runner_weak_notify_cb);
    }

    private void runner_weak_notify_cb () {
        _runner = null;
        notify_property ("runner");
    }

    private void set_screen_layout (ScreenLayout layout, bool animate) {
        if (screen_layout_animation.state == PLAYING)
            screen_layout_animation.skip ();

        screens_changed = true;

        if (!animate) {
            screen_layout = layout;
            prev_screen_layout = null;
            queue_draw ();
            return;
        }

        prev_screen_layout = screen_layout;
        screen_layout = layout;

        screen_layout_animation.play ();
    }

    public override void realize () {
        base.realize ();

        make_current ();

        if (get_error () != null)
            return;

        vertex_array = new GLVertexArray ();
        vertex_array.bind ();

        vertex_buffer = new GLVertexBuffer ();
        vertex_buffer.bind ();
        vertex_buffer.set_data ({
             0,  1,
             1,  1,
             1,  0,
             0,  0,
        });

        element_buffer = new GLElementBuffer ();
        element_buffer.bind ();
        element_buffer.set_data ({
            0, 1, 2,
            2, 3, 0
        });

        texture = new GLTexture ();
        last_texture = new GLTexture ();

        update_filter ();

        framebuffers = {};
        last_framebuffers = {};

        context_recreated = true;
    }

    public override void unrealize () {
        make_current ();

        if (get_error () != null) {
            base.unrealize ();
            return;
        }

        vertex_array = null;
        vertex_buffer = null;
        element_buffer = null;
        shader = null;
        platform_shader = null;
        texture = null;
        last_texture = null;
        framebuffers = {};
        last_framebuffers = {};

        base.unrealize ();
    }

    public override bool render (Gdk.GLContext context) {
        var error = get_error ();
        if (error != null)
            critical (error.message);

        if (filter_changed) {
            update_filter ();
            filter_changed = false;
        }

        glClearColor (0.0f, 0.0f, 0.0f, 0.0f);
        glClear (GL_COLOR_BUFFER_BIT);

        draw_frame ();

        glFlush ();

        return Gdk.EVENT_STOP;
    }

    private void draw_first_pass (
        GLTexture texture,
        GLFramebuffer[] framebuffers,
        Screen[] screens,
        ScreenLayout.Result[] layout_result
    ) {
        platform_shader.bind ();
        platform_shader.set_attribute_pointer ("position", 2, GL_FLOAT);
        platform_shader.set_attribute_pointer ("texCoord", 2, GL_FLOAT);

        texture.bind ();

        GLUtils.set_active_texture (0);
        texture.set_params (GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE);
        platform_shader.set_uniform_int ("u_texture", 0);

        for (int i = 0; i < screens.length; i++) {
            var screen = screens[i];
            var result = layout_result[i];

            if (!result.visible)
                continue;

            int w = (int) Math.ceil (last_width * screen.area.size.width);
            int h = (int) Math.ceil (last_height * screen.area.size.height);

            Graphene.Matrix mvp = {};
            mvp.init_identity ();
            mvp.scale (2, 2, 1);
            mvp.translate ({ -1, -1, 0 });

            Graphene.Vec2 position = {}, size = {};
            position.init (screen.area.origin.x, screen.area.origin.y);
            size.init (screen.area.size.width, screen.area.size.height);

            platform_shader.set_uniform_vec2 ("u_screenPosition", position);
            platform_shader.set_uniform_vec2 ("u_screenSize", size);
            platform_shader.set_uniform_bool ("u_flipped", flipped);
            platform_shader.set_uniform_matrix ("u_mvp", mvp);

            screen_set.setup_filter (platform_shader, screen.id);

            framebuffers[i].bind (GL_DRAW_FRAMEBUFFER);
            glViewport (0, 0, w, h);

            glClearColor (0.0f, 0.0f, 0.0f, 0.0f);
            glClear (GL_COLOR_BUFFER_BIT);

            element_buffer.draw ();

            framebuffers[i].unbind (GL_DRAW_FRAMEBUFFER);
        }

        texture.unbind ();
        platform_shader.unbind ();
    }

    private void draw_frame () {
        if (current_frame == null)
            return;

        int widget_width = get_width () * scale_factor;
        int widget_height = get_height () * scale_factor;

        if ((screens_changed || context_recreated) &&
            screens != null &&
            framebuffers.length != screens.length) {
            framebuffers = {};
            last_framebuffers = {};

            for (int i = 0; i < screens.length; i++) {
                framebuffers += new GLFramebuffer ();
                last_framebuffers += new GLFramebuffer ();
            }
        }

        if (platform_filter_recreated || context_recreated)
            create_platform_filter ();

        if (current_frame.width != last_width ||
            current_frame.height != last_height ||
            screens_changed ||
            context_recreated) {
            last_width = current_frame.width;
            last_height = current_frame.height;

            for (int i = 0; i < screens.length; i++) {
                var screen = screens[i];

                int w = (int) Math.ceil (last_width * screen.area.size.width);
                int h = (int) Math.ceil (last_height * screen.area.size.height);

                framebuffers[i].bind ();
                framebuffers[i].create_texture (
                    w, h, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER
                );
                framebuffers[i].unbind ();

                last_framebuffers[i].bind ();
                last_framebuffers[i].create_texture (
                    w, h, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_BORDER
                );
                last_framebuffers[i].unbind ();
            }

            attach_buffers ();
        }

        double[] opacities;
        var layout_result = ScreenLayout.interpolate (
            prev_screen_layout,
            screen_layout,
            screen_layout_animation.value,
            {
                widget_width,
                widget_height,
                top_inset * scale_factor,
                bottom_inset * scale_factor,
                left_inset * scale_factor,
                right_inset * scale_factor,
            },
            (float) aspect_ratio,
            screens,
            out opacities
        );

        assert (screens.length == layout_result.length);

        int[] indices = new int[screens.length];
        for (int i = 0; i < screens.length; i++)
            indices[i] = i;

        qsort_with_data<int> (indices, sizeof (int), (a, b) => {
            if (opacities[a] < opacities[b])
                return -1;
            if (opacities[a] > opacities[b])
                return 1;
            return 0;
        });

        if (frame_changed || context_recreated) {
            if (last_frame != null && !context_recreated) {
                for (int i = 0; i < screens.length; i++) {
                    var tmp = last_framebuffers[i];
                    last_framebuffers[i] = framebuffers[i];
                    framebuffers[i] = tmp;
                }

                var tmp = last_texture;
                last_texture = texture;
                texture = tmp;
            }

            texture.bind ();
            texture.upload (current_frame);
            texture.unbind ();

            if (context_recreated && last_frame != null) {
                last_texture.bind ();
                last_texture.upload (last_frame);
                last_texture.unbind ();
            }
        }

        draw_first_pass (texture, framebuffers, screens, layout_result);

        if ((screens_changed || platform_filter_changed || context_recreated) &&
            last_frame != null) {
            draw_first_pass (
                last_texture, last_framebuffers, screens, layout_result
            );
        }

        attach_buffers ();

        shader.bind ();
        shader.set_attribute_pointer ("position", 2, GL_FLOAT);
        shader.set_attribute_pointer ("texCoord", 2, GL_FLOAT);

        for (int i = 0; i < screens.length; i++) {
            var screen = screens[indices[i]];
            var result = layout_result[indices[i]];
            double opacity = opacities[indices[i]];

            if (!result.visible)
                continue;

            var transform = new Gsk.Transform ();
            transform = transform.translate ({ -1, 1 });
            transform = transform.scale (2, -2);
            transform = transform.scale (1.0f / widget_width, 1.0f / widget_height);

            float top = top_inset * scale_factor;
            float bottom = bottom_inset * scale_factor;
            float left = left_inset * scale_factor;
            float right = right_inset * scale_factor;

            transform = transform.translate ({
                left + (widget_width - left - right) / 2.0f,
                top + (widget_height - top - bottom) / 2.0f
            });
            transform = transform.rotate (result.angle);
            transform = transform.translate ({
                -left - (widget_width - left - right) / 2.0f,
                -top - (widget_height - top - bottom) / 2.0f
            });

            transform = transform.transform (result.transform);
            transform = transform.scale (result.width, result.height);

            Graphene.Rect rect = {{ 0, 0 }, { 1, 1 }};
            var transformed_rect = transform.transform_bounds (rect);

            if (!transformed_rect.intersection ({{ -1, -1 }, { 2, 2 }}, null))
                continue;

            var mvp = transform.to_matrix ();

            Graphene.Vec2 texture_size = {}, target_size = {};
            texture_size.init (
                last_width * screen.area.size.width,
                last_height * screen.area.size.height
            );
            target_size.init (result.width, result.height);

            bool blend_frames = last_frame != null && interframe_blending;

            GLUtils.set_active_texture (0);
            framebuffers[indices[i]].bind_texture ();
            shader.set_uniform_int ("u_nTextures", blend_frames ? 2 : 1);

            if (blend_frames) {
                GLUtils.set_active_texture (1);
                last_framebuffers[indices[i]].bind_texture ();

                shader.set_uniform_int_array ("u_textures", { 0, 1 });
            } else {
                shader.set_uniform_int_array ("u_textures", { 0, 0 });
            }

            glViewport (0, 0, widget_width, widget_height);

            shader.set_uniform_matrix ("u_mvp", mvp);
            shader.set_uniform_vec2 ("u_textureSize", texture_size);
            shader.set_uniform_vec2 ("u_targetSize", target_size);
            shader.set_uniform_float ("u_opacity", (float) opacity);

            element_buffer.draw ();

            if (blend_frames) {
                GLUtils.set_active_texture (1);
                last_framebuffers[indices[i]].unbind_texture ();
            }

            GLUtils.set_active_texture (0);
            framebuffers[indices[i]].unbind_texture ();
        }

        shader.unbind ();

        frame_changed = false;
        screens_changed = false;
        platform_filter_changed = false;
        platform_filter_recreated = false;
        context_recreated = false;
    }

    private void transform_coords_do (
        int? screen_id,
        double in_x,
        double in_y,
        out Screen? out_screen,
        out double out_x,
        out double out_y
    ) {
        if (current_frame == null) {
            out_screen = null;
            out_x = -1;
            out_y = -1;
            return;
        }

        int widget_width = get_width () * scale_factor;
        int widget_height = get_height () * scale_factor;

        var layout_info = ScreenLayout.interpolate (
            prev_screen_layout,
            screen_layout,
            screen_layout_animation.value,
            {
                widget_width,
                widget_height,
                top_inset,
                bottom_inset,
                left_inset,
                right_inset,
            },
            (float) aspect_ratio,
            screens,
            null
        );

        // Reverse direction since the last screen will end up drawn at the top
        for (int i = layout_info.length - 1; i >= 0; i--) {
            var transform = new Gsk.Transform ();

            if (screen_id != null && screen_id != screens[i].id)
                continue;

            transform = transform.scale (1 / layout_info[i].width, 1 / layout_info[i].height);
            transform = transform.transform (layout_info[i].transform.invert ());

            transform = transform.translate ({
                left_inset + (widget_width - left_inset - right_inset) / 2.0f,
                top_inset + (widget_height - top_inset - bottom_inset) / 2.0f
            });
            transform = transform.rotate (-layout_info[i].angle);
            transform = transform.translate ({
                -left_inset - (widget_width - left_inset - right_inset) / 2.0f,
                -top_inset - (widget_height - top_inset - bottom_inset) / 2.0f
            });

            transform = transform.scale (scale_factor, scale_factor);

            var point = transform.transform_point ({ (float) in_x, (float) in_y });

            if (screen_id == null && (point.x < 0 || point.x > 1 || point.y < 0 || point.y > 1))
                continue;

            out_screen = screens[i];
            out_x = point.x.clamp (0, 1);
            out_y = point.y.clamp (0, 1);
            return;
        }

        out_screen = null;
        out_x = -1;
        out_y = -1;
    }

    public Screen? find_screen (double x, double y) {
        if (screens == null)
            return null;

        Screen? ret;
        transform_coords_do (null, x, y, out ret, null, null);
        return ret;
    }

    public void transform_coords (int screen_id, double x, double y, out double out_x, out double out_y) {
        transform_coords_do (screen_id, x, y, null, out out_x, out out_y);
    }

    public void show_snapshot (Snapshot snapshot) {
        try {
            var metadata = snapshot.get_metadata ();

            aspect_ratio = metadata.get_double (
                "Screenshot", "Aspect Ratio"
            );
            flipped = metadata.get_boolean (
                "Screenshot", "Flipped"
            );

            current_frame = snapshot.get_screenshot ();
            last_frame = null;

            frame_changed = true;
        } catch (Error e) {
            critical ("Failed to show snapshot: %s", e.message);
        }

        if (runner == null) {
            screen_set = ScreenSet.create (null, snapshot);
            screens = screen_set.get_screens ();
            screens_changed = true;

            if (secondary) {
                set_screen_layout (screen_set.secondary_layout ?? new SingleLayout (0), false);

                screen_layout_id = screen_set.secondary_layout_changed.connect (animate => {
                    set_screen_layout (screen_set.secondary_layout ?? new SingleLayout (0), animate);
                });
            } else {
                set_screen_layout (screen_set.layout, false);

                screen_layout_id = screen_set.layout_changed.connect (animate => {
                    set_screen_layout (screen_set.layout, animate);
                });
            }

            screen_set.filter_changed.connect (() => {
                platform_filter_changed = true;

                queue_draw ();
            });

            platform_filter_recreated = true;
        }

        queue_draw ();
    }

    private void update_filter () {
        var context = get_context ();

        string path = null;
        string dir = "/org/gnome/World/Highscore2/display/glsl/filters";

        if (bilinear_filtering)
            path = @"$dir/smooth.glsl";
        else
            path = @"$dir/sharp.glsl";

        var shared = ResourceUtils.load_to_string (
            @"$dir/shared.glsl"
        );

        var main = ResourceUtils.load_to_string (path);

        shader = new GLShader (context, @"$shared\n\n$main");
    }

    private void create_platform_filter () {
        var shared_path = "/org/gnome/World/Highscore2/display/glsl/platform/shared.glsl";
        string main_path;

        main_path = screen_set.get_filter_path ();

        if (main_path == null)
            main_path = "/org/gnome/World/Highscore2/display/glsl/platform/default.glsl";

        var shared = ResourceUtils.load_to_string (shared_path);
        var main = ResourceUtils.load_to_string (main_path);

        platform_shader = new GLShader (context, @"$shared\n\n$main");
    }

    private Gdk.Texture get_screen_texture (Screen screen) {
        int index = -1;

        for (int i = 0; i < screens.length; i++) {
            if (screens[i] == screen) {
                index = i;
                break;
            }
        }

        assert (index >= 0);

        var builder = new Gdk.GLTextureBuilder ();
        builder.context = context;
        builder.format = R8G8B8A8_PREMULTIPLIED;
        builder.width = (int) Math.ceil (last_width * screen.area.size.width);
        builder.height = (int) Math.ceil (last_height * screen.area.size.height);
        builder.id = framebuffers[index].get_texture_id ();

        return builder.build (null, null);
    }

    public Gdk.Texture take_screenshot () {
        var renderer = get_native ().get_renderer ();
        var snapshot = new Gtk.Snapshot ();

        float screenshot_width, screenshot_height;

        screen_layout.measure (
            runner.texture.width, runner.texture.height, screens,
            out screenshot_width, out screenshot_height
        );

        screenshot_width = Math.roundf (screenshot_width);
        screenshot_height = Math.roundf (screenshot_height);

        var layout_result = screen_layout.layout (
            {
                screenshot_width,
                screenshot_height,
                0, 0, 0, 0,
            },
            runner.texture.width / (float) runner.texture.height,
            screens
        );

        for (int i = 0; i < screens.length; i++) {
            var screen = screens[i];
            var result = layout_result[i];

            if (!result.visible)
                continue;

            var texture = get_screen_texture (screen);

            snapshot.save ();

            snapshot.translate ({
                screenshot_width / 2.0f, screenshot_height / 2.0f
            });
            snapshot.rotate (result.angle);
            snapshot.translate ({
                -screenshot_width / 2.0f, -screenshot_height / 2.0f
            });

            snapshot.transform (result.transform);

            snapshot.scale (1, -1);
            snapshot.translate ({ 0, -texture.height });

            Graphene.Rect rect = {
                { 0, 0 }, { result.width, result.height }
            };

            snapshot.append_texture (texture, rect);

            snapshot.restore ();
        }

        var node = snapshot.free_to_node ();

        return renderer.render_texture (
            node, {{ 0, 0 }, { screenshot_width, screenshot_height }}
        );
    }
}
