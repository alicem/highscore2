// This file is part of Highscore. License: GPL-3.0+.

using GL;

public class Highscore.GLVertexBuffer {
    private GLuint vbo;

    public GLVertexBuffer () {
        glGenBuffers (1, (GLuint[]) &vbo);
    }

    ~GLVertexBuffer () {
        glDeleteBuffers (1, (GLuint[]) &vbo);
    }

    public void bind () {
        glBindBuffer (GL_ARRAY_BUFFER, vbo);
    }

    public void unbind () {
        glBindBuffer (GL_ARRAY_BUFFER, 0);
    }

    public void set_data (float[] vertex_data) {
        glBufferData (
            GL_ARRAY_BUFFER,
            sizeof (GLfloat) * vertex_data.length,
            (GLvoid[]) vertex_data,
            GL_STATIC_DRAW
        );
    }
}
