// This file is part of Highscore. License: GPL-3.0+.

using GL;

public class Highscore.GLVertexArray : Object {
    private GLuint vao;

    public GLVertexArray () {
        glGenVertexArrays (1, (GLuint[]) &vao);
    }

    ~GLVertexArray () {
        glDeleteVertexArrays (1, (GLuint[]) &vao);
    }

    public void bind () {
        glBindVertexArray (vao);
    }

    public void unbind () {
        glBindVertexArray (0);
    }
}
