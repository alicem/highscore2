// This file is part of Highscore. License: GPL-3.0+.

using GL;

public class Highscore.GLTexture : Object {
    private GLuint tex;

    public GLTexture () {
        glGenTextures (1, (GLuint[]) &tex);
    }

    ~GLTexture () {
        glDeleteTextures (1, (GLuint[]) &tex);
    }

    public void bind () {
        glBindTexture (GL_TEXTURE_2D, tex);
    }

    public void unbind () {
        glBindTexture (GL_TEXTURE_2D, 0);
    }

    public void upload (Gdk.Texture texture) {
        var downloader = new Gdk.TextureDownloader (texture);

        var format = texture.get_format ();

        int pixel_size;
        GLenum gl_format, internal_format;

        switch (format) {
            case R8G8B8:
                pixel_size = 3;
                gl_format = GL_RGB;
                internal_format = GL_RGB;
                break;
            case R8G8B8A8_PREMULTIPLIED:
                pixel_size = 4;
                gl_format = GL_RGBA;
                internal_format = GL_RGBA;
                break;
            case B8G8R8A8_PREMULTIPLIED:
                pixel_size = 4;
                gl_format = GL_BGRA;
                internal_format = GL_RGBA;
                break;
            default:
                error ("Unknown texture format: %d", format);
        }

        downloader.set_format (format);

        size_t stride;
        var bytes = downloader.download_bytes (out stride);

        glPixelStorei (GL_UNPACK_ROW_LENGTH, (GLint) stride / pixel_size);
        glTexImage2D (
            GL_TEXTURE_2D,
            0,
            (GLint) internal_format,
            texture.width,
            texture.height,
            0,
            gl_format,
            GL_UNSIGNED_BYTE,
            (GLvoid[]) bytes.get_data ()
        );
        glPixelStorei (GL_UNPACK_ROW_LENGTH, 0);
    }

    public void set_params (GLenum min_filter, GLenum mag_filter, GLenum wrap) {
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, (GLint) wrap);
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, (GLint) wrap);
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (GLint) min_filter);
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, (GLint) mag_filter);
    }
}
