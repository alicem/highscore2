// This file is part of Highscore. License: GPL-3.0+.

using GL;

public class Highscore.GLFramebuffer : Object {
    private GLuint fbo;
    private GLuint texture;

    private bool texture_bound;

    public GLFramebuffer () {
        glGenFramebuffers (1, (GLuint[]) &fbo);
    }

    ~GLFramebuffer () {
        glDeleteFramebuffers (1, (GLuint[]) &fbo);

        glDeleteTextures (1, (GLuint[]) &texture);
    }

    public void bind (GLenum dest = GL_FRAMEBUFFER) {
        glBindFramebuffer (dest, fbo);
    }

    public void unbind (GLenum dest = GL_FRAMEBUFFER) {
        glBindFramebuffer (dest, 0);

        if (texture_bound) {
            glBindTexture (GL_TEXTURE_2D, 0);
            texture_bound = false;
        }
    }

    public void bind_texture () {
        glBindTexture (GL_TEXTURE_2D, texture);
        texture_bound = true;
    }

    public void unbind_texture () {
        glBindTexture (GL_TEXTURE_2D, 0);
        texture_bound = false;
    }

    public uint get_texture_id () {
        return texture;
    }

    public void create_texture (
        int width,
        int height,
        GLenum min_filter,
        GLenum mag_filter,
        GLenum wrap
    ) {
        if (texture > 0)
            glDeleteTextures (1, (GLuint[]) &texture);

        glGenTextures (1, (GLuint[]) &texture);
        glBindTexture (GL_TEXTURE_2D, texture);

        glTexImage2D (
            GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0,
            GL_RGBA, GL_UNSIGNED_BYTE, null
        );

        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (GLint) min_filter);
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, (GLint) mag_filter);
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, (GLint) wrap);
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, (GLint) wrap);

        glFramebufferTexture2D (
            GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0
        );

        glBindTexture (GL_TEXTURE_2D, 0);
    }
}
