// This file is part of Highscore. License: GPL-3.0+.

using GL;

public class Highscore.GLElementBuffer {
    private GLuint ebo;

    private int length;

    public GLElementBuffer () {
        glGenBuffers (1, (GLuint[]) &ebo);
    }

    ~GLElementBuffer () {
        glDeleteBuffers (1, (GLuint[]) &ebo);
    }

    public void bind () {
        glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, ebo);
    }

    public void unbind () {
        glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    public void set_data (uint[] element_data) {
        length = element_data.length;

        glBufferData (
            GL_ELEMENT_ARRAY_BUFFER,
            sizeof (GLuint) * length,
            (GLvoid[]) element_data,
            GL_STATIC_DRAW
        );
    }

    public void draw () {
        glDrawElements (GL_TRIANGLES, length, GL_UNSIGNED_INT, null);
    }
}
