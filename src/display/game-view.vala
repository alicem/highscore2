// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.GameView : Adw.Bin {
    private const int CURSOR_HIDE_TIMEOUT = 1000;

    public bool secondary { get; set; }
    public bool uses_pointer { get; private set; }

    public int top_inset { get; set; }
    public int bottom_inset { get; set; }
    public int left_inset { get; set; }
    public int right_inset { get; set; }

    private Runner _runner;
    public Runner runner {
        get { return _runner; }
        set {
            if (runner != null) {
                runner.weak_unref (runner_weak_notify_cb);

                if (uses_pointer) {
                    disconnect_pointer ();
                    uses_pointer = false;
                }

                runner.disconnect (uses_pointer_id);
                uses_pointer_id = 0;
            }

            _runner = value;

            if (runner != null) {
                runner.weak_ref (runner_weak_notify_cb);

                if (runner.uses_pointer) {
                    connect_pointer ();
                    uses_pointer = true;
                }

                uses_pointer_id = runner.notify["uses-pointer"].connect (() => {
                    if (runner.uses_pointer)
                        connect_pointer ();
                    else
                        disconnect_pointer ();

                    uses_pointer = runner.uses_pointer;
                });
            }
        }
    }

    private GLDisplay display;

    private ulong uses_pointer_id;

    private Gtk.EventControllerMotion? motion_controller;
    private Gtk.GestureDrag? drag_gesture;
    private int? active_screen_id;
    private Runner.CursorType current_cursor;

    private uint cursor_timeout_id;

    construct {
        focusable = true;

        overflow = HIDDEN;

        display = new GLDisplay ();
        bind_property ("runner",       display, "runner",       SYNC_CREATE);
        bind_property ("secondary",    display, "secondary",    SYNC_CREATE);
        bind_property ("top-inset",    display, "top-inset",    SYNC_CREATE);
        bind_property ("bottom-inset", display, "bottom-inset", SYNC_CREATE);
        bind_property ("left-inset",   display, "left-inset",   SYNC_CREATE);
        bind_property ("right-inset",  display, "right-inset",  SYNC_CREATE);

        child = display;

        var controller = new Gtk.EventControllerKey ();
        controller.key_pressed.connect ((keyval, keycode, state) => {
            if (runner == null)
                return Gdk.EVENT_PROPAGATE;

            return runner.key_pressed ((int) keycode - 8);
        });
        controller.key_released.connect ((keyval, keycode, state) => {
            if (runner == null)
                return;

            runner.key_released ((int) keycode - 8);
        });
        add_controller (controller);

        motion_controller = new Gtk.EventControllerMotion ();
        motion_controller.enter.connect ((x, y) => {
            if (uses_pointer && active_screen_id == null) {
                var screen = display.find_screen (x, y);
                if (screen != null)
                    update_cursor (runner.get_cursor (screen));
                else
                    update_cursor (DEFAULT);
            }

            if (current_cursor == DEFAULT)
                cursor_moved ();
        });
        motion_controller.motion.connect ((x, y) => {
            if (uses_pointer && active_screen_id == null) {
                var screen = display.find_screen (x, y);
                if (screen != null)
                    update_cursor (runner.get_cursor (screen));
                else
                    update_cursor (DEFAULT);
            }

            if (current_cursor == DEFAULT)
                cursor_moved ();
        });
        motion_controller.leave.connect (() => {
            if (uses_pointer && active_screen_id == null) {
                update_cursor (DEFAULT);
            }

            if (current_cursor == DEFAULT)
                display.cursor = null;
        });

        add_controller (motion_controller);
    }

    ~GameView () {
        if (cursor_timeout_id > 0)
            Source.remove (cursor_timeout_id);

        if (runner != null)
            runner.weak_unref (runner_weak_notify_cb);
    }

    static construct {
        set_css_name ("game-view");
    }

    private void runner_weak_notify_cb () {
        _runner = null;
        notify_property ("runner");
    }

    private void update_cursor (Runner.CursorType cursor) {
        if (current_cursor == cursor)
            return;

        current_cursor = cursor;

        switch (cursor) {
            case DEFAULT:
                display.cursor = null;
                break;
            case CROSSHAIR:
                display.cursor = new Gdk.Cursor.from_name ("crosshair", null);

                if (cursor_timeout_id > 0) {
                    Source.remove (cursor_timeout_id);
                    cursor_timeout_id = 0;
                }

                break;
            default:
                assert_not_reached ();
        }
    }

    private void cursor_moved () {
        if (current_cursor != DEFAULT)
            return;

        display.cursor = null;

        if (cursor_timeout_id > 0)
            Source.remove (cursor_timeout_id);

        cursor_timeout_id = Timeout.add_once (CURSOR_HIDE_TIMEOUT, () => {
            display.cursor = new Gdk.Cursor.from_name ("none", null);
            cursor_timeout_id = 0;
        });
    }

    private void connect_pointer () {
        if (uses_pointer)
            return;

        drag_gesture = new Gtk.GestureDrag ();
        drag_gesture.drag_begin.connect ((x, y) => {
            var screen = display.find_screen (x, y);

            if (screen == null) {
                drag_gesture.set_state (DENIED);
                return;
            }

            display.transform_coords (screen.id, x, y, out x, out y);

            if (runner.drag_begin (screen.id, x, y)) {
                active_screen_id = screen.id;
                drag_gesture.set_state (CLAIMED);
                update_cursor (runner.get_cursor (screen));
            } else {
                drag_gesture.set_state (DENIED);
            }
        });
        drag_gesture.drag_update.connect ((x, y) => {
            if (active_screen_id == null)
                return;

            double start_x, start_y;

            if (drag_gesture.get_start_point (out start_x, out start_y)) {
                x += start_x;
                y += start_y;
            }

            display.transform_coords (active_screen_id, x, y, out x, out y);
            runner.drag_update (x, y);
        });
        drag_gesture.drag_end.connect ((x, y) => {
            if (active_screen_id == null)
                return;

            double start_x, start_y;

            if (drag_gesture.get_start_point (out start_x, out start_y)) {
                x += start_x;
                y += start_y;
            }

            var screen = display.find_screen (x, y);
            display.transform_coords (active_screen_id, x, y, out x, out y);

            runner.drag_end (x, y);

            if (screen == null || screen.id != active_screen_id)
                update_cursor (DEFAULT);
            active_screen_id = null;
        });
        drag_gesture.cancel.connect (() => {
            if (active_screen_id == null)
                return;

            runner.drag_end (-1, -1);
            active_screen_id = null;
            update_cursor (DEFAULT);
        });
        add_controller (drag_gesture);
    }

    private void disconnect_pointer () {
        if (!uses_pointer)
            return;

        remove_controller (drag_gesture);
        drag_gesture = null;

        active_screen_id = null;
        update_cursor (DEFAULT);
    }

    public void show_snapshot (Snapshot snapshot) {
        display.show_snapshot (snapshot);
    }

    public Gdk.Texture take_screenshot () {
        return display.take_screenshot ();
    }
}
