// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/org/gnome/World/Highscore2/display/display-page.ui")]
public class Highscore.DisplayPage : Adw.NavigationPage {
    [GtkChild]
    private unowned Gtk.Stack stack;
    [GtkChild]
    private unowned Gtk.Stack header_stack;
    [GtkChild]
    private unowned Gtk.Revealer loading_revealer;
    [GtkChild]
    private unowned GameView view;
    [GtkChild]
    private unowned Adw.Bin platform_area;
    [GtkChild]
    private unowned Adw.StatusPage error_page;

    public Game game { get; construct; }
    public unowned Runner runner { get; private set; }
    public string window_title { get; private set; }
    public string subtitle { get; set; }
    public bool primary { get; set; }

    public bool loading { get; set; }

    private SecondaryWindow? secondary_window;

    private Settings settings;

    public DisplayPage (Game game) {
        Object (game: game);
    }

    construct {
        settings = new Settings ("org.gnome.World.Highscore2");

        settings.changed["pause-when-inactive"].connect (() => {
            if (get_is_active () || runner == null || !runner.running)
                return;

            if (settings.get_boolean ("pause-when-inactive"))
                runner.pause.begin ();
            else
                runner.resume.begin ();
        });

        notify["title"].connect (update_title);
        notify["subtitle"].connect (update_title);
        notify["loading"].connect (() => {
            header_stack.visible_child_name = loading ? "loading" : "game";
            loading_revealer.reveal_child = loading;
            update_actions ();
        });

        loading = true;

        game.bind_property ("title", this, "title", SYNC_CREATE);
    }

    protected override void map () {
        base.map ();

        var window = get_root () as Gtk.Window;

        window.notify["is-active"].connect (update_active);
        window.notify["visible-dialog"].connect (update_active);

        action_set_enabled ("game.exit", window is MainWindow);
    }

    protected override void unmap () {
        var window = get_root () as Gtk.Window;

        window.notify["is-active"].disconnect (update_active);
        window.notify["visible-dialog"].disconnect (update_active);

        base.unmap ();

        action_set_enabled ("game.exit", false);
    }

    private void update_actions () {
        bool error = stack.visible_child_name == "error";

        action_set_enabled ("game.reset",      !loading && !error);
        action_set_enabled ("game.save-state", !loading && !error);
        action_set_enabled ("game.load-state", !loading && !error);
        action_set_enabled ("game.screenshot", !loading && !error);
        action_set_enabled ("game.properties", !loading && !error);
    }

    private void set_page (string page) {
        if (page == "game") {
            stack.visible_child_name = "game";
            view.grab_focus ();
        } else if (page == "error") {
            stack.visible_child_name = "error";
        }

        update_actions ();
    }

    private async void save_and_pop () {
        if (loading && stack.visible_child_name == "game")
            return;

        yield save_and_close ();

        activate_action ("navigation.pop", null);
    }

    public async void save_and_close () {
        if (runner == null) {
            update_secondary_window ();
            return;
        }

        try {
            if (game.platform.platform == NINTENDO_64)
                yield runner.resume (); // TODO: fix the core
            yield runner.save_snapshot ();
            yield runner.stop ();
            view.runner = null;
            runner = null;
            update_secondary_window ();
        } catch (Error e) {
            critical ("Failed to save state and close: %s", e.message);
            view.runner = null;
            runner = null;
        }
    }

    private async void save_and_reset () {
        try {
            yield runner.save_snapshot ();
            yield runner.reset ();
        } catch (Error e) {
            critical ("Failed to save state and reset: %s", e.message);
        }

        view.grab_focus ();
    }

    private async void save () {
        try {
            yield runner.save_snapshot ();
        } catch (Error e) {
            critical ("Failed to save state: %s", e.message);
        }

        view.grab_focus ();
    }

    private async void load () {
        var snapshot = new Snapshot (game);

        try {
            yield runner.load_snapshot (snapshot, true);
            view.grab_focus ();
        } catch (Error e) {
            error_page.title = _("Failed to load save state");
            error_page.description = e.message;
            set_page ("error");
            view.runner = null;
            runner = null;
        }
    }

    static construct {
        install_action ("game.exit", null, widget => {
            var self = widget as DisplayPage;

            self.save_and_pop.begin ();
        });

        install_action ("game.reset", null, widget => {
            var self = widget as DisplayPage;

            self.save_and_reset.begin ();
        });

        install_action ("game.save-state", null, widget => {
            var self = widget as DisplayPage;

            self.save.begin ();
        });

        install_action ("game.load-state", null, widget => {
            var self = widget as DisplayPage;

            self.load.begin ();
        });

        install_action ("game.screenshot", null, widget => {
            var self = widget as DisplayPage;

            Screenshot.take_screenshot.begin (self.view);
        });

        install_action ("game.properties", null, widget => {
            var self = widget as DisplayPage;

            new PropertiesDialog (self.game).present (self);
        });
    }

    private void update_secondary_window () {
        if (runner != null && runner.screen_set.secondary_layout != null) {
            if (secondary_window != null)
                return;

            secondary_window = new SecondaryWindow (runner);
            secondary_window.notify["is-active"].connect (update_active);
            secondary_window.notify["visible-dialog"].connect (update_active);
            secondary_window.close_request.connect (() => {
                if (runner != null)
                    runner.secondary_view_closed ();
                secondary_window = null;

                return Gdk.EVENT_PROPAGATE;
            });
        } else {
            if (secondary_window == null)
                return;

            secondary_window.close ();
            secondary_window = null;
        }
    }

    public async void start_game () {
        bool load_state = settings.get_boolean ("load-state-on-startup");

        var snapshot = new Snapshot (game);
        if (snapshot.is_valid () && load_state)
            view.show_snapshot (snapshot);

        var cores = CoreRegister.get_register ().find_registrars_for_platform (
            game.platform.platform
        );

        if (cores.length == 0) {
            error_page.title = _("Unable to run the game");
            error_page.description = _("No matching core found");
            set_page ("error");
            return;
        }

        var runner = new Runner (cores[0].filename, game, snapshot);
        runner.stopped.connect (error => {
            view.runner = null;
            this.runner = null;

            if (error == null)
                return;

            error_page.title = _("Game Crashed");
            error_page.description = error;
            set_page ("error");
        });

        this.runner = runner;
        view.runner = runner;
        view.grab_focus ();

        runner.screen_set.secondary_layout_changed.connect (() => {
            update_secondary_window ();

            if (secondary_window != null)
                secondary_window.present ();
        });

        update_secondary_window ();

        try {
            yield runner.start ();
        } catch (Error e) {
            error_page.title = _("Unable to run the game");
            error_page.description = _("Failed to start the game: %s").printf (e.message);
            set_page ("error");
            try {
                yield runner.stop ();
            } catch (Error e2) {
                critical ("Failed to stop core: %s", e2.message);
            }
            view.runner = null;
            this.runner = null;
            update_secondary_window ();
            return;
        }

        try {
            if (!load_state || !yield runner.load_snapshot (snapshot, false))
                yield runner.reset ();
        } catch (Error e) {
            if (load_state && snapshot.is_valid ())
                error_page.title = _("Failed to load state");
            else
                error_page.title = _("Failed to reset game");

            error_page.description = e.message;
            set_page ("error");
            try {
                yield runner.stop ();
            } catch (Error e2) {
                critical ("Failed to stop core: %s", e2.message);
            }
            view.runner = null;
            this.runner = null;
            update_secondary_window ();
            return;
        }

        try {
            if (settings.get_boolean ("pause-when-inactive") && !get_is_active ())
                yield runner.pause ();
        } catch (Error e) {
            critical ("Failed to pause the game: %s", e.message);
        }

        platform_area.child = runner.create_header_widget ();
        platform_area.visible = platform_area.child != null;

        runner.bind_property ("view-name", this, "subtitle", SYNC_CREATE);

        runner.block_redraw = false;

        loading = false;

        if (secondary_window != null)
            secondary_window.present ();
    }

    private void update_active () {
        if (!settings.get_boolean ("pause-when-inactive"))
            return;

        if (runner == null || !runner.running)
            return;

        if (get_is_active ())
            runner.resume.begin ();
        else
            runner.pause.begin ();
    }

    private bool get_is_active () {
        assert (get_root () is Adw.ApplicationWindow);

        var root = get_root () as Adw.ApplicationWindow;

        if (root.is_active && root.visible_dialog == null)
            return true;

        if (secondary_window != null &&
            secondary_window.is_active &&
            secondary_window.visible_dialog == null) {
            return true;
        }

        return false;
    }

    [GtkCallback]
    private void spinner_map_cb (Gtk.Widget widget) {
        var spinner = widget as Gtk.Spinner;
        spinner.spinning = true;
    }

    [GtkCallback]
    private void spinner_unmap_cb (Gtk.Widget widget) {
        var spinner = widget as Gtk.Spinner;
        spinner.spinning = false;
    }

    private void update_title () {
        if (subtitle == null || subtitle == "")
            window_title = title;
        else
            window_title = _("%s (%s)").printf (title, subtitle);
    }
}
