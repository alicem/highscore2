// This file is part of Highscore. License: GPL-3.0-or-later

namespace Highscore.Screenshot {
    private async void take_screenshot (GameView view) {
        var texture = view.take_screenshot ();

        var dialog = new Gtk.FileDialog ();
        File? file = null;

        dialog.title = _("Save Screenshot");
        dialog.initial_name = "screenshot.png";

        try {
            file = yield dialog.save (view.get_root () as Gtk.Window, null);
        } catch (Gtk.DialogError e) {
            return;
        } catch (Error e) {
            critical ("Couldn't save screenshot: %s", e.message);
        }

        if (file == null)
            return;

        // Screenshot
        var downloader = new Gdk.TextureDownloader (texture);
        downloader.set_format (R8G8B8A8);

        size_t rowstride;
        var bytes = downloader.download_bytes (out rowstride);

        var pixbuf = new Gdk.Pixbuf.from_data (
            bytes.get_data (),
            RGB,
            true,
            8,
            texture.width,
            texture.height,
            (int) rowstride
        );

        try {
            pixbuf.save (file.get_path (), "png", null);
        } catch (Error e) {
            critical ("Failed to save screenshot: %s", e.message);
        }
    }
}
