// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/org/gnome/World/Highscore2/welcome-dialog.ui")]
public class Highscore.WelcomeDialog : Adw.Dialog {
    [GtkChild]
    private unowned Gtk.Stack stack;
    [GtkChild]
    private unowned Gtk.Label progress_label;

    static construct {
        install_action ("welcome.select-library-dir", null, widget => {
            var dialog = widget as WelcomeDialog;

            dialog.select_library_folder.begin ();
        });
    }

    private async void select_library_folder () {
        var dialog = new Gtk.FileDialog () {
            title = _("Select Library Folder"),
            modal = true,
        };

        File folder = null;

        var window = get_root () as Gtk.Window;

        try {
            folder = yield dialog.select_folder (window, null);
        } catch (Gtk.DialogError e) {
            return;
        } catch (Error e) {
            critical ("Failed to select library folder: %s", e.message);
            return;
        }

        var settings = new Settings ("org.gnome.World.Highscore2");

        settings.set_string ("library-dir", folder.get_path ());

        stack.visible_child_name = "progress";

        var library = Library.get_instance ();

        update_scan_progress (0);

        yield library.rescan (update_scan_progress);

        close ();
    }

    private void update_scan_progress (uint n_games) {
        progress_label.label = ngettext (
            "%u game scanned",
            "%u games scanned",
            n_games
        ).printf (n_games);
    }

    [GtkCallback]
    private void spinner_map_cb (Gtk.Widget widget) {
        var spinner = widget as Gtk.Spinner;
        spinner.spinning = true;
    }

    [GtkCallback]
    private void spinner_unmap_cb (Gtk.Widget widget) {
        var spinner = widget as Gtk.Spinner;
        spinner.spinning = false;
    }
}
