// This file is part of Highscore. License: GPL-3.0+.

public interface Highscore.GameMetadata : Object {
    public abstract VariantType serialize_type ();

    public abstract Variant serialize ();

    public abstract void deserialize (Variant variant);

    public static GameMetadata? create (Platform platform) {
        var type = platform.metadata_type;

        if (type == Type.NONE && platform.parent != null)
            type = platform.parent.metadata_type;

        if (type == Type.NONE)
            return null;

        return Object.new (type) as GameMetadata;
    }
}
