// This file is part of Highscore. License: GPL-3.0+.

private class Highscore.CoreRegistrar : Object {
    public string filename { get; private set; }
    public string name { get; private set; }
    public string[] copyright { get; private set; }
    public string license { get; private set; }

    private Hs.Platform[] platforms;

    public CoreRegistrar (string core_filename) throws CoreError {
        assert (Module.supported ());

        filename = core_filename;

        try {
            var keyfile_path = Path.build_filename (
                Config.CORES_DIR,
                @"$core_filename.highscore"
            );

            var keyfile = new KeyFile ();
            keyfile.load_from_file (keyfile_path, KeyFileFlags.NONE);
            name = keyfile.get_string ("Highscore", "Name");
            copyright = keyfile.get_string_list ("Highscore", "Copyright");
            license = keyfile.get_string ("Highscore", "License");
            var platform_names = keyfile.get_string_list ("Highscore", "Platforms");

            platforms = {};

            foreach (var platform_name in platform_names)
                platforms += Hs.Platform.get_from_name (platform_name);
        }
        catch (Error e) {
            throw new CoreError.INVALID_CORE_DESCRIPTOR ("Invalid core descriptor: %s", e.message);
        }
    }

    public Hs.Platform[] get_platforms () {
        return platforms;
    }

    public static int compare (CoreRegistrar a, CoreRegistrar b) {
        return a.name.collate (b.name);
    }
}

private errordomain Highscore.CoreError {
    INVALID_CORE_DESCRIPTOR,
    NOT_A_CORE,
}
