// This file is part of Highscore. License: GPL-3.0+.

private class Highscore.GameCover : Gtk.Widget {
    public Game game { get; set; }
    public bool width_for_height { get; set; }

    private Gtk.Image image;
    private SnapshotThumbnail thumbnail;

    construct {
        overflow = HIDDEN;

        image = new Gtk.Image () {
            halign = CENTER,
            valign = CENTER,
        };
        image.add_css_class ("empty");
        image.set_parent (this);

        thumbnail = new SnapshotThumbnail ();
        thumbnail.set_parent (this);
        thumbnail.visible = false;

        notify["game"].connect (() => {
            Snapshot snapshot = null;
            if (game != null) {
                image.icon_name = game.platform.icon_name;

                snapshot = new Snapshot (game);

                if (!snapshot.is_valid ())
                    snapshot = null;
            }

            thumbnail.snapshot = snapshot;
            thumbnail.visible = snapshot != null;
            image.visible = snapshot == null;
        });

        notify["width-for-height"].connect (queue_resize);
    }

    static construct {
        set_css_name ("game-cover");
    }

    protected override void dispose () {
        if (image != null) {
            image.unparent ();
            image = null;
        }

        if (thumbnail != null) {
            thumbnail.unparent ();
            thumbnail = null;
        }

        base.dispose ();
    }

    protected override Gtk.SizeRequestMode get_request_mode () {
        if (width_for_height)
            return WIDTH_FOR_HEIGHT;
        else
            return HEIGHT_FOR_WIDTH;
    }

    protected override void measure (
        Gtk.Orientation orientation,
        int for_size,
        out int minimum,
        out int natural,
        out int minimum_baseline,
        out int natural_baseline
    ) {
        if (image.visible)
            image.measure (orientation, for_size, out minimum, null, null, null);
        else if (thumbnail.visible)
            thumbnail.measure (orientation, for_size, out minimum, null, null, null);
        else
            assert_not_reached ();

        minimum = int.max (minimum, 64);

        if ((orientation == HORIZONTAL) == width_for_height)
            natural = for_size >= 0 ? for_size : 64;
        else
            natural = int.min (minimum, 64);

        minimum_baseline = natural_baseline = -1;
    }

    protected override void size_allocate (int width, int height, int baseline) {
        if (image.visible)
            image.allocate (width, height, baseline, null);

         if (thumbnail.visible)
            thumbnail.allocate (width, height, baseline, null);
   }
}
