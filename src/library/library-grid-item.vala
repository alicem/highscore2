// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/org/gnome/World/Highscore2/library/library-grid-item.ui")]
public class Highscore.LibraryGridItem : ContextMenuBin {
    public Game game { get; set; }

    static construct {
        install_action ("game.open-in-new-window", null, widget => {
            var self = widget as LibraryGridItem;
            self.open_in_new_window ();
        });

        install_action ("game.properties", null, widget => {
            var self = widget as LibraryGridItem;
            self.open_properties ();
        });

        set_css_name ("library-grid-item");
    }

    private void open_in_new_window () {
        var app = GLib.Application.get_default () as Application;

        app.run_game_standalone (game);
    }

    private void open_properties () {
        new PropertiesDialog (game).present (this);
    }
}
