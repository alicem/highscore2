// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.GameScanner : Object {
    public delegate void GameCallback (Game game);

    private File library_dir;
    private unowned GameCallback callback;

    private Cancellable cancellable;

    public GameScanner (File library_dir, GameCallback callback) {
        this.library_dir = library_dir;
        this.callback = callback;
    }

    public void scan () {
        cancellable = new Cancellable ();

        scan_directory (library_dir);
    }

    public void stop () {
        cancellable.cancel ();
    }

    private void scan_directory (File directory) {
        FileEnumerator enumerator;

        try {
            enumerator = directory.enumerate_children (
                "standard::*",
                NOFOLLOW_SYMLINKS,
                cancellable
            );
        } catch (Error e) {
            critical (
                "Failed to index directory %s: %s",
                directory.get_path (), e.message
            );
            return;
        }

        FileInfo info = null;

        while (true) {
            try {
                info = enumerator.next_file (cancellable);
            } catch (Error e) {
                critical (
                    "Failed to enumerate file: %s", e.message
                );
            }

            if (info == null || cancellable.is_cancelled ())
                break;

            var name = info.get_name ();
            var file_type = info.get_file_type ();

            if (file_type == DIRECTORY) {
                scan_directory (directory.get_child (name));
            } else {
                scan_file (directory.get_child (name), info);
            }
        }
    }

    private void scan_file (File file, FileInfo info) {
        var content_type = info.get_content_type ();
        var mime_type = ContentType.get_mime_type (content_type);

        var register = PlatformRegister.get_register ();
        var platforms = register.list_platforms_for_mime_type (mime_type);
        Game game = null;

        foreach (var platform in platforms) {
            try {
                var parser = GameParser.create (platform, file);

                if (parser.parse ()) {
                    game = parser.create_game ();
                    break;
                }
            } catch (Error e) {
                critical (
                    "Failed to parse the game %s: %s\n",
                    file.get_path (), e.message
                );
                return;
            }
        }

        if (game == null)
            return;

        callback (game);
    }
}
