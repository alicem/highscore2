// This file is part of Highscore. License: GPL-3.0+.

private class Highscore.Database : Object {
    public delegate void GameLoadedCallback (Game game);

    private Sqlite.Database database;

    private const string CREATE_GAMES_TABLE_QUERY = """
        CREATE TABLE IF NOT EXISTS games (
            id INTEGER PRIMARY KEY NOT NULL,
            uid TEXT NOT NULL UNIQUE,
            title TEXT NOT NULL,
            platform TEXT NOT NULL,
            path TEXT NOT NULL,
            extra_paths TEXT NULL,
            metadata TEXT NULL
        );
    """;

    private const string CREATE_TITLES_TABLE_QUERY = """
        CREATE TABLE IF NOT EXISTS titles (
            id INTEGER PRIMARY KEY NOT NULL,
            uid TEXT NOT NULL UNIQUE,
            title TEXT NOT NULL
        );
    """;

    private const string CREATE_GAMES_UID_INDEX_QUERY = """
        CREATE UNIQUE INDEX IF NOT EXISTS idx_games_uid ON games (uid);
    """;

    private const string CREATE_TITLES_UID_INDEX_QUERY = """
        CREATE UNIQUE INDEX IF NOT EXISTS idx_titles_uid ON titles (uid);
    """;

    private const string ADD_GAME_QUERY = """
        INSERT INTO games (
            uid, title, platform, path, extra_paths, metadata
        ) VALUES (
            $UID, $TITLE, $PLATFORM, $PATH, $EXTRA_PATHS, $METADATA
        );
    """;

    private const string UPDATE_GAME_QUERY = """
        UPDATE games SET
            title = $TITLE,
            platform = $PLATFORM,
            path = $PATH,
            extra_paths = $EXTRA_PATHS,
            metadata = $METADATA
        WHERE uid = $UID;
    """;

    private const string REMOVE_GAME_QUERY = """
        DELETE FROM games WHERE uid = $UID;
    """;

    private const string LIST_GAMES_QUERY = """
        SELECT
            games.uid,
            titles.title,
            games.title,
            platform,
            path,
            extra_paths,
            metadata
        FROM games LEFT JOIN titles ON games.uid == titles.uid
        ORDER BY games.uid;
    """;

    private const string ADD_TITLE_QUERY = """
        INSERT INTO titles (uid, title) VALUES ($UID, $TITLE);
    """;

    private const string UPDATE_TITLE_QUERY = """
        UPDATE titles SET title = $TITLE WHERE uid = $UID;
    """;

    private const string REMOVE_TITLE_QUERY = """
        DELETE FROM titles WHERE uid = $UID;
    """;

    private const string FIND_TITLE_QUERY = """
        SELECT title FROM titles WHERE uid = $UID;
    """;

    private Sqlite.Statement add_game_query;
    private Sqlite.Statement update_game_query;
    private Sqlite.Statement remove_game_query;
    private Sqlite.Statement list_games_query;
    private Sqlite.Statement add_title_query;
    private Sqlite.Statement update_title_query;
    private Sqlite.Statement remove_title_query;
    private Sqlite.Statement find_title_query;

    public Database (string path) throws DatabaseError {
        var db_file = File.new_for_path (path);

        try {
            db_file.get_parent ().make_directory_with_parents ();
        } catch (IOError.EXISTS e) {
        } catch (Error e) {
            throw new DatabaseError.COULDNT_OPEN ("Failed to create the directory: %s", e.message);
        }

        if (Sqlite.Database.open (path, out database) != Sqlite.OK)
            throw new DatabaseError.COULDNT_OPEN ("Couldn’t open the database for “%s”.", path);

        DatabaseUtils.exec (database, CREATE_GAMES_TABLE_QUERY, null);
        DatabaseUtils.exec (database, CREATE_GAMES_UID_INDEX_QUERY, null);

        DatabaseUtils.exec (database, CREATE_TITLES_TABLE_QUERY, null);
        DatabaseUtils.exec (database, CREATE_TITLES_UID_INDEX_QUERY, null);

        add_game_query = DatabaseUtils.prepare (database, ADD_GAME_QUERY);
        update_game_query = DatabaseUtils.prepare (database, UPDATE_GAME_QUERY);
        remove_game_query = DatabaseUtils.prepare (database, REMOVE_GAME_QUERY);
        list_games_query = DatabaseUtils.prepare (database, LIST_GAMES_QUERY);
        add_title_query = DatabaseUtils.prepare (database, ADD_TITLE_QUERY);
        update_title_query = DatabaseUtils.prepare (database, UPDATE_TITLE_QUERY);
        remove_title_query = DatabaseUtils.prepare (database, REMOVE_TITLE_QUERY);
        find_title_query = DatabaseUtils.prepare (database, FIND_TITLE_QUERY);
    }

    private string? serialize_extra_paths (Game game) {
        var paths = game.extra_paths;

        if (paths == null)
            return null;

        return new Variant.bytestring_array (paths).print (false);
    }

    private string? serialize_metadata (Game game) {
        if (game.metadata == null)
            return null;

        var variant = game.metadata.serialize ();

        if (variant == null)
            return null;

        return variant.print (false);
    }

    private string[]? deserialize_extra_paths (string? str) throws DatabaseError {
        if (str == null)
            return null;

        Variant variant;

        try {
            variant = Variant.parse (VariantType.BYTESTRING_ARRAY, str);
         } catch (VariantParseError e) {
            throw new DatabaseError.INVALID_GAME (
                "Failed to parse extra paths '%s': %s", str, e.message
            );
        }

        if (variant == null)
            return null;

        return variant.get_bytestring_array ();
    }

    private GameMetadata? deserialize_metadata (Platform platform, string? str) throws DatabaseError {
        var metadata = GameMetadata.create (platform);
        if (metadata != null) {
            Variant variant;

            try {
                var type = metadata.serialize_type ();
                variant = Variant.parse (type, str);
            } catch (VariantParseError e) {
                throw new DatabaseError.INVALID_GAME (
                    "Failed to parse metadata '%s': %s", str, e.message
                );
            }

            metadata.deserialize (variant);
        }

        return metadata;
    }

    public bool add_game (Game game) throws DatabaseError {
        var uid = game.uid;
        var default_title = game.default_title;
        var platform = game.platform.id;
        var path = game.file.get_path ();
        var extra_paths = serialize_extra_paths (game);
        var metadata = serialize_metadata (game);

        add_game_query.reset ();
        DatabaseUtils.bind_text (add_game_query, "$UID", uid);
        DatabaseUtils.bind_text (add_game_query, "$TITLE", default_title);
        DatabaseUtils.bind_text (add_game_query, "$PLATFORM", platform);
        DatabaseUtils.bind_text (add_game_query, "$PATH", path);
        DatabaseUtils.bind_text (add_game_query, "$EXTRA_PATHS", extra_paths);
        DatabaseUtils.bind_text (add_game_query, "$METADATA", metadata);

        var result = add_game_query.step ();

        if (result == Sqlite.CONSTRAINT)
            return false;

        if (result != Sqlite.DONE) {
            throw new DatabaseError.EXECUTION_FAILED (
                "Couldn't add game (%s, %s, %s, %s, %s, %s)",
                uid, default_title, platform, path, extra_paths, metadata
            );
        }

        return true;
    }

    public void update_game (Game game) throws DatabaseError {
        var uid = game.uid;
        var default_title = game.default_title;
        var platform = game.platform.id;
        var path = game.file.get_path ();
        var extra_paths = serialize_extra_paths (game);
        var metadata = serialize_metadata (game);

        update_game_query.reset ();
        DatabaseUtils.bind_text (update_game_query, "$UID", uid);
        DatabaseUtils.bind_text (update_game_query, "$TITLE", default_title);
        DatabaseUtils.bind_text (update_game_query, "$PLATFORM", platform);
        DatabaseUtils.bind_text (update_game_query, "$PATH", path);
        DatabaseUtils.bind_text (update_game_query, "$EXTRA_PATHS", extra_paths);
        DatabaseUtils.bind_text (update_game_query, "$METADATA", metadata);

        if (update_game_query.step () != Sqlite.DONE) {
            throw new DatabaseError.EXECUTION_FAILED (
                "Couldn't update game (%s, %s, %s, %s, %s, %s)",
                uid, default_title, platform, path, extra_paths, metadata
            );
        }
    }

    public void remove_game (Game game) throws DatabaseError {
        remove_game_by_uid (game.uid);
    }

    public void load_games (
        GameLoadedCallback game_callback
    ) throws DatabaseError {
        list_games_query.reset ();

        string[] invalid_games = {};

        while (list_games_query.step () == Sqlite.ROW) {
            var uid = list_games_query.column_text (0);
            var title = list_games_query.column_text (1);
            var default_title = list_games_query.column_text (2);
            var platform_id = list_games_query.column_text (3);
            var path = list_games_query.column_text (4);
            var extra_paths = list_games_query.column_text (5);
            var metadata = list_games_query.column_text (6);

            if (title == null)
                title = default_title;

            try {
                var game = create_game (
                    uid, title, default_title, platform_id,
                    path, extra_paths, metadata
                );

                game_callback (game);
            } catch (DatabaseError.INVALID_GAME error) {
                debug ("Couldn't load game %s: %s", title, error.message);
                invalid_games += uid;
            }
        }

        foreach (var uid in invalid_games)
            remove_game_by_uid (uid);
    }

    private void remove_game_by_uid (string uid) throws DatabaseError {
        remove_game_query.reset ();

        DatabaseUtils.bind_text (remove_game_query, "$UID", uid);

        var result = remove_game_query.step ();
        if (result == Sqlite.ROW)
            return;

        if (result != Sqlite.DONE)
            throw new DatabaseError.EXECUTION_FAILED ("Failed to delete game '%s'", uid);
    }

    private Game create_game (
        string uid,
        string title,
        string default_title,
        string platform_id,
        string path,
        string? extra_paths_str,
        string? metadata_str
    ) throws DatabaseError {
        var platform = PlatformRegister.get_register ().get_platform (platform_id);
        if (platform == null)
            throw new DatabaseError.INVALID_GAME ("Unknown platform: %s", platform_id);

        var file = File.new_for_path (path);
        if (!file.query_exists ())
            throw new DatabaseError.INVALID_GAME ("File doesn't exist %s", path);

        var extra_paths = deserialize_extra_paths (extra_paths_str);

        foreach (var extra_path in extra_paths) {
            var extra_file = File.new_for_path (extra_path);
            if (!extra_file.query_exists ())
                throw new DatabaseError.INVALID_GAME ("Extra file doesn't exist %s", path);
        }

        var metadata = deserialize_metadata (platform, metadata_str);

        return new Game (file, platform, uid, title, default_title, metadata, extra_paths);
    }

    public void rename_game (Game game, string title) throws DatabaseError {
        add_title_query.reset ();
        DatabaseUtils.bind_text (add_title_query, "$UID", game.uid);
        DatabaseUtils.bind_text (add_title_query, "$TITLE", title);

        var result = add_title_query.step ();

        if (result == Sqlite.CONSTRAINT) {
            update_title_query.reset ();
            DatabaseUtils.bind_text (update_title_query, "$UID", game.uid);
            DatabaseUtils.bind_text (update_title_query, "$TITLE", title);

            result = update_title_query.step ();

            if (result != Sqlite.DONE) {
                throw new DatabaseError.EXECUTION_FAILED (
                    "Couldn't rename game '%s' to '%s'",
                    game.title, title
                );
            }

            return;
        }

        if (result != Sqlite.DONE) {
            throw new DatabaseError.EXECUTION_FAILED (
                "Couldn't rename game '%s' to '%s'",
                game.title, title
            );
        }
    }

    public void reset_game_title (Game game) throws DatabaseError {
        remove_title_query.reset ();
        DatabaseUtils.bind_text (remove_title_query, "$UID", game.uid);

        if (remove_title_query.step () != Sqlite.DONE) {
            throw new DatabaseError.EXECUTION_FAILED (
                "Couldn't reset title for the game '%s'",
                game.title
            );
        }
    }

    public string? find_game_title (Game game) throws Error {
        find_title_query.reset ();
        DatabaseUtils.bind_text (find_title_query, "$UID", game.uid);

        if (find_title_query.step () == Sqlite.ROW)
            return find_title_query.column_text (0);

        return null;
    }
}
