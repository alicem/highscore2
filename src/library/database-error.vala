// This file is part of Highscore. License: GPL-3.0+.

public errordomain Highscore.DatabaseError {
    COULDNT_OPEN,
    EXECUTION_FAILED,
    PREPARATION_FAILED,
    BINDING_FAILED,
    INVALID_GAME,
}
