// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Library : Object {
    public delegate void ScanProgressCallback (uint n_games);

    public signal void game_renamed (Game game);

    private static Library instance;

    private Database database;

    private ListStore games;
    private GenericSet<Game> games_set;

    public LibraryModel model { get; private set; }

    public static Library get_instance () {
        if (instance == null)
            instance = new Library ();

        return instance;
    }

    construct {
        var data_dir = Environment.get_user_data_dir ();
        var db_path = Path.build_filename (data_dir, "highscore", "library.sqlite3");

        try {
            database = new Database (db_path);
        } catch (DatabaseError e) {
            error ("Failed to create the database: %s", e.message);
        }

        model = new LibraryModel ();
    }

    public async void load_games () {
        var games = new ListStore (typeof (Game));
        var games_set = new GenericSet<Game> (Game.hash, Game.equal);

        new Thread<void> ("library-load-thread", () => {
            try {
                database.load_games (game => {
                    games.append (game);
                    games_set.add (game);
                });
            } catch (DatabaseError e) {
                critical ("Failed to load games from the database: %s", e.message);
            }

            load_games.callback ();
        });

        yield;

        model.set_games (games);
        this.games = games;
        this.games_set = games_set;
    }

    public async void rescan (ScanProgressCallback? callback) {
        var settings = new Settings ("org.gnome.World.Highscore2");
        var library_path = settings.get_string ("library-dir");
        var library_dir = File.new_for_path (library_path);

        var new_games = new ListStore (typeof (Game));
        var new_games_set = new GenericSet<Game> (Game.hash, Game.equal);
        uint n_games = 0;

        new Thread<void> ("library-scan-thread", () => {
            var scanner = new GameScanner (library_dir, game => {
                new_games.append (game);
                new_games_set.add (game);

                try {
                    if (has_game (game) || !database.add_game (game))
                        database.update_game (game);
                } catch (DatabaseError e) {
                    critical ("Failed to add game: %s", e.message);
                }

                AtomicUint.set (ref n_games, new_games.get_n_items ());

                if (callback != null) {
                    Idle.add_once (() => {
                        callback (AtomicUint.get (ref n_games));
                    });
                }
            });

            scanner.scan ();

            new_games.sort ((CompareDataFunc<Object>) Game.compare_id);

            int i = 0, j = 0;

            if (games != null) {
                uint n = games.get_n_items ();
                while (i < n) {
                    var old_game = games.get_item (i) as Game;

                    if (j >= new_games.get_n_items ()) {
                        i++;
                        try {
                            database.remove_game (old_game);
                        } catch (DatabaseError e) {
                            critical ("Failed to remove game: %s", e.message);
                        }
                        continue;
                    }

                    var new_game = new_games.get_item (j) as Game;

                    if (old_game.uid == new_game.uid) {
                        i++;
                        j++;

                        if (old_game.title != old_game.default_title)
                            new_game.title = old_game.title;

                        continue;
                    }

                    if (strcmp (new_game.uid, old_game.uid) > 0) {
                        i++;
                        try {
                            database.remove_game (old_game);
                        } catch (DatabaseError e) {
                            critical ("Failed to remove game: %s", e.message);
                        }
                        continue;
                    }

                    j++;
                }
            }

            rescan.callback ();
        });

        yield;

        model.set_games (new_games);
        games = new_games;
        games_set = new_games_set;
    }

    public bool has_game (Game game) {
        if (games_set == null)
            return false;

        return game in games_set;
    }

    public async void import_game (Game game) throws Error {
        var settings = new Settings ("org.gnome.World.Highscore2");
        var library_path = settings.get_string ("library-dir");

        var library_dir = File.new_for_path (library_path);

        bool needs_copy = false;

        if (!game.file.has_prefix (library_dir))
            needs_copy = true;

        if (!needs_copy && game.extra_paths != null) {
            foreach (var extra_path in game.extra_paths) {
                var extra_file = File.new_for_path (extra_path);

                if (!extra_file.has_prefix (library_dir)) {
                    needs_copy = true;
                    break;
                }
            }
        }

        File dest;
        string[]? extra_paths = null;

        if (needs_copy) {
            var imports_dir = FileUtils.get_directory_safely (library_dir, "Imports");
            var platform_dir = FileUtils.get_directory_safely (imports_dir, game.platform.name);

            if (game.extra_paths != null) {
                var game_dir = FileUtils.get_directory_safely (platform_dir, game.title);

                if (!game_dir.query_exists ())
                    game_dir.make_directory_with_parents ();

                extra_paths = {};

                foreach (var extra_path in game.extra_paths) {
                    var extra_file = File.new_for_path (extra_path);
                    var extra_dest = game_dir.get_child (extra_file.get_basename ());

                    yield extra_file.copy_async (extra_dest, NONE);

                    extra_paths += extra_dest.get_path ();
                }

                dest = game_dir.get_child (game.file.get_basename ());
                yield game.file.copy_async (dest, NONE);
            } else {
                if (!platform_dir.query_exists ())
                    platform_dir.make_directory_with_parents ();

                var rom_name = game.file.get_basename ();
                dest = FileUtils.get_file_with_unique_name (platform_dir, rom_name);

                yield game.file.copy_async (dest, NONE);
            }
        } else {
            dest = game.file;
            extra_paths = game.extra_paths;
        }

        var imported_game = new Game (
            dest,
            game.platform,
            game.uid,
            game.title,
            game.default_title,
            game.metadata,
            extra_paths
        );

        database.add_game (imported_game);

        uint pos = games.insert_sorted (imported_game, (CompareDataFunc<Object>) Game.compare_id);
        games_set.add (imported_game);

        model.items_changed (pos, 0, 1);
    }

    public void rename_game (Game game, string? title) throws Error {
        if (title == null)
            title = game.default_title;

        if (title == game.default_title)
            database.reset_game_title (game);
        else
            database.rename_game (game, title);

        game.title = title;

        game_renamed (game);
    }

    public string? find_title (Game game) throws Error {
        return database.find_game_title (game);
    }
}
