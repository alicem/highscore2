// This file is part of Highscore. License: GPL-3.0+.

namespace Highscore.LibraryUtils {
    public bool has_library_dir () {
        var settings = new Settings ("org.gnome.World.Highscore2");

        return settings.get_string ("library-dir") != "";
    }
}
