// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/org/gnome/World/Highscore2/library/library-page.ui")]
public class Highscore.LibraryPage : Adw.NavigationPage {
    public signal void game_activated (Game game);

    [GtkChild]
    private unowned Gtk.Stack stack;
    [GtkChild]
    private unowned Gtk.Label progress_label;
    [GtkChild]
    private unowned Adw.StatusPage empty_page;
    [GtkChild]
    private unowned Adw.OverlaySplitView split_view;
    [GtkChild]
    private unowned Gtk.ListBox sidebar;
    [GtkChild]
    private unowned Adw.WindowTitle content_title;
    [GtkChild]
    private unowned Gtk.SearchBar search_bar;
    [GtkChild]
    private unowned Gtk.SearchEntry search_entry;
    [GtkChild]
    private unowned Gtk.Stack search_stack;
    [GtkChild]
    private unowned Gtk.GridView grid_view;

    private Gtk.ListBoxRow all_games_row;

    private Gtk.StringFilter search_filter;
    private Gtk.CustomFilter platform_filter;
    private Gtk.CustomSorter game_sorter;

    public bool compact { get; set; }
    public bool search_mode { get; set; }

    private LibraryModel model;

    private Platform? current_platform;
    private bool can_type_to_search;

    private ulong model_items_changed_id;
    private ulong model_notify_empty_id;

    construct {
        empty_page.icon_name = @"$(Config.APPLICATION_ID)-symbolic";

        notify["compact"].connect (() => {
            if (compact)
                grid_view.add_css_class ("compact");
            else
                grid_view.remove_css_class ("compact");
        });

        search_bar.connect_entry (search_entry);

        var expr = new Gtk.PropertyExpression (typeof (Game), null, "title");
        search_filter = new Gtk.StringFilter (expr);
        platform_filter = new Gtk.CustomFilter (item => {
            var game = item as Game;

            return current_platform == null || game.platform == current_platform;
        });

        game_sorter = new Gtk.CustomSorter ((CompareDataFunc<Object>) Game.compare);

        all_games_row = make_sidebar_row (null);
        sidebar.append (all_games_row);

        var platforms = PlatformRegister.get_register ().get_all_platforms ();
        foreach (var platform in platforms) {
            var row = make_sidebar_row (platform);

            row.set_data<Platform> ("platform", platform);

            sidebar.append (row);
        }

        sidebar.set_header_func ((row, before) => {
            if (before == all_games_row)
                row.set_header (new Gtk.Separator (HORIZONTAL));
            else
                row.set_header (null);
        });

        sidebar.select_row (all_games_row);
        sidebar_row_activated_cb (all_games_row);

        Library.get_instance ().game_renamed.connect (() => {
            game_sorter.changed (DIFFERENT);
        });
    }

    static construct {
        install_action ("library.open-folder", null, widget => {
            var self = widget as LibraryPage;
            self.open_library_folder ();
        });

        install_action ("library.rescan", null, widget => {
            var self = widget as LibraryPage;
            self.rescan.begin ();
        });

        install_property_action ("library.search", "search-mode");

        add_binding_action (
            Gdk.Key.F, Gdk.ModifierType.CONTROL_MASK, "library.search", null
        );
    }

    protected override void map () {
        base.map ();

        if (can_type_to_search)
            search_bar.key_capture_widget = get_root () as Gtk.Widget;
    }

    protected override void shown () {
        if (get_mapped ())
            search_bar.key_capture_widget = get_root () as Gtk.Widget;

        can_type_to_search = true;
    }

    protected override void hiding () {
        can_type_to_search = false;

        search_bar.key_capture_widget = null;
    }

    private Gtk.ListBoxRow make_sidebar_row (Platform? platform) {
        string name, icon_name;

        if (platform != null) {
            name = platform.name;
            icon_name = platform.icon_name;
        } else {
            name = _("All Games");
            icon_name = "all-games-symbolic";
        }

        var icon = new Gtk.Image.from_icon_name (icon_name) {
            pixel_size = 24,
        };
        icon.add_css_class ("sidebar-icon");

        var label = new Gtk.Label (name) {
            xalign = 0,
            ellipsize = END,
        };

        var box = new Gtk.Box (HORIZONTAL, 12) {
            margin_start = 3,
            margin_top = 9,
            margin_bottom = 9,
        };
        box.append (icon);
        box.append (label);

        return new Gtk.ListBoxRow () {
            child = box,
        };
    }

    [GtkCallback]
    private void grid_activate_cb (uint position) {
        var model = grid_view.model;

        var game = model.get_item (position) as Game;

        game_activated (game);
    }

    private void open_library_folder () {
        var settings = new Settings ("org.gnome.World.Highscore2");

        var library_path = settings.get_string ("library-dir");
        var library_file = File.new_for_path (library_path);

        var root = get_root ();

        if (root == null)
            return;

        var window = root as Gtk.Window;

        var launcher = new Gtk.FileLauncher (library_file);

        launcher.launch.begin (window, null);
    }

    private void update_scan_progress (uint n_games) {
        progress_label.label = ngettext (
            "%u game scanned",
            "%u games scanned",
            n_games
        ).printf (n_games);
    }

    private async void rescan () {
        var library = Library.get_instance ();

        progress_label.visible = true;
        update_scan_progress (0);

        yield WidgetUtils.change_stack_child_by_name (stack, "loading");

        if (model_items_changed_id > 0) {
            model.disconnect (model_items_changed_id);
            model_items_changed_id = 0;
        }
        if (model_notify_empty_id > 0) {
            model.disconnect (model_notify_empty_id);
            model_notify_empty_id = 0;
        }

        grid_view.model = null;
        model = null;

        yield library.rescan (update_scan_progress);

        show_games ();
    }

    public void show_games () {
        if (grid_view.model != null)
            return;

        model = Library.get_instance ().model;
        var sort_model = new Gtk.SortListModel (model, game_sorter);
        var multi_filter = new Gtk.EveryFilter ();
        multi_filter.append (platform_filter);
        multi_filter.append (search_filter);
        var filter_model = new Gtk.FilterListModel (sort_model, multi_filter);
        var selection = new Gtk.NoSelection (filter_model);

        update_visibility (model);

        model_items_changed_id = model.items_changed.connect (() => {
            update_visibility (model);
        });

        search_filter.changed.connect (() => {
            update_visibility (model);
        });

        grid_view.model = selection;

        stack.visible_child_name = model.empty ? "empty" : "content";

        model_notify_empty_id = model.notify["empty"].connect (() => {
            stack.visible_child_name = model.empty ? "empty" : "content";
        });
    }

    [GtkCallback]
    private void spinner_map_cb (Gtk.Widget widget) {
        var spinner = widget as Gtk.Spinner;
        spinner.spinning = true;
    }

    [GtkCallback]
    private void spinner_unmap_cb (Gtk.Widget widget) {
        var spinner = widget as Gtk.Spinner;
        spinner.spinning = false;
    }

    [GtkCallback]
    private void sidebar_row_activated_cb (Gtk.ListBoxRow? row) {
        if (row == all_games_row || row == null) {
            current_platform = null;
            content_title.title = _("All Games");
        } else {
            var platform = row.get_data<Platform> ("platform");

            current_platform = platform;
            content_title.title = platform.name;
        }

        platform_filter.changed (DIFFERENT);

        if (split_view.collapsed)
            split_view.show_sidebar = false;
    }

    [GtkCallback]
    private void search_changed_cb () {
        search_filter.search = search_entry.text;
    }

    private void update_visibility (ListModel? model) {
        if (model == null)
            return;

        var visible_platforms = new GenericSet<Platform> (Platform.hash, Platform.equal);
        var enabled_platforms = new GenericSet<Platform> (Platform.hash, Platform.equal);

        var n_games = model.get_n_items ();
        for (uint i = 0; i < n_games; i++) {
            var game = model.get_item (i) as Game;

            if (!(game.platform in visible_platforms))
                visible_platforms.add (game.platform);

            if (!(game.platform in enabled_platforms) &&
                search_filter.match (game)) {
                enabled_platforms.add (game.platform);
            }
        }

        for (int i = 0; true; i++) {
            var row = sidebar.get_row_at_index (i);

            if (row == null)
                break;

            if (row == all_games_row)
                continue;

            var platform = row.get_data<Platform> ("platform");

            row.visible = platform in visible_platforms;
            row.sensitive = platform in enabled_platforms;
        }

        all_games_row.sensitive = enabled_platforms.length > 0;

        var selected = sidebar.get_selected_row ();

        if (selected == null || !selected.sensitive || !selected.visible) {
            if (all_games_row.sensitive) {
                sidebar.select_row (all_games_row);
                sidebar_row_activated_cb (all_games_row);
            } else {
                sidebar.unselect_all ();
                sidebar_row_activated_cb (null);
            }
        }

        search_stack.visible_child_name =
            all_games_row.sensitive ? "content" : "empty";
    }
}
