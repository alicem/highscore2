// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Application : Adw.Application {
    private bool games_loaded;

    public weak MainWindow main_window;

    public Application () {
        Object (
            application_id: Config.APPLICATION_ID,
            flags: ApplicationFlags.HANDLES_OPEN
        );
    }

    construct {
        ActionEntry[] action_entries = {
            { "open", on_open_action },
            { "about", on_about_action },
            { "preferences", on_preferences_action },
            { "quit", quit }
        };
        add_action_entries (action_entries, this);
        set_accels_for_action ("app.open",        {"<primary>o"});
        set_accels_for_action ("app.preferences", {"<primary>comma"});
        set_accels_for_action ("app.quit",        {"<primary>q"});
    }

    protected override void startup () {
        set_resource_base_path ("/org/gnome/World/Highscore2");

        base.startup ();

        style_manager.color_scheme = PREFER_DARK;
    }

    protected override void activate () {
        base.activate ();

        if (main_window == null) {
            var win = new MainWindow (this);
            main_window = win;
        }

        main_window.present ();

        load_games.begin (() => {
            main_window.show_games ();
        });
    }

    public async void load_games () {
        if (games_loaded)
            return;

        if (!LibraryUtils.has_library_dir ()) {
            var dialog = new WelcomeDialog ();

            dialog.closed.connect (() => {
                load_games.callback ();
            });

            dialog.present (main_window);

            yield;
        } else {
            var library = Library.get_instance ();

            yield library.load_games ();
        }

        games_loaded = true;
    }

    protected override void open (File[] files, string hint) {
        foreach (var file in files)
            run_file (file);
    }

    private void on_open_action () {
        open_action_do.begin ();
    }

    private async void open_action_do () {
        var dialog = new Gtk.FileDialog ();
        File? file = null;

        var filters = new ListStore (typeof (Gtk.FileFilter));
        var filter = new Gtk.FileFilter ();

        var mime_types = PlatformRegister.get_register ().list_mime_types ();
        foreach (var mime_type in mime_types)
            filter.add_mime_type (mime_type);

        filter.name = _("Games");
        filters.append (filter);
        dialog.filters = filters;

        dialog.title = _("Open Game");

        try {
            file = yield dialog.open (active_window, null);
        } catch (Gtk.DialogError e) {
            return;
        } catch (Error e) {
            var window = new StandaloneWindow (this);
            window.show_error (_("Failed to open file: %s").printf (e.message));
            window.present ();
            return;
        }

        if (file == null)
            return;

        run_file (file);
    }

    public bool try_focus_existing_window (Game game) {
        foreach (var window in get_windows ()) {
            if (!(window is RunnerWindow))
                continue;

            var runner_window = window as RunnerWindow;
            var running_game = runner_window.running_game;

            if (running_game == null)
                continue;

            if (running_game.uid == game.uid) {
                runner_window.present ();
                return true;
            }
        }

        return false;
    }

    private void run_file (File file) {
        Game game = null;

        try {
            game = create_game_from_file (file);
        } catch (Error e) {
            var window = new StandaloneWindow (this);
            window.show_error (e.message);
            window.present ();
            return;
        }

        if (game == null) {
            var window = new StandaloneWindow (this);
            window.show_error (_("No matching platform found"));
            window.present ();
            return;
        }

        run_game_standalone (game);
    }

    public void run_game_standalone (Game game) {
        if (try_focus_existing_window (game))
            return;

        var window = new StandaloneWindow (this);

        window.run_game.begin (game, window.present);
    }

    private Game create_game_from_file (File file) throws Error {
        string mime_type = null;

        var info = file.query_info (
            FileAttribute.STANDARD_CONTENT_TYPE,
            NONE
        );

        var content_type = info.get_content_type ();
        mime_type = ContentType.get_mime_type (content_type);

        var platforms = PlatformRegister.get_register ().list_platforms_for_mime_type (mime_type);
        Game game = null;

        foreach (var platform in platforms) {
            var parser = GameParser.create (platform, file);

            if (parser.parse ()) {
                game = parser.create_game ();
                break;
            }
        }

        var library = Library.get_instance ();
        game.title = library.find_title (game) ?? game.default_title;

        return game;
    }

    private void on_about_action () {
        string[] developers = { _("Alice Mikhaylenko") };

        var about = new Adw.AboutDialog () {
            application_name = "Highscore",
            application_icon = Config.APPLICATION_ID,
            developer_name = _("Alice Mikhaylenko"),
            version = Config.VERSION,
            website = "https://gitlab.gnome.org/alicem/highscore2",
            issue_url = "https://gitlab.gnome.org/alicem/highscore2/-/issues/new",
            developers = developers,
            copyright = "© 2023 Alice Mikhaylenko",
            translator_credits = _("translator-credits"),
            license_type = GPL_3_0,
        };

        var cores = CoreRegister.get_register ().get_registrars ();

        foreach (var core in cores) {
            about.add_legal_section (
                core.name,
                string.joinv ("\n", core.copyright),
                LicenseUtils.spdx_to_license (core.license),
                null
            );
        }

        about.present (active_window);
    }

    private void on_preferences_action () {
        new PreferencesDialog ().present (active_window);
    }
}
