// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/org/gnome/World/Highscore2/properties-dialog.ui")]
public class Highscore.PropertiesDialog : Adw.Dialog {
    public Game game { get; construct; }

    [GtkChild]
    private unowned Adw.ToastOverlay toast_overlay;
    [GtkChild]
    private unowned Gtk.Box main_box;

    public bool interframe_blending { get; set; }

    private Settings settings;

    public PropertiesDialog (Game game) {
        Object (game: game);
    }

    construct {
        var platform_props = create_platform_properties ();

        if (platform_props != null)
            main_box.append (platform_props);

        settings = new GameSettings (game).get_common ();

        settings.bind ("interframe-blending", this, "interframe-blending", DEFAULT);
    }

    static construct {
        install_action ("game.edit-title", null, widget => {
            var self = widget as PropertiesDialog;
            self.edit_title.begin ();
        });
    }

    private async void edit_title () {
        var dialog = new Adw.AlertDialog (
            _("Edit Title"),
            _("Enter a new title for this game")
        );

        dialog.add_response ("cancel", _("_Cancel"));
        dialog.add_response ("reset", _("Reset"));
        dialog.add_response ("rename", _("_Rename"));

        dialog.set_response_appearance ("rename", SUGGESTED);
        dialog.set_response_appearance ("reset", DESTRUCTIVE);

        dialog.default_response = "rename";

        var initial_title = game.title;
        var entry = new Gtk.Entry () {
            placeholder_text = _("Enter Name"),
            activates_default = true,
            text = initial_title,
        };

        dialog.set_response_enabled ("rename", false);

        if (game.title == game.default_title)
            dialog.remove_response ("reset");

        entry.notify["text"].connect (() => {
            dialog.set_response_enabled (
                "rename", entry.text != "" && entry.text != initial_title
            );
        });

        dialog.extra_child = entry;

        var response = yield dialog.choose (this, null);

        if (response == "rename") {
            var text = entry.text;

            try {
                var library = Library.get_instance ();
                library.rename_game (game, text);
            } catch (Error e) {
                toast_overlay.add_toast (new Adw.Toast.format (
                    _("Failed to rename game: %s"), e.message
                ));
            }
        }

        if (response == "reset") {
            try {
                var library = Library.get_instance ();
                library.rename_game (game, null);
            } catch (Error e) {
                toast_overlay.add_toast (new Adw.Toast.format (
                    _("Failed to rename game: %s"), e.message
                ));
            }
        }
    }

    private PlatformProperties? create_platform_properties () {
        var platform = game.platform;
        var type = platform.properties_type;

        if (type == Type.NONE && platform.parent != null)
            type = platform.parent.properties_type;

        if (type == Type.NONE)
            return null;

        return Object.new (
            type, dialog: this, game: game
        ) as PlatformProperties;
    }

    public void reset_height () {
        content_height = -1;
    }
}
