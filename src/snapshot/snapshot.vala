// This file is part of Highscore. License: GPL-3.0-or-later

public class Highscore.Snapshot : Object {
    public Game game { get; set; }

    private File save_dir;

    public Snapshot (Game game) {
        Object (game: game);

        var data_dir = Environment.get_user_data_dir ();
        var save_dir_path = Path.build_filename (data_dir, "highscore", "saves");

        save_dir = File.new_for_path (save_dir_path);
    }

    private File get_save_location () {
        return save_dir.get_child (@"$(game.uid).save");
    }

    public File get_savestate_file () {
        return save_dir.get_child (@"$(game.uid).savestate");
    }

    private File get_screenshot_file () {
        return save_dir.get_child (@"$(game.uid).png");
    }

    private File get_metadata_file () {
        return save_dir.get_child (@"$(game.uid).metadata");
    }

    public bool is_valid () {
        if (!save_dir.query_exists ())
            return false;

        if (!get_savestate_file ().query_exists ())
            return false;

        if (!get_screenshot_file ().query_exists ())
            return false;

        if (!get_metadata_file ().query_exists ())
            return false;

        return true;
    }

    public void ensure_dir () throws Error {
        if (!save_dir.query_exists ())
            save_dir.make_directory_with_parents ();
    }

    public string get_savestate_path () {
        return get_savestate_file ().get_path ();
    }

    public async void save_data (File location) throws Error {
        if (!location.query_exists ())
            return;

        var file = get_save_location ();

        if (file.query_exists ())
            yield FileUtils.delete_recursively (file);

        yield FileUtils.copy_recursively (location, file);
    }

    public async void load_data (File location) throws Error {
        var file = get_save_location ();

        if (location.query_exists ())
            yield FileUtils.delete_recursively (location);

        if (file.query_exists ())
            yield FileUtils.copy_recursively (file, location);
    }

    public Gdk.Texture get_screenshot () throws Error {
        var file = get_screenshot_file ();

        return Gdk.Texture.from_file (file);
    }

    public KeyFile get_metadata () throws Error {
        var metadata_file = get_metadata_file ();

        var keyfile = new KeyFile ();
        keyfile.load_from_file (metadata_file.get_path (), KeyFileFlags.NONE);

        return keyfile;
    }

    public void save_screenshot (Gdk.Texture texture) throws Error {
        var downloader = new Gdk.TextureDownloader (texture);
        downloader.set_format (R8G8B8);

        size_t rowstride;
        var bytes = downloader.download_bytes (out rowstride);

        var pixbuf = new Gdk.Pixbuf.from_data (
            bytes.get_data (),
            RGB,
            false,
            8,
            texture.get_width (),
            texture.get_height (),
            (int) rowstride
        );

        var file = get_screenshot_file ();
        pixbuf.save (file.get_path (), "png", null);
    }

    public void save_metadata (KeyFile keyfile) throws Error {
        var metadata_file = get_metadata_file ();

        if (metadata_file.query_exists ())
            metadata_file.@delete ();

        keyfile.save_to_file (metadata_file.get_path ());
    }
}
