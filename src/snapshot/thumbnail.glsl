#ifdef CORE
#  define _OUT_ out
#  define _IN_ in
#else
#  define _OUT_ varying

#  ifdef VERTEX
#    define _IN_ attribute
#  else
#    define _IN_ varying
#  endif

#  define texture texture2D
#endif

#if !defined(LEGACY)
precision mediump float;
#endif

#ifdef VERTEX

_IN_ vec2 position;
_IN_ vec2 texCoord;

_OUT_ vec2 uv;

uniform mat4 u_mvp;

void main() {
  uv = texCoord;

  gl_Position = u_mvp * vec4(position, 0, 1);
}

#else // FRAGMENT

_IN_ vec2 uv;

#ifdef CORE
_OUT_ vec4 outputColor;
#endif

uniform sampler2D u_texture;

void main() {
#ifdef CORE
  outputColor = texture(u_texture, uv);
#else
  gl_FragColor = texture(u_texture, uv);
#endif
}

#endif