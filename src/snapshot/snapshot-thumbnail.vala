// This file is part of Highscore. License: GPL-3.0+.

using GL;

public class Highscore.SnapshotThumbnail : Gtk.GLArea {
    public bool secondary { get; set; }

    private Snapshot _snapshot;
    public new Snapshot snapshot {
        get { return _snapshot; }
        set {
            _snapshot = value;

            if (snapshot != null) {
                screen_set = ScreenSet.create (null, snapshot);

                if (secondary) {
                    set_screen_layout (screen_set.secondary_layout ?? new SingleLayout (0));

                    screen_set.secondary_layout_changed.connect (animate => {
                        set_screen_layout (screen_set.secondary_layout ?? new SingleLayout (0));
                    });
                } else {
                    set_screen_layout (screen_set.layout);

                    screen_set.layout_changed.connect (animate => {
                        set_screen_layout (screen_set.layout);
                    });
                }

                screens = screen_set.get_screens ();

                try {
                    var metadata = snapshot.get_metadata ();

                    aspect_ratio = metadata.get_double (
                        "Screenshot", "Aspect Ratio"
                    );
                    flipped = metadata.get_boolean (
                        "Screenshot", "Flipped"
                    );

                    current_frame = snapshot.get_screenshot ();
                    frame_changed = true;
                } catch (Error e) {
                    critical ("Failed to show snapshot: %s", e.message);
                }

                screen_set.filter_changed.connect (() => {
                    platform_filter_changed = true;
                    queue_draw ();
                });

                platform_filter_changed = true;
            }

            queue_draw ();
        }
    }

    private bool platform_filter_changed;

    private Gdk.Texture? current_frame;
    private bool frame_changed;

    private ScreenSet screen_set;
    private Screen[]? screens;
    private ScreenLayout screen_layout;
    private bool screen_layout_changed;

    private int layout_width;
    private int layout_height;

    private int last_layout_width;
    private int last_layout_height;

    private double aspect_ratio;
    private bool flipped;

    private bool context_recreated;

    private GLVertexArray vertex_array;
    private GLVertexBuffer vertex_buffer;
    private GLElementBuffer element_buffer;
    private GLShader platform_shader;
    private GLShader shader;
    private GLTexture texture;
    private GLFramebuffer framebuffer;

    static construct {
        set_css_name ("snapshot-thumbnail");
    }

    private void set_screen_layout (ScreenLayout layout) {
        screen_layout = layout;
        screen_layout_changed = true;
        queue_draw ();
    }

    public override void realize () {
        base.realize ();

        make_current ();

        if (get_error () != null)
            return;

        vertex_array = new GLVertexArray ();
        vertex_array.bind ();

        vertex_buffer = new GLVertexBuffer ();
        vertex_buffer.bind ();
        vertex_buffer.set_data ({
             0,  1,
             1,  1,
             1,  0,
             0,  0,
        });

        element_buffer = new GLElementBuffer ();
        element_buffer.bind ();
        element_buffer.set_data ({
            0, 1, 2,
            2, 3, 0
        });

        texture = new GLTexture ();

        create_platform_filter ();

        var shader_path = "/org/gnome/World/Highscore2/snapshot/thumbnail.glsl";
        var shader_src = ResourceUtils.load_to_string (shader_path);
        shader = new GLShader (get_context (), shader_src);

        framebuffer = new GLFramebuffer ();

        context_recreated = true;
    }

    public override void unrealize () {
        make_current ();

        if (get_error () != null) {
            base.unrealize ();
            return;
        }

        vertex_array = null;
        vertex_buffer = null;
        element_buffer = null;
        shader = null;
        platform_shader = null;
        texture = null;
        framebuffer = null;

        base.unrealize ();
    }

    public override bool render (Gdk.GLContext context) {
        var error = get_error ();
        if (error != null)
            critical (error.message);

        glClearColor (0.0f, 0.0f, 0.0f, 0.0f);
        glClear (GL_COLOR_BUFFER_BIT);

        draw_frame ();

        glFlush ();

        return Gdk.EVENT_STOP;
    }

    private void draw_frame () {
        if (current_frame == null)
            return;

        int widget_width = get_width () * scale_factor;
        int widget_height = get_height () * scale_factor;

        if (screen_layout_changed || context_recreated) {
            float w, h;
            screen_layout.measure (
                current_frame.width, current_frame.height, screens, out w, out h
            );

            layout_width = (int) Math.ceil (w);
            layout_height = (int) Math.ceil (h);

            if (last_layout_width != layout_width ||
                last_layout_height != layout_height ||
                context_recreated) {
                framebuffer.bind ();
                framebuffer.create_texture (
                    layout_width, layout_height,
                    GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_BORDER
                );
                framebuffer.unbind ();

                attach_buffers ();

                last_layout_width = layout_width;
                last_layout_height = layout_height;
            }
        }

        var layout_result = screen_layout.layout (
            {
                layout_width,
                layout_height,
                0, 0, 0, 0,
            },
            (float) current_frame.width / current_frame.height,
            screens
        );

        assert (screens.length == layout_result.length);

        if (frame_changed || context_recreated) {
            texture.bind ();
            texture.upload (current_frame);
            texture.unbind ();
        }

        if (frame_changed ||
            screen_layout_changed ||
            platform_filter_changed ||
            context_recreated) {
            framebuffer.bind (GL_DRAW_FRAMEBUFFER);

            glViewport (0, 0, layout_width, layout_height);

            glClearColor (0.0f, 0.0f, 0.0f, 0.0f);
            glClear (GL_COLOR_BUFFER_BIT);

            platform_shader.bind ();
            platform_shader.set_attribute_pointer ("position", 2, GL_FLOAT);
            platform_shader.set_attribute_pointer ("texCoord", 2, GL_FLOAT);

            texture.bind ();

            GLUtils.set_active_texture (0);
            texture.set_params (GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE);
            platform_shader.set_uniform_int ("u_texture", 0);

            for (int i = 0; i < screens.length; i++) {
                var screen = screens[i];
                var result = layout_result[i];

                if (!result.visible)
                    continue;

                var transform = new Gsk.Transform ();
                transform = transform.translate ({ -1, 1 });
                transform = transform.scale (2, -2);
                transform = transform.scale (1.0f / layout_width, 1.0f / layout_height);

                transform = transform.translate ({ layout_width / 2.0f, layout_height / 2.0f });
                transform = transform.rotate (result.angle);
                transform = transform.translate ({ -layout_width / 2.0f, -layout_height / 2.0f });

                transform = transform.transform (result.transform);
                transform = transform.scale (result.width, result.height);

                Graphene.Rect rect = {{ 0, 0 }, { 1, 1 }};
                var transformed_rect = transform.transform_bounds (rect);

                if (!transformed_rect.intersection ({{ -1, -1 }, { 2, 2 }}, null))
                    continue;

                var mvp = transform.to_matrix ();

                Graphene.Vec2 position = {}, size = {};
                position.init (screen.area.origin.x, screen.area.origin.y);
                size.init (screen.area.size.width, screen.area.size.height);

                platform_shader.set_uniform_vec2 ("u_screenPosition", position);
                platform_shader.set_uniform_vec2 ("u_screenSize", size);
                platform_shader.set_uniform_bool ("u_flipped", !flipped);
                platform_shader.set_uniform_matrix ("u_mvp", mvp);

                screen_set.setup_filter (platform_shader, screen.id);

                element_buffer.draw ();
            }

            texture.unbind ();
            platform_shader.unbind ();
            framebuffer.unbind (GL_DRAW_FRAMEBUFFER);

            attach_buffers ();
        }

        glViewport (0, 0, widget_width, widget_height);

        shader.bind ();
        shader.set_attribute_pointer ("position", 2, GL_FLOAT);
        shader.set_attribute_pointer ("texCoord", 2, GL_FLOAT);

        framebuffer.bind_texture ();
        shader.set_uniform_int ("u_texture", 0);

        var transform = new Gsk.Transform ();
        transform = transform.translate ({ -1, -1 });
        transform = transform.scale (2, 2);

        float widget_aspect_ratio = (float) widget_width / widget_height;
        float layout_aspect_ratio = (float) layout_width / layout_height;

        layout_aspect_ratio *= (float) aspect_ratio;
        layout_aspect_ratio /= (float) current_frame.width / current_frame.height;

        float w = 1, h = 1;

        if (widget_aspect_ratio < layout_aspect_ratio)
            h = (float) widget_aspect_ratio / layout_aspect_ratio;
        else
            w = (float) layout_aspect_ratio / widget_aspect_ratio;

        transform = transform.translate ({ (1 - w) / 2f, (1 - h) / 2f });
        transform = transform.scale (w, h);

        var mvp = transform.to_matrix ();

        Graphene.Vec2 texture_size = {};
        texture_size.init (layout_width, layout_height);
        shader.set_uniform_vec2 ("u_textureSize", texture_size);
        shader.set_uniform_matrix ("u_mvp", mvp);

        element_buffer.draw ();

        framebuffer.unbind_texture ();
        shader.unbind ();

        frame_changed = false;
        screen_layout_changed = false;
        platform_filter_changed = false;
        context_recreated = false;
    }

    private void create_platform_filter () {
        var shared_path = "/org/gnome/World/Highscore2/display/glsl/platform/shared.glsl";
        string main_path;

        main_path = screen_set.get_filter_path ();

        if (main_path == null)
            main_path = "/org/gnome/World/Highscore2/display/glsl/platform/default.glsl";

        var shared = ResourceUtils.load_to_string (shared_path);
        var main = ResourceUtils.load_to_string (main_path);

        platform_shader = new GLShader (context, @"$shared\n\n$main");
    }
}
