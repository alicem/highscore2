// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/org/gnome/World/Highscore2/preferences-dialog.ui")]
public class Highscore.PreferencesDialog: Adw.PreferencesDialog {
    public string library_dir { get; set; }
    public bool pause_when_inactive { get; set; }
    public bool load_state_on_startup { get; set; }

    public bool bilinear_filtering { get; set; }

    private Settings settings;

    construct {
        settings = new Settings ("org.gnome.World.Highscore2");

        settings.bind ("library-dir", this, "library-dir", DEFAULT);
        settings.bind ("pause-when-inactive", this, "pause-when-inactive", DEFAULT);
        settings.bind ("load-state-on-startup", this, "load-state-on-startup", DEFAULT);

        settings.bind ("bilinear-filtering", this, "bilinear-filtering", DEFAULT);
    }

    static construct {
        install_action ("preferences.select-library-folder", null, widget => {
            var self = widget as PreferencesDialog;

            self.select_library_folder.begin ();
        });
    }

    private async void select_library_folder () {
        var dialog = new Gtk.FileDialog () {
            title = _("Select Library Folder"),
            modal = true,
        };

        File folder = null;

        try {
            folder = yield dialog.select_folder (get_root () as Gtk.Window, null);
        } catch (Gtk.DialogError e) {
            return;
        } catch (Error e) {
            add_toast (new Adw.Toast.format (
                _("Failed to select library folder: %s"), e.message
            ));
            return;
        }

        settings.set_string ("library-dir", folder.get_path ());
    }
}
