// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Game : Object {
    public string title { get; construct set; }
    public string default_title { get; construct; }
    public string uid { get; construct; }
    public File file { get; construct; }
    public Platform platform { get; construct; }
    public GameMetadata? metadata { get; construct; }
    public string[] extra_paths { get; construct; }

    public Game (
        File file,
        Platform platform,
        string uid,
        string title,
        string default_title,
        GameMetadata? metadata,
        string[]? extra_paths
    ) {
        Object (
            file: file,
            platform: platform,
            uid: uid,
            title: title,
            default_title: default_title,
            metadata: metadata,
            extra_paths: extra_paths
        );
    }

    public bool matches_search_terms (string[] search_terms) {
        if (search_terms.length != 0)
            foreach (var term in search_terms)
                if (!(term.casefold () in title.casefold ()))
                    return false;

        return true;
    }

    public static uint hash (Game key) {
        return str_hash (key.uid);
    }

    public static bool equal (Game a, Game b) {
        if (a == b)
            return true;

        return str_equal (a.uid, b.uid);
    }

    public static int compare_id (Game a, Game b) {
        if (a == b)
            return 0;

        return strcmp (a.uid, b.uid);
    }

    public static int compare (Game a, Game b) {
        var ret = a.title.collate (b.title);
        if (ret != 0)
            return ret;

        ret = Platform.compare (a.platform, b.platform);
        if (ret != 0)
            return ret;

        return compare_id (a, b);
    }
}
