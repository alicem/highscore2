// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Gamepad : Object {
    public signal void button_pressed (Button button);
    public signal void button_released (Button button);
    public signal void analog_moved (Analog analog, double x, double y);

    private struct ButtonData {
        string id;
        int code;
    }

    public enum Button {
        UP, DOWN, LEFT, RIGHT, NORTH, SOUTH, WEST, EAST,
        SELECT, START, L, R, L2, R2, L3, R3, HOME;

        public string get_id () {
            return BUTTON_DATA[this].id;
        }

        public int get_code () {
            return BUTTON_DATA[this].code;
        }

        public Button[] all () {
            return {
                UP, DOWN, LEFT, RIGHT, NORTH, SOUTH, WEST, EAST,
                SELECT, START, L, R, L2, R2, L3, R3, HOME,
            };
        }
    }

    private const ButtonData[] BUTTON_DATA = {
        { "up",     Linux.Input.BTN_DPAD_UP },
        { "down",   Linux.Input.BTN_DPAD_DOWN },
        { "left",   Linux.Input.BTN_DPAD_LEFT },
        { "right",  Linux.Input.BTN_DPAD_RIGHT },
        { "north",  Linux.Input.BTN_X },
        { "south",  Linux.Input.BTN_A },
        { "west",   Linux.Input.BTN_Y },
        { "east",   Linux.Input.BTN_B },
        { "select", Linux.Input.BTN_SELECT },
        { "start",  Linux.Input.BTN_START },
        { "l",      Linux.Input.BTN_TL },
        { "r",      Linux.Input.BTN_TR },
        { "l2",     Linux.Input.BTN_TL2 },
        { "r2",     Linux.Input.BTN_TR2 },
        { "l3",     Linux.Input.BTN_THUMBL },
        { "r3",     Linux.Input.BTN_THUMBR },
        { "home",   Linux.Input.BTN_MODE },
    };

    public const int N_BUTTONS = 17;

    private struct AnalogData {
        string id;
        int x_code;
        int y_code;
    }

    public enum Analog {
        LEFT, RIGHT;

        public string get_id () {
            return ANALOG_DATA[this].id;
        }

        public int get_x_code () {
            return ANALOG_DATA[this].x_code;
        }

        public int get_y_code () {
            return ANALOG_DATA[this].y_code;
        }

        public Analog[] all () {
            return { LEFT, RIGHT };
        }
    }

    private const AnalogData[] ANALOG_DATA = {
        { "left",  Linux.Input.ABS_X,  Linux.Input.ABS_Y },
        { "right", Linux.Input.ABS_RX, Linux.Input.ABS_RY },
    };

    public const int N_ANALOGS = 2;

    public Manette.Device device { get; construct; }

    private bool[] buttons;
    private double[] axes;

    private HashTable<int, Button> button_mapping;

    public Gamepad (Manette.Device device) {
        Object (device: device);
    }

    construct {
        button_mapping = new HashTable<int, Button> (direct_hash, direct_equal);
        button_mapping[Linux.Input.BTN_DPAD_UP]    = UP;
        button_mapping[Linux.Input.BTN_DPAD_DOWN]  = DOWN;
        button_mapping[Linux.Input.BTN_DPAD_LEFT]  = LEFT;
        button_mapping[Linux.Input.BTN_DPAD_RIGHT] = RIGHT;
        button_mapping[Linux.Input.BTN_X]          = NORTH;
        button_mapping[Linux.Input.BTN_A]          = SOUTH;
        button_mapping[Linux.Input.BTN_Y]          = WEST;
        button_mapping[Linux.Input.BTN_B]          = EAST;
        button_mapping[Linux.Input.BTN_SELECT]     = SELECT;
        button_mapping[Linux.Input.BTN_START]      = START;
        button_mapping[Linux.Input.BTN_TL]         = L;
        button_mapping[Linux.Input.BTN_TR]         = R;
        button_mapping[Linux.Input.BTN_TL2]        = L2;
        button_mapping[Linux.Input.BTN_TR2]        = R2;
        button_mapping[Linux.Input.BTN_THUMBL]     = L3;
        button_mapping[Linux.Input.BTN_THUMBR]     = R3;
        button_mapping[Linux.Input.BTN_MODE]       = HOME;

        buttons = new bool[N_BUTTONS];
        axes = new double[N_ANALOGS * 2];

        device.button_press_event.connect (button_pressed_cb);
        device.button_release_event.connect (button_released_cb);
        device.absolute_axis_event.connect (absolute_axis_cb);
    }

    public bool get_button_pressed (Button button) {
        return buttons[button];
    }

    public double get_analog_x (Analog analog) {
        return axes[analog * 2];
    }

    public double get_analog_y (Analog analog) {
        return axes[analog * 2 + 1];
    }

    public bool get_supports_rumble () {
        return device.has_rumble ();
    }

    private void button_pressed_cb (Manette.Event event) {
        uint16 button_code;

        if (!event.get_button (out button_code))
            return;

        if (!(button_code in button_mapping))
            return;

        var button = button_mapping[button_code];

        buttons[button] = true;

        button_pressed (button);
    }

    private void button_released_cb (Manette.Event event) {
        uint16 button_code;

        if (!event.get_button (out button_code))
            return;

        if (!(button_code in button_mapping))
            return;

        var button = button_mapping[button_code];

        buttons[button] = false;

        button_released (button);
    }

    private void absolute_axis_cb (Manette.Event event) {
        int axis;
        double value;

        if (!event.get_absolute (out axis, out value))
            return;

        Analog analog;
        bool y;

        switch (axis) {
            case Linux.Input.ABS_X:
                analog = LEFT;
                y = false;
                break;
            case Linux.Input.ABS_Y:
                analog = LEFT;
                y = true;
                break;
            case Linux.Input.ABS_RX:
                analog = RIGHT;
                y = false;
                break;
            case Linux.Input.ABS_RY:
                analog = RIGHT;
                y = true;
                break;
            default:
                return;
        }

        axes[analog * 2 + (y ? 1 : 0)] = value;

        analog_moved (analog, axes[analog * 2], axes[analog * 2 + 1]);
    }
}
