// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.GameParser : Object {
    public Platform platform { get; construct; }
    public File file { get; construct; }

    public GameParser (Platform platform, File file) {
        Object (platform: platform, file: file);
    }

    public virtual bool parse () throws Error {
        return true;
    }

    public virtual string get_uid () throws Error {
        return Fingerprint.get_uid (file, platform.id);
    }

    public virtual string get_title () throws Error {
        return Filename.get_title (file);
    }

    public virtual GameMetadata? get_metadata () throws Error {
        return null;
    }

    public virtual string[]? get_extra_paths () throws Error {
        return null;
    }

    public Game create_game () throws Error {
        var uid = get_uid ();
        var title = get_title ();
        var metadata = get_metadata ();
        var extra_paths = get_extra_paths ();

        return new Game (file, platform, uid, title, title, metadata, extra_paths);
    }

    public static GameParser create (Platform platform, File file) {
        var type = platform.parser_type;

        if (type == Type.NONE && platform.parent != null)
            type = platform.parent.parser_type;

        if (type == Type.NONE)
            type = typeof (GameParser);

        return Object.new (type, platform: platform, file: file) as GameParser;
    }
}
