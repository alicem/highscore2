// This file is part of Highscore. License: GPL-3.0+.

public interface Highscore.RunnerWindow : Gtk.Window {
    public abstract Game? running_game { get; }
}
