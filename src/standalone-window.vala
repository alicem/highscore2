// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/org/gnome/World/Highscore2/standalone-window.ui")]
public class Highscore.StandaloneWindow : Adw.ApplicationWindow, RunnerWindow {
    [GtkChild]
    private unowned Adw.ToastOverlay toast_overlay;
    [GtkChild]
    private unowned Gtk.Stack stack;
    [GtkChild]
    private unowned Adw.StatusPage error_page;

    private DisplayPage? display_page;

    public Game? running_game {
        get {
            if (display_page != null)
                return display_page.game;

            return null;
        }
    }

    public StandaloneWindow (Gtk.Application app) {
        Object (application: app);
    }

    construct {
        var settings = new Settings ("org.gnome.World.Highscore2.state");

        settings.bind ("standalone-window-maximized", this, "maximized",      GET | SET | GET_NO_CHANGES);
        settings.bind ("standalone-window-width",     this, "default-width",  GET | SET | GET_NO_CHANGES);
        settings.bind ("standalone-window-height",    this, "default-height", GET | SET | GET_NO_CHANGES);

        action_set_enabled ("game.add-to-library", false);
    }

    static construct {
        install_action ("game.add-to-library", null, widget => {
            var self = widget as StandaloneWindow;

            self.add_to_library.begin ();
        });
    }

    public void show_error (string message) {
        error_page.title = _("Unable to run the game");
        error_page.description = message;
        stack.visible_child_name = "error";
    }

    public async void run_game (Game game) requires (display_page == null) {
        display_page = new DisplayPage (game) {
            primary = true
        };

        display_page.bind_property ("window-title", this, "title", SYNC_CREATE);

        stack.add_child (display_page);
        stack.visible_child = display_page;

        if (LibraryUtils.has_library_dir ()) {
            var app = application as Application;
            var library = Library.get_instance ();

            yield app.load_games ();

            action_set_enabled ("game.add-to-library", !library.has_game (game));
        }

        display_page.start_game.begin ();
    }

    protected override bool close_request () {
        if (display_page != null) {
            display_page.save_and_close.begin (() => {
                display_page = null;
                close ();
            });
            return Gdk.EVENT_STOP;
        }

        return Gdk.EVENT_PROPAGATE;
    }

    private async void add_to_library () {
        var library = Library.get_instance ();

        try {
            yield library.import_game (running_game);
        } catch (Error e) {
            toast_overlay.add_toast (
                new Adw.Toast.format (_("Failed to add game: %s"), e.message)
            );
            return;
        }

        action_set_enabled ("game.add-to-library", !library.has_game (running_game));

        toast_overlay.add_toast (new Adw.Toast (_("Game Added")));
    }
}
