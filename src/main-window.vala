// This file is part of Highscore. License: GPL-3.0-or-later

[GtkTemplate (ui = "/org/gnome/World/Highscore2/main-window.ui")]
public class Highscore.MainWindow : Adw.ApplicationWindow, RunnerWindow {
    [GtkChild]
    private unowned Adw.NavigationView nav_view;
    [GtkChild]
    private unowned LibraryPage library_page;

    private DisplayPage? display_page;

    public Game? running_game {
        get {
            if (display_page != null)
                return display_page.game;

            return null;
        }
    }

    public MainWindow (Gtk.Application app) {
        Object (application: app);
    }

    construct {
        var settings = new Settings ("org.gnome.World.Highscore2.state");

        settings.bind ("main-window-maximized", this, "maximized",      GET | SET | GET_NO_CHANGES);
        settings.bind ("main-window-width",     this, "default-width",  GET | SET | GET_NO_CHANGES);
        settings.bind ("main-window-height",    this, "default-height", GET | SET | GET_NO_CHANGES);
    }

    protected override bool close_request () {
        if (display_page != null) {
            display_page.save_and_close.begin (() => {
                display_page = null;
                close ();
            });
            return Gdk.EVENT_STOP;
        }

        return Gdk.EVENT_PROPAGATE;
    }

    [GtkCallback]
    private async void game_activated_cb (Game game) {
        var app = application as Application;
        if (app.try_focus_existing_window (game))
            return;

        display_page = new DisplayPage (game);
        display_page.hidden.connect (() => {
            display_page = null;
            title = _("Highscore");
        });

        display_page.bind_property ("window-title", this, "title", SYNC_CREATE);

        nav_view.push (display_page);

        display_page.start_game.begin ();
    }

    public void show_games () {
        library_page.show_games ();
    }
}
