// This file is part of Highscore. License: GPL-3.0+.

private class Highscore.CoreRegister : Object {
    private static CoreRegister instance;
    private HashTable<string, CoreRegistrar> core_registrars;

    private CoreRegister () {
        core_registrars = new HashTable<string, CoreRegistrar> (str_hash, str_equal);

        try {
            var directory = File.new_for_path (Config.CORES_DIR);
            var enumerator = directory.enumerate_children (FileAttribute.STANDARD_NAME, 0);

            FileInfo info;
            while ((info = enumerator.next_file ()) != null) {
                var name = info.get_name ();
                if (!name.has_suffix (".highscore"))
                    continue;

                // Remove the ".highscore" suffix
                name = name.substring (0, name.length - 10);

                core_registrars[name] = new CoreRegistrar (name);
            }
        }
        catch (Error e) {
            debug (e.message);
        }
    }

    public static CoreRegister get_register () {
        if (instance == null)
            instance = new CoreRegister ();

        return instance;
    }

    public CoreRegistrar[] get_registrars () {
        var values = core_registrars.get_values ();
        var result = new Gee.ArrayList<CoreRegistrar> ();

        foreach (var registrar in values)
            result.add (registrar);

        result.sort (CoreRegistrar.compare);

        return result.to_array ();
    }

    public CoreRegistrar[] find_registrars_for_platform (Hs.Platform platform) {
        var values = core_registrars.get_values ();
        var result = new Gee.ArrayList<CoreRegistrar> ();

        foreach (var registrar in values) {
            var platforms = registrar.get_platforms ();

            if (platform in platforms)
                result.add (registrar);
        }

        result.sort (CoreRegistrar.compare);

        return result.to_array ();
    }
}
