// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.SwitchIcon : Gtk.Widget {
    private const int SPACING = 4;
    private const int SWITCH_WIDTH = 8;
    private const int SWITCH_HEIGHT = 16;
    private const int SLIDER_SIZE = 4;

    public uint n_switches { get; construct; default = 2; }

    private bool[] states;
    private Adw.SpringAnimation[] animations;

    construct {
        states = {};

        for (int i = 0; i < n_switches; i++) {
            states += false;
            animations += new Adw.SpringAnimation (
                this, 0, 1,
                new Adw.SpringParams (1, 1, 800),
                new Adw.CallbackAnimationTarget (value => {
                    queue_draw ();
                })
            );
        }
    }

    public override void measure (
        Gtk.Orientation orientation,
        int for_size,
        out int minimum,
        out int natural,
        out int minimum_baseline,
        out int natural_baseline
    ) {
        if (orientation == HORIZONTAL)
            minimum = natural = ((SWITCH_WIDTH + SPACING) * (int) n_switches) - SPACING;
        else
            minimum = natural = SWITCH_HEIGHT;

        minimum_baseline = natural_baseline = -1;
    }

    public override void snapshot (Gtk.Snapshot snapshot) {
        var color = get_color ();

        for (int i = 0; i < n_switches; i++) {
            Graphene.Rect rect = {{ 0, 0 }, { SWITCH_WIDTH, SWITCH_HEIGHT }};
            Gsk.RoundedRect rounded_rect = {};
            rounded_rect.init_from_rect (rect, SWITCH_WIDTH / 2);

            Graphene.Rect slider_rect = {{ 0, 0 }, { SLIDER_SIZE, SLIDER_SIZE }};
            Gsk.RoundedRect slider_rounded_rect = {};
            slider_rounded_rect.init_from_rect (slider_rect, SLIDER_SIZE / 2);

            snapshot.save ();
            {
                snapshot.translate ({ i * (SWITCH_WIDTH + SPACING), 0 });
                snapshot.push_mask (INVERTED_ALPHA);
                {
                    int slider_padding = (SWITCH_WIDTH - SLIDER_SIZE) / 2;

                    snapshot.translate ({
                        slider_padding,
                        (float) Adw.lerp (
                            slider_padding,
                            SWITCH_HEIGHT - SLIDER_SIZE - slider_padding,
                            animations[i].value
                        )
                    });
                    snapshot.push_rounded_clip (slider_rounded_rect);
                    snapshot.append_color ({ 0, 0, 0, 1 }, slider_rect);
                    snapshot.pop ();
                }
                snapshot.pop ();
                {
                    snapshot.push_rounded_clip (rounded_rect);
                    snapshot.append_color (color, rect);
                    snapshot.pop ();
                }
                snapshot.pop ();
            }
            snapshot.restore ();
        }
    }

    public bool get_switch_state (uint index) requires (index < n_switches) {
        return states[index];
    }

    public void set_switch_state (uint index, bool state) requires (index < n_switches) {
        states[index] = state;

        animations[index].value_from = animations[index].value;
        animations[index].value_to = state ? 1 : 0;
        animations[index].play ();
    }
}
