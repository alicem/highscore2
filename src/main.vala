// This file is part of Highscore. License: GPL-3.0-or-later

int main (string[] args) {
    Intl.bindtextdomain (Config.GETTEXT_PACKAGE, Config.GNOMELOCALEDIR);
    Intl.bind_textdomain_codeset (Config.GETTEXT_PACKAGE, "UTF-8");
    Intl.textdomain (Config.GETTEXT_PACKAGE);

    Highscore.Debug.parse (false);

    var app = new Highscore.Application ();

    return app.run (args);
}
