// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.GamepadManager : Object {
    public signal void gamepad_added (Gamepad gamepad);
    public signal void gamepad_removed (Gamepad gamepad);

    private static GamepadManager instance;

    private Manette.Monitor monitor;

    private Gee.ArrayList<Gamepad> gamepads;
    private HashTable<Manette.Device, Gamepad> gamepad_mapping;

    construct {
        monitor = new Manette.Monitor ();

        gamepads = new Gee.ArrayList<Gamepad> ();
        gamepad_mapping = new HashTable<Manette.Device, Gamepad> (direct_hash, direct_equal);

        var iterator = monitor.iterate ();
        Manette.Device device = null;

        while (iterator.next (out device))
            add_gamepad (device);

        monitor.device_connected.connect (add_gamepad);
        monitor.device_disconnected.connect (remove_gamepad);
    }

    public static GamepadManager get_instance () {
        if (instance == null)
            instance = new GamepadManager ();

        return instance;
    }

    private void add_gamepad (Manette.Device device) {
        var gamepad = new Gamepad (device);

        gamepads.add (gamepad);
        gamepad_mapping[device] = gamepad;

        gamepad_added (gamepad);
    }

    private void remove_gamepad (Manette.Device device) {
        var gamepad = gamepad_mapping[device];

        gamepads.remove (gamepad);
        gamepad_mapping.remove (device);

        gamepad_removed (gamepad);
    }

    public Gamepad[] get_gamepads () {
        return gamepads.to_array ();
    }
}
