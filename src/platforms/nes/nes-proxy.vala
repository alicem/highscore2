// This file is part of Highscore. License: GPL-3.0+.

[DBus (name = "org.gnome.World.Highscore2.Runner")]
public interface Highscore.Nes.Proxy : Object {
    public abstract async uint get_players () throws Error;

    public abstract async bool get_has_mic () throws Error;

    public abstract async Hs.NesAccessory get_accessory () throws Error;
}
