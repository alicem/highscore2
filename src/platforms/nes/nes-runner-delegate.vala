// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Nes.RunnerDelegate : Highscore.RunnerDelegate {
    private Hs.NesAccessory accessory;

    public bool has_mic { get; private set; }
    public Hs.NesMicLevel mic_level { get; set; }

    construct {
        notify["mic-level"].connect (() => {
            runner.input_buffer.modify_input_state (state => {
                state->nes.mic_level = mic_level;
            });
        });
    }

    public override async void after_load () throws Error {
        var proxy = runner.platform_proxy as Proxy;

        accessory = yield proxy.get_accessory ();

        if (accessory == ZAPPER) {
            runner.uses_pointer = true;
            runner.set_cursor (ScreenSet.DEFAULT_SCREEN, CROSSHAIR);
        }

        has_mic = yield proxy.get_has_mic ();
    }

    public override async void after_reset () throws Error {
        mic_level = QUIET;
    }

    public override async void load_state (KeyFile metadata) throws Error {
        mic_level = QUIET;
    }

    private void set_zapper (bool fire, double x, double y) {
        runner.input_buffer.modify_input_state (state => {
            state->nes.accessory_zapper_fire = fire;

            if (fire) {
                state->nes.accessory_zapper_x = x;
                state->nes.accessory_zapper_y = y;
            }
        });
    }

    public override bool drag_begin (int screen_id, double x, double y) {
        switch (accessory) {
        case ZAPPER:
            set_zapper (true, x, y);
            break;
        default:
            assert_not_reached ();
        }

        return true;
    }

    public override void drag_end (double x, double y) {
        switch (accessory) {
        case ZAPPER:
            set_zapper (false, 0, 0);
            break;
        default:
            assert_not_reached ();
        }
    }

    public override Gtk.Widget? create_header_widget () {
        var mic_button = new Gtk.ToggleButton () {
            icon_name = "audio-input-microphone-symbolic",
        };

        bind_property ("has-mic", mic_button, "visible", SYNC_CREATE);
        bind_property (
            "mic-level", mic_button, "active",
            SYNC_CREATE | BIDIRECTIONAL,
            (binding, from_value, ref to_value) => {
                to_value = from_value == Hs.NesMicLevel.LOUD;
                return true;
            },
            (binding, from_value, ref to_value) => {
                if (from_value == true)
                    to_value = Hs.NesMicLevel.LOUD;
                else
                    to_value = Hs.NesMicLevel.QUIET;
                return true;
            }
        );

        return mic_button;
    }
}
