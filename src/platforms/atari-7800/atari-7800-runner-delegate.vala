// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Atari7800.RunnerDelegate : Highscore.RunnerDelegate {
    private Hs.Atari7800Controller controllers[2];
    private Hs.Atari7800Difficulty difficulty[2];

    public Hs.Atari7800Difficulty left_difficulty {
        get { return difficulty[0]; }
        set {
            if (left_difficulty == value)
                return;

            difficulty[0] = value;

            runner.input_buffer.modify_input_state (state => {
                state->atari_7800.difficulty[0] = left_difficulty;
            });
        }
    }

    public Hs.Atari7800Difficulty right_difficulty {
        get { return difficulty[1]; }
        set {
            if (right_difficulty == value)
                return;

            difficulty[1] = value;

            runner.input_buffer.modify_input_state (state => {
                state->atari_7800.difficulty[1] = right_difficulty;
            });
        }
    }

    public override async void after_load () throws Error {
        var proxy = runner.platform_proxy as Proxy;

        controllers[0] = yield proxy.get_controller (0);
        controllers[1] = yield proxy.get_controller (1);

        if (controllers[0] == LIGHTGUN || controllers[1] == LIGHTGUN) {
            runner.uses_pointer = true;
            runner.set_cursor (ScreenSet.DEFAULT_SCREEN, CROSSHAIR);
        }
    }

    public override async void after_reset () throws Error {
        var proxy = runner.platform_proxy as Proxy;

        left_difficulty = yield proxy.get_default_difficulty (0);
        right_difficulty = yield proxy.get_default_difficulty (1);
    }

    public override async void save_state (KeyFile metadata) throws Error {
        string[] names = { "Left", "Right" };
        for (int i = 0; i < Hs.ATARI_7800_MAX_PLAYERS; i++) {
            var name = names[i];
            var diff_str = "";

            switch (difficulty[0]) {
                case ADVANCED:
                    diff_str = "Advanced";
                    break;
                case BEGINNER:
                    diff_str = "Beginner";
                    break;
                default:
                    assert_not_reached ();
            }

            metadata.set_string ("Atari 7800", @"$name Difficulty", diff_str);
        }
    }

    public override async void load_state (KeyFile metadata) throws Error {
        string[] names = { "Left", "Right" };
        for (int i = 0; i < Hs.ATARI_7800_MAX_PLAYERS; i++) {
            var name = names[i];
            Hs.Atari7800Difficulty diff;
            string diff_str;

            try {
                diff_str = metadata.get_string ("Atari 7800", @"$name Difficulty");
            } catch (KeyFileError e) {
                diff_str = "Advanced";
            }

            if (diff_str == "Advanced") {
                diff = ADVANCED;
            } else if (diff_str == "Beginner") {
                diff = BEGINNER;
            } else {
                warning ("Unsupported Atari 7800 difficulty: %s", diff_str);
                diff = ADVANCED;
            }

            if (i == 0)
                left_difficulty = diff;
            else
                right_difficulty = diff;
        }
    }

    private void set_lightgun_state (bool fire, double x, double y) {
        runner.input_buffer.modify_input_state (state => {
            state->atari_7800.lightgun_fire = fire;
            state->atari_7800.lightgun_x = x;
            state->atari_7800.lightgun_y = y;
        });
    }

    public override bool drag_begin (int screen_id, double x, double y) {
        if (controllers[0] == LIGHTGUN || controllers[1] == LIGHTGUN)
            set_lightgun_state (true, x, y);

        return true;
    }

    public override void drag_update (double x, double y) {
        if (controllers[0] == LIGHTGUN || controllers[1] == LIGHTGUN)
            set_lightgun_state (true, x, y);
    }

    public override void drag_end (double x, double y) {
        if (controllers[0] == LIGHTGUN || controllers[1] == LIGHTGUN)
            set_lightgun_state (false, x, y);
    }

    public override Gtk.Widget? create_header_widget () {
        var menu = new SwitchMenu ();

        bind_property (
            "left-difficulty", menu, "left-difficulty", BIDIRECTIONAL | SYNC_CREATE
        );
        bind_property (
            "right-difficulty", menu, "right-difficulty", BIDIRECTIONAL | SYNC_CREATE
        );
        menu.pause.connect (() => {
            var proxy = runner.platform_proxy as Proxy;
            proxy.press_pause.begin ();
        });
        menu.select.connect (() => {
            var proxy = runner.platform_proxy as Proxy;
            proxy.press_select.begin ();
        });
        menu.reset.connect (() => {
            var proxy = runner.platform_proxy as Proxy;
            proxy.press_reset.begin ();
        });

        return menu;
    }
}
