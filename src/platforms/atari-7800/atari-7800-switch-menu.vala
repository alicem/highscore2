// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore2/platforms/atari-7800/atari-7800-switch-menu.ui")]
public class Highscore.Atari7800.SwitchMenu : Adw.Bin {
    [GtkChild]
    private unowned SwitchIcon icon;

    public Hs.Atari7800Difficulty left_difficulty { get; set; }
    public Hs.Atari7800Difficulty right_difficulty { get; set; }

    public signal void pause ();
    public signal void select ();
    public signal void reset ();

    construct {
        notify["left-difficulty"].connect (() => {
            icon.set_switch_state (0, left_difficulty == ADVANCED);
        });
        notify["right-difficulty"].connect (() => {
            icon.set_switch_state (1, right_difficulty == ADVANCED);
        });
    }

    static construct {
        install_property_action ("atari7800.left-difficulty", "left-difficulty");
        install_property_action ("atari7800.right-difficulty", "right-difficulty");
        install_action ("atari7800.pause", null, widget => {
            var self = widget as SwitchMenu;
            self.pause ();
        });
        install_action ("atari7800.select", null, widget => {
            var self = widget as SwitchMenu;
            self.select ();
        });
        install_action ("atari7800.reset", null, widget => {
            var self = widget as SwitchMenu;
            self.reset ();
        });
    }
}
