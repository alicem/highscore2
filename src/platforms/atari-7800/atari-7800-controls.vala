// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Atari7800.Controls : PlatformControls {
    construct {
        var controller = new PlatformController ("joystick", _("Joystick"));
        ControlHelpers.add_dpad (
            controller, set_pressed,
            "joystick", _("Joystick"),
            Hs.Atari7800JoystickButton.UP,
            Hs.Atari7800JoystickButton.DOWN,
            Hs.Atari7800JoystickButton.LEFT,
            Hs.Atari7800JoystickButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed,
            "one", _("One"), Hs.Atari7800JoystickButton.ONE,
            "two", _("Two"), Hs.Atari7800JoystickButton.TWO,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,    "joystick:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,  "joystick:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "joystick:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "joystick:right");
        kbd_mapping.map (Linux.Input.KEY_S,     "one");
        kbd_mapping.map (Linux.Input.KEY_D,     "two");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (UP,    "joystick:up");
        gamepad_mapping.map_button (DOWN,  "joystick:down");
        gamepad_mapping.map_button (LEFT,  "joystick:left");
        gamepad_mapping.map_button (RIGHT, "joystick:right");
        gamepad_mapping.map_button (SOUTH, "one");
        gamepad_mapping.map_button (EAST,  "two");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);
        default_controller = "joystick";

        // lightgun

        controller = new PlatformController ("lightgun", _("Light Gun"));
        add_controller (controller);

        // global

        global_controller = new PlatformController ("console", _("Built-in"));

        var button = new ButtonControl ("left-difficulty", _("Left Difficulty"));
        button.activate.connect (left_difficulty_cb);
        global_controller.add_control (button);

        button = new ButtonControl ("right-difficulty", _("Right Difficulty"));
        button.activate.connect (right_difficulty_cb);
        global_controller.add_control (button);

        button = new ButtonControl ("pause", _("Pause"));
        button.activate.connect (pause_cb);
        global_controller.add_control (button);

        button = new ButtonControl ("select", _("Select"));
        button.activate.connect (select_cb);
        global_controller.add_control (button);

        button = new ButtonControl ("reset", _("Reset"));
        button.activate.connect (reset_cb);
        global_controller.add_control (button);

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_1,         "left-difficulty");
        kbd_mapping.map (Linux.Input.KEY_2,         "right-difficulty");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "pause");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        kbd_mapping.map (Linux.Input.KEY_W,         "reset");
        global_controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (L,      "left-difficulty");
        gamepad_mapping.map_button (R,      "right-difficulty");
        gamepad_mapping.map_button (START,  "pause");
        gamepad_mapping.map_button (SELECT, "select");
        gamepad_mapping.map_button (NORTH,  "reset");
        global_controller.default_gamepad_mapping = gamepad_mapping;
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->atari_7800.joystick[player], button, pressed);
        });
    }

    private static void left_difficulty_cb (Runner runner, uint player, bool pressed) {
        if (pressed)
            return;

        var delegate = runner.delegate as RunnerDelegate;

        if (delegate.left_difficulty == ADVANCED)
            delegate.left_difficulty = BEGINNER;
        else
            delegate.left_difficulty = ADVANCED;
    }

    private static void right_difficulty_cb (Runner runner, uint player, bool pressed) {
        if (pressed)
            return;

        var delegate = runner.delegate as RunnerDelegate;

        if (delegate.right_difficulty == ADVANCED)
            delegate.right_difficulty = BEGINNER;
        else
            delegate.right_difficulty = ADVANCED;
    }

    private static void pause_cb (Runner runner, uint player, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            state->atari_7800.pause_button = pressed;
        });
    }

    private static void select_cb (Runner runner, uint player, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            state->atari_7800.select_button = pressed;
        });
    }

    private static void reset_cb (Runner runner, uint player, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            state->atari_7800.reset_button = pressed;
        });
    }
}
