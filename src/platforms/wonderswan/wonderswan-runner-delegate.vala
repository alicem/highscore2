// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.WonderSwan.RunnerDelegate : Highscore.RunnerDelegate {
    public bool rotated { get; set; }

    private Settings settings;

    construct {
        update_rotation ();

        notify["rotated"].connect (update_rotation);

        settings = new GameSettings (runner.game).get_platform ();

        settings.bind ("rotated", this, "rotated", DEFAULT);
    }

    private void update_rotation () {
        runner.set_controller_type (
            0, rotated ? "wswan-portrait" : "wswan-landscape"
        );
    }

    public override Gtk.Widget? create_header_widget () {
        var button = new OrientationButton ();

        bind_property ("rotated", button, "rotated", SYNC_CREATE | BIDIRECTIONAL);

        return button;
    }
}
