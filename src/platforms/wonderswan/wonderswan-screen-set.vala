// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.WonderSwan.ScreenSet : Highscore.ScreenSet {
    public bool rotated { get; set; }

    private Settings settings;

    construct {
        notify["rotated"].connect (() => {
            if (rotated)
                layout = new SingleLayout (DEFAULT_SCREEN, 270_DEG);
            else
                layout = new SingleLayout (DEFAULT_SCREEN);

            layout_changed (true);
        });

        settings = new GameSettings (game).get_platform ();

        settings.bind ("rotated", this, "rotated", GET);
    }
}
