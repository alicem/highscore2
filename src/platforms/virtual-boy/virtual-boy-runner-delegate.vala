// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.VirtualBoy.RunnerDelegate : Highscore.RunnerDelegate {
    public StereoMode stereo_mode { get; set; }
    public bool gray_colors { get; set; }

    private Settings settings;

    construct {
        settings = new Settings ("org.gnome.World.Highscore2.platforms.virtual-boy");

        settings.bind_with_mapping (
            "stereo-mode", this, "stereo-mode", DEFAULT,
            (value, variant, user_data) => {
                var str = variant.get_string ();
                var mode = StereoMode.from_string (str);

                if (mode == null) {
                    critical ("Unknown stereo mode: %s", str);
                    value = StereoMode.LEFT_ONLY;
                } else {
                    value = (StereoMode) mode;
                }

                return true;
            },
            (value, variant_type, user_data) => {
                var mode = (StereoMode) value.get_enum ();
                return mode.to_string ();
            },
            null, null
        );

        settings.bind ("gray-colors", this, "gray-colors", DEFAULT);
    }

    public override Gtk.Widget? create_header_widget () {
        var switcher = new StereoSwitcher ();

        bind_property ("stereo-mode", switcher, "stereo-mode", SYNC_CREATE | BIDIRECTIONAL);
        bind_property ("gray-colors", switcher, "gray-colors", SYNC_CREATE | BIDIRECTIONAL);

        return switcher;
    }
}
