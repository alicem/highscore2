// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.VirtualBoy.Controls : PlatformControls {
    construct {
        var controller = new PlatformController ("vb", "");
        ControlHelpers.add_dpad (
            controller, set_pressed,
            "ldpad", _("Left D-pad"),
            Hs.VirtualBoyButton.L_UP,
            Hs.VirtualBoyButton.L_DOWN,
            Hs.VirtualBoyButton.L_LEFT,
            Hs.VirtualBoyButton.L_RIGHT
        );
        ControlHelpers.add_dpad (
            controller, set_pressed,
            "rdpad", _("Right D-pad"),
            Hs.VirtualBoyButton.R_UP,
            Hs.VirtualBoyButton.R_DOWN,
            Hs.VirtualBoyButton.R_LEFT,
            Hs.VirtualBoyButton.R_RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed,
            "a",      _("A"),      Hs.VirtualBoyButton.A,
            "b",      _("B"),      Hs.VirtualBoyButton.B,
            "l",      _("L"),      Hs.VirtualBoyButton.L,
            "r",      _("R"),      Hs.VirtualBoyButton.R,
            "select", _("Select"), Hs.VirtualBoyButton.SELECT,
            "start",  _("Start"),  Hs.VirtualBoyButton.START,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,        "ldpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "ldpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "ldpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "ldpad:right");
        kbd_mapping.map (Linux.Input.KEY_W,         "rdpad:up");
        kbd_mapping.map (Linux.Input.KEY_S,         "rdpad:down");
        kbd_mapping.map (Linux.Input.KEY_A,         "rdpad:left");
        kbd_mapping.map (Linux.Input.KEY_D,         "rdpad:right");
        kbd_mapping.map (Linux.Input.KEY_Z,         "b");
        kbd_mapping.map (Linux.Input.KEY_X,         "a");
        kbd_mapping.map (Linux.Input.KEY_Q,         "l");
        kbd_mapping.map (Linux.Input.KEY_E,         "r");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "start");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (UP,     "ldpad:up");
        gamepad_mapping.map_button (DOWN,   "ldpad:down");
        gamepad_mapping.map_button (LEFT,   "ldpad:left");
        gamepad_mapping.map_button (RIGHT,  "ldpad:right");
        gamepad_mapping.map_button (SOUTH,  "b");
        gamepad_mapping.map_button (EAST,   "a");
        gamepad_mapping.map_button (L,      "l");
        gamepad_mapping.map_button (R,      "r");
        gamepad_mapping.map_button (SELECT, "select");
        gamepad_mapping.map_button (START,  "start");
        gamepad_mapping.map_analog (LEFT,   "ldpad");
        gamepad_mapping.map_analog (RIGHT,  "rdpad");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);
        default_controller = "vb";
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->virtual_boy.buttons, button, pressed);
        });
    }
}
