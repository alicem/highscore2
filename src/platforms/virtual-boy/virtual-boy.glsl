#ifdef VERTEX

void hs_main() {
}

#else // FRAGMENT

uniform mat4 u_colorMatrix;

vec4 hs_main() {
  return u_colorMatrix * hs_texture(uv);
}

#endif