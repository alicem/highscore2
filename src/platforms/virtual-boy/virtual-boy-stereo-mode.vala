// This file is part of Highscore. License: GPL-3.0+.

public enum Highscore.VirtualBoy.StereoMode {
    LEFT_ONLY,
    RIGHT_ONLY,
    RED_BLUE,
    RED_CYAN,
    SIDE_BY_SIDE;

    public string to_string () {
        switch (this) {
            case LEFT_ONLY:
                return "left-only";
            case RIGHT_ONLY:
                return "right-only";
            case RED_BLUE:
                return "red-blue";
            case RED_CYAN:
                return "red-cyan";
            case SIDE_BY_SIDE:
                return "side-by-side";
            default:
                assert_not_reached ();
        }
    }

    public static StereoMode? from_string (string str) {
        if (str == "left-only")
            return LEFT_ONLY;
        if (str == "right-only")
            return RIGHT_ONLY;
        if (str == "red-blue")
            return RED_BLUE;
        if (str == "red-cyan")
            return RED_CYAN;
        if (str == "side-by-side")
            return SIDE_BY_SIDE;

        return null;
    }
}
