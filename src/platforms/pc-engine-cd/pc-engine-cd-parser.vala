// This file is part of Highscore. License: GPL-3.0+.

public errordomain Highscore.PcEngineCd.ParserError {
    MISSING_BIN,
}

public class Highscore.PcEngineCd.Parser : GameParser {
    private const string CUE_MIME_TYPE = "application/x-cue";
    private const string CD_MAGIC_VALUE = "PC Engine CD-ROM SYSTEM";

    private string bin_path;

    public Parser (Platform platform, File file) {
        base (platform, file);
    }

    public override bool parse () throws Error {
        var file_info = file.query_info (FileAttribute.STANDARD_CONTENT_TYPE, FileQueryInfoFlags.NONE);
        var mime_type = file_info.get_content_type ();

        File bin_file;
        switch (mime_type) {
        case CUE_MIME_TYPE:
            var cue = new CueSheet (file);
            if (cue.tracks_number < 2)
                return false;

            var track = cue.get_track (1);
            if (!track.track_mode.is_mode1 ())
                return false;

            bin_file = track.file.file;

            break;
        // TODO Add support for binary files.
        default:
            return false;
        }

        bin_path = bin_file.get_path ();

        if (!bin_file.query_exists ())
            throw new ParserError.MISSING_BIN ("Missing the binary file '%s'", bin_file.get_path ());

        var offsets = Grep.get_offsets (bin_path, CD_MAGIC_VALUE);

        return offsets.length > 0;
    }

    public override string[]? get_extra_paths () {
        return { bin_path };
    }
}
