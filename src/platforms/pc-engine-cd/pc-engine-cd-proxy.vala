// This file is part of Highscore. License: GPL-3.0+.

[DBus (name = "org.gnome.World.Highscore2.Runner")]
public interface Highscore.PcEngineCd.Proxy : Object {
    public abstract async void set_bios_path (string path) throws Error;
}
