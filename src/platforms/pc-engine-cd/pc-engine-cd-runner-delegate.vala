// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.PcEngineCd.RunnerDelegate : PcEngine.RunnerDelegate {
    public override async void before_load () throws Error {
        var pce_cd_proxy = runner.addon_proxy as Proxy;

        var data_dir = Environment.get_user_data_dir ();
        var bios_path = Path.build_filename (data_dir, "highscore", "bios", "syscard3.pce");
        var bios_file = File.new_for_path (bios_path);

        if (bios_file.query_exists ())
            yield pce_cd_proxy.set_bios_path (bios_file.get_path ());
    }
}
