// This file is part of Highscore. License: GPL-3.0+.

public enum Highscore.NintendoDs.LayoutMode {
    VERTICAL,
    SIDE_BY_SIDE,
    SINGLE_SCREEN,
    SEPARATE_WINDOWS;

    public string to_string () {
        switch (this) {
            case VERTICAL:
                return "vertical";
            case SIDE_BY_SIDE:
                return "side-by-side";
            case SINGLE_SCREEN:
                return "single-screen";
            case SEPARATE_WINDOWS:
                return "separate-windows";
            default:
                assert_not_reached ();
        }
    }

    public static LayoutMode? from_string (string str) {
        if (str == "vertical")
            return VERTICAL;
        if (str == "side-by-side")
            return SIDE_BY_SIDE;
        if (str == "single-screen")
            return SINGLE_SCREEN;
        if (str == "separate-windows")
            return SEPARATE_WINDOWS;

        return null;
    }
}
