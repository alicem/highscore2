// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.NintendoDs.Controls : PlatformControls {
    construct {
        var controller = new PlatformController ("ds", "");
        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("D-pad"),
            Hs.NintendoDsButton.UP,
            Hs.NintendoDsButton.DOWN,
            Hs.NintendoDsButton.LEFT,
            Hs.NintendoDsButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed,
            "a",      _("A"),      Hs.NintendoDsButton.A,
            "b",      _("B"),      Hs.NintendoDsButton.B,
            "x",      _("X"),      Hs.NintendoDsButton.X,
            "y",      _("Y"),      Hs.NintendoDsButton.Y,
            "l",      _("L"),      Hs.NintendoDsButton.L,
            "r",      _("R"),      Hs.NintendoDsButton.R,
            "select", _("Select"), Hs.NintendoDsButton.SELECT,
            "start",  _("Start"),  Hs.NintendoDsButton.START,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,        "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_W,         "x");
        kbd_mapping.map (Linux.Input.KEY_A,         "y");
        kbd_mapping.map (Linux.Input.KEY_S,         "b");
        kbd_mapping.map (Linux.Input.KEY_D,         "a");
        kbd_mapping.map (Linux.Input.KEY_Q,         "l");
        kbd_mapping.map (Linux.Input.KEY_E,         "r");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "start");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (UP,     "dpad:up");
        gamepad_mapping.map_button (DOWN,   "dpad:down");
        gamepad_mapping.map_button (LEFT,   "dpad:left");
        gamepad_mapping.map_button (RIGHT,  "dpad:right");
        gamepad_mapping.map_button (SOUTH,  "b");
        gamepad_mapping.map_button (EAST,   "a");
        gamepad_mapping.map_button (WEST,   "y");
        gamepad_mapping.map_button (NORTH,  "x");
        gamepad_mapping.map_button (L,      "l");
        gamepad_mapping.map_button (R,      "r");
        gamepad_mapping.map_button (SELECT, "select");
        gamepad_mapping.map_button (START,  "start");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);
        default_controller = "ds";

        // global

        global_controller = new PlatformController ("shortcuts", _("Shortcuts"));
        var shortcut = new ButtonControl ("swap-screens", _("Swap Screens"));
        shortcut.activate.connect (swap_screens_cb);
        global_controller.add_control (shortcut);

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_SPACE, "swap-screens");
        global_controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (R3, "swap-screens");
        global_controller.default_gamepad_mapping = gamepad_mapping;
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->nintendo_ds.buttons, button, pressed);
        });
    }

    private static void swap_screens_cb (Runner runner, uint player, bool pressed) {
        if (!pressed)
            return;

        var delegate = runner.delegate as RunnerDelegate;
        delegate.swap_screens ();
    }
}
