// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore2/platforms/nintendo-ds/nintendo-ds-layout-switcher.ui")]
public class Highscore.NintendoDs.LayoutSwitcher : Adw.Bin {
    public LayoutMode layout_mode { get; set; }
    public bool bottom_screen_left { get; set; }
    public bool show_bottom_screen { get; set; }
    public bool allow_rotation { get; set; }
    public bool rotate_clockwise { get; set; }

    [GtkChild]
    private unowned Gtk.Revealer extra_button_revealer;
    [GtkChild]
    private unowned Gtk.Stack extra_button_stack;
    [GtkChild]
    private unowned Gtk.Button screen_button;
    [GtkChild]
    private unowned Gtk.Button rotate_button;
    [GtkChild]
    private unowned Gtk.Button swap_button;

    construct {
        update_state ();

        notify["layout-mode"].connect (update_state);
        notify["allow-rotation"].connect (update_state);

        notify["show-bottom-screen"].connect (() => {
            if (show_bottom_screen)
                screen_button.add_css_class ("flipped");
            else
                screen_button.remove_css_class ("flipped");
        });
    }

    static construct {
        install_property_action ("ds.layout-mode", "layout-mode");
        install_property_action ("ds.bottom-screen-left", "bottom-screen-left");
        install_property_action ("ds.toggle-screen", "show-bottom-screen");
        install_property_action ("ds.allow-rotation", "allow-rotation");
        install_property_action ("ds.rotate-clockwise", "rotate-clockwise");

        install_action ("ds.swap-screens", null, widget => {
            var switcher = widget as LayoutSwitcher;

            switcher.show_bottom_screen = !switcher.show_bottom_screen;
        });
    }

    private void update_state () {
        action_set_enabled ("ds.allow-rotation", layout_mode == VERTICAL);
        action_set_enabled ("ds.swap-screens", layout_mode == SEPARATE_WINDOWS);

        bool show_rotate = layout_mode == VERTICAL && allow_rotation;
        bool show_toggle = layout_mode == SINGLE_SCREEN;
        bool show_swap = layout_mode == SIDE_BY_SIDE;

        extra_button_revealer.reveal_child = show_toggle || show_rotate || show_swap;
        if (show_toggle)
            extra_button_stack.visible_child = screen_button;
        else if (show_rotate)
            extra_button_stack.visible_child = rotate_button;
        else if (show_swap)
            extra_button_stack.visible_child = swap_button;
    }
}
