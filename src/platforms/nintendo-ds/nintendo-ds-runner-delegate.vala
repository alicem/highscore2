// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.NintendoDs.RunnerDelegate : Highscore.RunnerDelegate {
    public LayoutMode layout_mode { get; set; }
    public bool bottom_screen_left { get; set; }
    public bool show_bottom_screen { get; set; }
    public bool allow_rotation { get; set; }
    public bool rotate_clockwise { get; set; }

    public LayoutMode last_layout_mode = VERTICAL;

    private Settings settings;

    construct {
        notify["layout-mode"].connect (() => {
            update_view_names ();

            if (layout_mode != SEPARATE_WINDOWS)
                last_layout_mode = layout_mode;
        });
        notify["show-bottom-screen"].connect (update_view_names);

        var game_settings = new GameSettings (runner.game);

        settings = game_settings.get_platform ();

        settings.bind_with_mapping (
            "layout-mode", this, "layout-mode", DEFAULT,
            (value, variant, user_data) => {
                var str = variant.get_string ();
                var mode = LayoutMode.from_string (str);

                if (mode == null) {
                    critical ("Unknown layout mode: %s", str);
                    value = LayoutMode.VERTICAL;
                } else {
                    value = (LayoutMode) mode;
                }

                return true;
            },
            (value, variant_type, user_data) => {
                var mode = (LayoutMode) value.get_enum ();
                return mode.to_string ();
            },
            null, null
        );

        settings.bind ("bottom-screen-left", this, "bottom-screen-left", DEFAULT);
        settings.bind ("show-bottom-screen", this, "show-bottom-screen", DEFAULT);
        settings.bind ("allow-rotation",     this, "allow-rotation",     DEFAULT);
        settings.bind ("rotate-clockwise",   this, "rotate-clockwise",   DEFAULT);
    }

    public override async void before_load () throws Error {
        runner.uses_pointer = true;
        runner.set_cursor (ScreenSet.TOUCH_ID, CROSSHAIR);
    }

    private void update_view_names () {
        string name, secondary_name;

        if (layout_mode == SEPARATE_WINDOWS) {
            name = show_bottom_screen ? _("Bottom Screen") : _("Top Screen");
            secondary_name = show_bottom_screen ? _("Top Screen") : _("Bottom Screen");
        } else {
            name = secondary_name = "";
        }

        runner.view_name = name;
        runner.secondary_view_name = secondary_name;
    }

    public override void secondary_view_closed () {
        if (layout_mode == SEPARATE_WINDOWS)
            layout_mode = last_layout_mode;
    }

    private void set_touch_state (bool pressed, double x, double y) {
        runner.input_buffer.modify_input_state (state => {
            state->nintendo_ds.touch_pressed = pressed;
            state->nintendo_ds.touch_x = x;
            state->nintendo_ds.touch_y = y;
        });
    }

    public override bool drag_begin (int screen_id, double x, double y) {
        if (screen_id != ScreenSet.TOUCH_ID)
            return false;

        set_touch_state (true, x, y);
        return true;
    }

    public override void drag_update (double x, double y) {
        set_touch_state (true, x, y);
    }

    public override void drag_end (double x, double y) {
        set_touch_state (false, 0, 0);
    }

    public override Gtk.Widget? create_header_widget () {
        var switcher = new LayoutSwitcher ();

        bind_property (
            "layout-mode", switcher, "layout-mode", SYNC_CREATE | BIDIRECTIONAL
        );
        bind_property (
            "bottom-screen-left", switcher, "bottom-screen-left", SYNC_CREATE | BIDIRECTIONAL
        );
        bind_property (
            "show-bottom-screen", switcher, "show-bottom-screen", SYNC_CREATE | BIDIRECTIONAL
        );
        bind_property (
            "allow-rotation", switcher, "allow-rotation", SYNC_CREATE | BIDIRECTIONAL
        );
        bind_property (
            "rotate-clockwise", switcher, "rotate-clockwise", SYNC_CREATE | BIDIRECTIONAL
        );

        return switcher;
    }

    public void swap_screens () {
        if (layout_mode == VERTICAL && allow_rotation)
            rotate_clockwise = !rotate_clockwise;

        if (layout_mode == SIDE_BY_SIDE)
            bottom_screen_left = !bottom_screen_left;

        if (layout_mode == SINGLE_SCREEN)
            show_bottom_screen = !show_bottom_screen;
    }
}
