// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.NintendoDs.ScreenSet : Highscore.ScreenSet {
    public const int MAIN_ID = 0;
    public const int TOUCH_ID = 1;

    public LayoutMode layout_mode { get; set; }
    public bool bottom_screen_left { get; set; }
    public bool show_bottom_screen { get; set; }
    public bool allow_rotation { get; set; }
    public bool rotate_clockwise { get; set; }

    private Settings settings;

    construct {
        notify["layout-mode"].connect (() => {
            update_layout (true);
        });
        notify["bottom-screen-left"].connect (() => {
            update_layout (true);
        });
        notify["show-bottom-screen"].connect (() => {
            update_layout (true);
        });
        notify["allow-rotation"].connect (() => {
            update_layout (true);
        });
        notify["rotate-clockwise"].connect (() => {
            update_layout (true);
        });

        update_layout (false);

        settings = new GameSettings (game).get_platform ();

        settings.bind_with_mapping (
            "layout-mode", this, "layout-mode", GET,
            (value, variant, user_data) => {
                var str = variant.get_string ();
                var mode = LayoutMode.from_string (str);

                if (mode == null) {
                    critical ("Unknown layout mode: %s", str);
                    value = LayoutMode.VERTICAL;
                } else {
                    value = (LayoutMode) mode;
                }

                return true;
            },
            (value, variant_type, user_data) => {
                var mode = (LayoutMode) value.get_enum ();
                return mode.to_string ();
            },
            null, null
        );

        settings.bind ("bottom-screen-left", this, "bottom-screen-left", GET);
        settings.bind ("show-bottom-screen", this, "show-bottom-screen", GET);
        settings.bind ("allow-rotation",     this, "allow-rotation",     GET);
        settings.bind ("rotate-clockwise",   this, "rotate-clockwise",   GET);
    }

    private void update_layout (bool animate) {
        var metadata = game.metadata as Metadata;
        float gap = metadata.get_screen_gap ();

        switch (layout_mode) {
            case VERTICAL:
                if (allow_rotation && rotate_clockwise)
                    layout = new VerticalLayout (gap, 90_DEG);
                else if (allow_rotation)
                    layout = new VerticalLayout (gap, 270_DEG);
                else
                    layout = new VerticalLayout (gap);
                secondary_layout = null;
                break;
            case SIDE_BY_SIDE:
                layout = new HorizontalLayout (bottom_screen_left);
                secondary_layout = null;
                break;
            case SINGLE_SCREEN:
                layout = new FocusLayout (
                    show_bottom_screen ? TOUCH_ID : MAIN_ID, gap
                );
                secondary_layout = null;
                break;
            case SEPARATE_WINDOWS:
                layout = new SingleLayout (
                    show_bottom_screen ? TOUCH_ID : MAIN_ID
                );
                secondary_layout = new SingleLayout (
                    show_bottom_screen ? MAIN_ID : TOUCH_ID
                );
                break;
            default:
                assert_not_reached ();
        }

        layout_changed (animate);
        secondary_layout_changed (animate);
    }

    public override Screen[] get_screens () {
        var top_screen    = new Screen (MAIN_ID,  {{ 0, 0    }, { 1, 0.5f }});
        var bottom_screen = new Screen (TOUCH_ID, {{ 0, 0.5f }, { 1, 0.5f }});

        return ({ top_screen, bottom_screen });
    }
}
