// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Sg1000.Controls : PlatformControls {
    construct {
        var controller = new PlatformController ("pad", _("Pad"));
        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("D-pad"),
            Hs.Sg1000Button.UP,
            Hs.Sg1000Button.DOWN,
            Hs.Sg1000Button.LEFT,
            Hs.Sg1000Button.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed,
            "one",    _("One"),   Hs.Sg1000Button.ONE,
            "two",    _("Two"),   Hs.Sg1000Button.TWO,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,    "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,  "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_S,     "one");
        kbd_mapping.map (Linux.Input.KEY_D,     "two");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (UP,    "dpad:up");
        gamepad_mapping.map_button (DOWN,  "dpad:down");
        gamepad_mapping.map_button (LEFT,  "dpad:left");
        gamepad_mapping.map_button (RIGHT, "dpad:right");
        gamepad_mapping.map_button (SOUTH, "one");
        gamepad_mapping.map_button (EAST,  "two");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);
        default_controller = "pad";

        // global

        global_controller = new PlatformController ("console", _("Built-in"));
        var pause_button = new ButtonControl ("pause", _("Pause"));
        pause_button.activate.connect ((runner, player, pressed) => {
            set_pause_button_pressed (runner, pressed);
        });
        global_controller.add_control (pause_button);

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_ENTER, "pause");
        global_controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (START, "pause");
        global_controller.default_gamepad_mapping = gamepad_mapping;
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->sg1000.pad_buttons[player], button, pressed);
        });
    }

    private void set_pause_button_pressed (Runner runner, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            state->sg1000.pause_button = pressed;
        });
    }
}
