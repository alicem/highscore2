// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Sg1000.RunnerDelegate : Highscore.RunnerDelegate {
    public override Gtk.Widget? create_header_widget () {
        var pause_button = new Gtk.Button () {
            icon_name = "media-playback-pause-symbolic",
            tooltip_text = _("Pause"),
        };

        pause_button.clicked.connect (() => {
            var proxy = runner.platform_proxy as Proxy;
            proxy.press_pause.begin ();
        });

        return pause_button;
    }
}
