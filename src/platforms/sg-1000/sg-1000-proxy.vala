// This file is part of Highscore. License: GPL-3.0+.

[DBus (name = "org.gnome.World.Highscore2.Runner")]
public interface Highscore.Sg1000.Proxy : Object {
    public abstract async void press_pause () throws Error;
}
