// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Fds.RunnerDelegate : Nes.RunnerDelegate {
    public uint n_sides { get; private set; }
    public uint side { get; set; }

    construct {
        notify["side"].connect (() => {
            var proxy = runner.addon_proxy as Proxy;
            proxy.set_side.begin (side);
        });
    }

    public override async void before_load () throws Error {
        yield base.before_load ();

        var proxy = runner.addon_proxy as Proxy;

        var data_dir = Environment.get_user_data_dir ();
        var bios_path = Path.build_filename (data_dir, "highscore", "bios", "disksys.rom");
        var bios_file = File.new_for_path (bios_path);

        if (bios_file.query_exists ())
            yield proxy.set_bios_path (bios_file.get_path ());
    }

    public override async void after_load () throws Error {
        yield base.after_load ();

        var proxy = runner.addon_proxy as Proxy;

        n_sides = yield proxy.get_n_sides ();
    }

    public override async void after_reset () throws Error {
        yield base.after_reset ();

        side = 0;
    }

    public override async void load_state (KeyFile metadata) throws Error {
        yield base.load_state (metadata);

        var proxy = runner.addon_proxy as Proxy;

        side = yield proxy.get_side ();
    }

    public override Gtk.Widget? create_header_widget () {
        var disk_switcher = new DiskSwitcher ();

        bind_property ("n-sides", disk_switcher, "n-sides", SYNC_CREATE);
        bind_property ("side", disk_switcher, "side", SYNC_CREATE | BIDIRECTIONAL);

        var box = new Gtk.Box (HORIZONTAL, 6);
        box.append (disk_switcher);
        box.append (base.create_header_widget ());

        return box;
    }
}
