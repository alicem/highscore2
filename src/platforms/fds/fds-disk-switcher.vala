// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Fds.DiskSwitcher : Adw.Bin {
    public uint n_sides { get; set; }
    public uint side { get; set; }

    private Gtk.Box sides_box;
    private Gtk.ToggleButton[] side_buttons;

    construct {
        populate ();

        notify["n-sides"].connect (populate);
        notify["side"].connect (() => {
            if (side >= n_sides || n_sides < 2)
                return;

            side_buttons[side].active = true;
        });
    }

    private void populate () {
        sides_box = new Gtk.Box (HORIZONTAL, 0);
        sides_box.add_css_class ("linked");
        sides_box.add_css_class ("disk-switcher");
        child = sides_box;

        side_buttons = {};

        if (n_sides < 2) {
            visible = false;

            return;
        }

        Gtk.ToggleButton first_button = null;

        for (uint i = 0; i < n_sides; i++) {
            var label = "";

            if (n_sides == 2)
                label = i == 0 ? "A" : "B";
            else
                label = "%u%s".printf ((i / 2) + 1, (i % 2 == 0) ? "A" : "B");

            var button = new Gtk.ToggleButton.with_label (label);

            if (i == 0)
                first_button = button;
            else
                button.group = first_button;

            if (i == side)
                button.active = true;

            button.set_data ("side", i);

            button.notify["active"].connect (() => {
                if (!button.active)
                    return;

                side = button.get_data ("side");
            });

            side_buttons += button;
            sides_box.append (button);
        }

        visible = true;
    }
}
