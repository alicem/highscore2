// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Atari2600.RunnerDelegate : Highscore.RunnerDelegate {
    private Hs.Atari2600Difficulty difficulty[Hs.ATARI_2600_MAX_PLAYERS];
    private double paddle_position[Hs.ATARI_2600_MAX_PLAYERS * 2];

    private Hs.Atari2600TVType _tv_type;
    public Hs.Atari2600TVType tv_type {
        get { return _tv_type; }
        set {
            if (tv_type == value)
                return;

            _tv_type = value;

            runner.input_buffer.modify_input_state (state => {
                state->atari_2600.tv_type = tv_type;
            });
        }
    }

    public Hs.Atari2600Difficulty left_difficulty {
        get { return difficulty[0]; }
        set {
            if (left_difficulty == value)
                return;

            difficulty[0] = value;

            runner.input_buffer.modify_input_state (state => {
                state->atari_2600.difficulty[0] = left_difficulty;
            });
        }
    }

    public Hs.Atari2600Difficulty right_difficulty {
        get { return difficulty[1]; }
        set {
            if (right_difficulty == value)
                return;

            difficulty[1] = value;

            runner.input_buffer.modify_input_state (state => {
                state->atari_2600.difficulty[1] = right_difficulty;
            });
        }
    }

    public override async void after_load () throws Error {
        var proxy = runner.platform_proxy as Proxy;

        var left_controller = yield proxy.get_controller (0);
        var left_type = get_controller_type (left_controller);

        var right_controller = yield proxy.get_controller (1);
        var right_type = get_controller_type (right_controller);

        // There are two paddles in each port, so we present them as two players

        int port = 0;
        runner.set_controller_type (port++, left_type);

        if (left_controller == PADDLES)
            runner.set_controller_type (port++, left_type);

        runner.set_controller_type (port++, right_type);

        if (right_controller == PADDLES)
            runner.set_controller_type (port++, right_type);
    }

    private string? get_controller_type (Hs.Atari2600Controller controller) {
        switch (controller) {
            case NONE:
                return null;
            case JOYSTICK:
                return "joystick";
            case DRIVING:
                return "driving";
            case PADDLES:
                return "paddle";
            default:
                return null;
        }
    }

    public override async void after_reset () throws Error {
        var proxy = runner.platform_proxy as Proxy;

        tv_type = COLOR;
        left_difficulty = yield proxy.get_default_difficulty (0);
        right_difficulty = yield proxy.get_default_difficulty (1);
    }

    public override async void save_state (KeyFile metadata) throws Error {
        var tv_type_str = "";

        switch (tv_type) {
            case COLOR:
                tv_type_str = "Color";
                break;
            case BLACK_WHITE:
                tv_type_str = "Black & White";
                break;
            default:
                assert_not_reached ();
        }

        metadata.set_string ("Atari 2600", "TV Type", tv_type_str);

        string[] names = { "Left", "Right" };
        for (int i = 0; i < Hs.ATARI_2600_MAX_PLAYERS; i++) {
            var name = names[i];
            var diff_str = "";

            switch (difficulty[i]) {
                case ADVANCED:
                    diff_str = "Advanced";
                    break;
                case BEGINNER:
                    diff_str = "Beginner";
                    break;
                default:
                    assert_not_reached ();
            }

            metadata.set_string ("Atari 2600", @"$name Difficulty", diff_str);
        }
    }

    public override async void load_state (KeyFile metadata) throws Error {
        string tv_type_str;

        try {
            tv_type_str = metadata.get_string ("Atari 2600", "TV Type");
        } catch (KeyFileError e) {
            tv_type_str = "Color";
        }

        if (tv_type_str == "Color") {
            tv_type = COLOR;
        } else if (tv_type_str == "Black & White") {
            tv_type = BLACK_WHITE;
        } else {
            warning ("Unsupported Atari 2600 TV type: %s", tv_type_str);
            tv_type = COLOR;
        }

        string[] names = { "Left", "Right" };
        for (int i = 0; i < Hs.ATARI_2600_MAX_PLAYERS; i++) {
            var name = names[i];
            Hs.Atari2600Difficulty diff;
            string diff_str;

            try {
                diff_str = metadata.get_string ("Atari 2600", @"$name Difficulty");
            } catch (KeyFileError e) {
                diff_str = "Advanced";
            }

            if (diff_str == "Advanced") {
                diff = ADVANCED;
            } else if (diff_str == "Beginner") {
                diff = BEGINNER;
            } else {
                warning ("Unsupported Atari 2600 difficulty: %s", diff_str);
                diff = ADVANCED;
            }

            if (i == 0)
                left_difficulty = diff;
            else
                right_difficulty = diff;
        }
    }

    public override Gtk.Widget? create_header_widget () {
        var menu = new SwitchMenu ();

        bind_property (
            "tv-type", menu, "tv-type", BIDIRECTIONAL | SYNC_CREATE
        );
        bind_property (
            "left-difficulty", menu, "left-difficulty", BIDIRECTIONAL | SYNC_CREATE
        );
        bind_property (
            "right-difficulty", menu, "right-difficulty", BIDIRECTIONAL | SYNC_CREATE
        );
        menu.select.connect (() => {
            var proxy = runner.platform_proxy as Proxy;
            proxy.flick_select.begin ();
        });
        menu.reset.connect (() => {
            var proxy = runner.platform_proxy as Proxy;
            proxy.flick_reset.begin ();
        });

        return menu;
    }

    public void paddle_turned (uint player, double value) {
        paddle_position[player] += value * 0.1;

        paddle_position[player] = paddle_position[player].clamp (-1, 1);

        runner.input_buffer.modify_input_state (state => {
            state->atari_2600.paddle_axis[player] = paddle_position[player];
        });
    }
}
