// This file is part of Highscore. License: GPL-3.0+.

[DBus (name = "org.gnome.World.Highscore2.Runner")]
public interface Highscore.Atari2600.Proxy : Object {
    public abstract async Hs.Atari2600Controller get_controller (uint player) throws Error;
    public abstract async Hs.Atari2600Difficulty get_default_difficulty (uint player) throws Error;
    public abstract async void flick_select () throws Error;
    public abstract async void flick_reset () throws Error;
}
