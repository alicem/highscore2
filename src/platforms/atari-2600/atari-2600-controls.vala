// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Atari2600.Controls : PlatformControls {
    construct {
        var controller = new PlatformController ("joystick", _("Joystick"));
        ControlHelpers.add_dpad (
            controller, set_pressed,
            "joystick", _("Joystick"),
            Hs.Atari2600JoystickButton.UP,
            Hs.Atari2600JoystickButton.DOWN,
            Hs.Atari2600JoystickButton.LEFT,
            Hs.Atari2600JoystickButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed,
            "fire",  _("Fire"),     Hs.Atari2600JoystickButton.FIRE,
            "fire5", _("2nd Fire"), Hs.Atari2600JoystickButton.FIRE_5,
            "fire9", _("3rd Fire"), Hs.Atari2600JoystickButton.FIRE_9,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,    "joystick:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,  "joystick:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "joystick:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "joystick:right");
        kbd_mapping.map (Linux.Input.KEY_S,     "fire");
        kbd_mapping.map (Linux.Input.KEY_D,     "fire5");
        kbd_mapping.map (Linux.Input.KEY_A,     "fire9");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (UP,    "joystick:up");
        gamepad_mapping.map_button (DOWN,  "joystick:down");
        gamepad_mapping.map_button (LEFT,  "joystick:left");
        gamepad_mapping.map_button (RIGHT, "joystick:right");
        gamepad_mapping.map_button (SOUTH, "fire");
        gamepad_mapping.map_button (EAST,  "fire5");
        gamepad_mapping.map_button (WEST,  "fire9");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);
        default_controller = "joystick";

        // paddle

        controller = new PlatformController ("paddle", _("Paddle"));

        var knob = new AnalogControl ("knob", _("Knob"), true, false);
        knob.moved.connect (paddle_moved_cb);
        controller.add_control (knob);

        var button = new ButtonControl ("fire", _("Fire"));
        button.activate.connect (paddle_fire_cb);
        controller.add_control (button);

        add_controller (controller);

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "knob:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "knob:right");
        kbd_mapping.map (Linux.Input.KEY_S,     "fire");
        controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_analog (LEFT,  "knob");
        gamepad_mapping.map_button (LEFT,  "knob:left");
        gamepad_mapping.map_button (RIGHT, "knob:right");
        gamepad_mapping.map_button (SOUTH, "fire");
        controller.default_gamepad_mapping = gamepad_mapping;

        // driving

        controller = new PlatformController ("driving", _("Driving"));

        knob = new AnalogControl ("knob", _("Knob"), true, false);
        knob.moved.connect (driving_moved_cb);
        controller.add_control (knob);

        button = new ButtonControl ("fire", _("Fire"));
        button.activate.connect (driving_fire_cb);
        controller.add_control (button);

        add_controller (controller);

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "knob:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "knob:right");
        kbd_mapping.map (Linux.Input.KEY_S,     "fire");
        controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_analog (LEFT,  "knob");
        gamepad_mapping.map_button (LEFT,  "knob:left");
        gamepad_mapping.map_button (RIGHT, "knob:right");
        gamepad_mapping.map_button (SOUTH, "fire");
        controller.default_gamepad_mapping = gamepad_mapping;

        // global

        global_controller = new PlatformController ("console", _("Built-in"));

        button = new ButtonControl ("tv-type", _("TV Type"));
        button.activate.connect (tv_type_cb);
        global_controller.add_control (button);

        button = new ButtonControl ("left-difficulty", _("Left Difficulty"));
        button.activate.connect (left_difficulty_cb);
        global_controller.add_control (button);

        button = new ButtonControl ("right-difficulty", _("Right Difficulty"));
        button.activate.connect (right_difficulty_cb);
        global_controller.add_control (button);

        button = new ButtonControl ("select", _("Select"));
        button.activate.connect (select_cb);
        global_controller.add_control (button);

        button = new ButtonControl ("reset", _("Reset"));
        button.activate.connect (reset_cb);
        global_controller.add_control (button);

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_1,         "tv-type");
        kbd_mapping.map (Linux.Input.KEY_2,         "left-difficulty");
        kbd_mapping.map (Linux.Input.KEY_3,         "right-difficulty");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "reset");
        global_controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (L2,     "tv-type");
        gamepad_mapping.map_button (L,      "left-difficulty");
        gamepad_mapping.map_button (R,      "right-difficulty");
        gamepad_mapping.map_button (SELECT, "select");
        gamepad_mapping.map_button (START,  "reset");
        global_controller.default_gamepad_mapping = gamepad_mapping;
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->atari_2600.joystick[player], button, pressed);
        });
    }

    private static void tv_type_cb (Runner runner, uint player, bool pressed) {
        if (pressed)
            return;

        var delegate = runner.delegate as RunnerDelegate;

        if (delegate.tv_type == COLOR)
            delegate.tv_type = BLACK_WHITE;
        else
            delegate.tv_type = COLOR;
    }

    private static void left_difficulty_cb (Runner runner, uint player, bool pressed) {
        if (pressed)
            return;

        var delegate = runner.delegate as RunnerDelegate;

        if (delegate.left_difficulty == ADVANCED)
            delegate.left_difficulty = BEGINNER;
        else
            delegate.left_difficulty = ADVANCED;
    }

    private static void right_difficulty_cb (Runner runner, uint player, bool pressed) {
        if (pressed)
            return;

        var delegate = runner.delegate as RunnerDelegate;

        if (delegate.right_difficulty == ADVANCED)
            delegate.right_difficulty = BEGINNER;
        else
            delegate.right_difficulty = ADVANCED;
    }

    private static void select_cb (Runner runner, uint player, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            state->atari_2600.select_switch = pressed;
        });
    }

    private static void reset_cb (Runner runner, uint player, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            state->atari_2600.reset_switch = pressed;
        });
    }

    private void paddle_moved_cb (Runner runner, uint player, double x, double y) {
        var delegate = runner.delegate as RunnerDelegate;

        delegate.paddle_turned (player, x);
    }

    private void paddle_fire_cb (Runner runner, uint player, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            state->atari_2600.paddle_fire[player] = pressed;
        });
    }

    private void driving_moved_cb (Runner runner, uint player, double x, double y) {
        runner.input_buffer.modify_input_state (state => {
            state->atari_2600.driving_axis[player] = x;
        });
    }

    private void driving_fire_cb (Runner runner, uint player, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            state->atari_2600.driving_fire[player] = pressed;
        });
    }
}
