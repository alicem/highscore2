// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.GameGear.Controls : PlatformControls {
    construct {
        var controller = new PlatformController ("gg", "");
        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("D-pad"),
            Hs.GameGearButton.UP,
            Hs.GameGearButton.DOWN,
            Hs.GameGearButton.LEFT,
            Hs.GameGearButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed,
            "one",    _("One"),   Hs.GameGearButton.ONE,
            "two",    _("Two"),   Hs.GameGearButton.TWO,
            "start",  _("Start"), Hs.GameGearButton.START,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,    "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,  "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_S,     "one");
        kbd_mapping.map (Linux.Input.KEY_D,     "two");
        kbd_mapping.map (Linux.Input.KEY_ENTER, "start");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (UP,    "dpad:up");
        gamepad_mapping.map_button (DOWN,  "dpad:down");
        gamepad_mapping.map_button (LEFT,  "dpad:left");
        gamepad_mapping.map_button (RIGHT, "dpad:right");
        gamepad_mapping.map_button (SOUTH, "one");
        gamepad_mapping.map_button (EAST,  "two");
        gamepad_mapping.map_button (START, "start");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);
        default_controller = "gg";
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->game_gear.buttons, button, pressed);
        });
    }
}
