// This file is part of Highscore. License: GPL-3.0+.

[DBus (name = "org.gnome.World.Highscore2.Runner")]
public interface Highscore.MasterSystem.Proxy : Object {
    public abstract async void set_enable_fm_audio (bool enable_fm_audio) throws Error;
    public abstract async void press_pause () throws Error;
}
