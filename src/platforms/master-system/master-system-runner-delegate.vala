// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.MasterSystem.RunnerDelegate : Highscore.RunnerDelegate {
    private bool enable_fm_audio;

    public override async void before_reset () throws Error {
        yield base.before_reset ();

        var settings = new GameSettings (runner.game).get_platform ();

        enable_fm_audio = settings.get_boolean ("fm-audio");

        var proxy = runner.platform_proxy as Proxy;
        yield proxy.set_enable_fm_audio (enable_fm_audio);
    }

    public override async void save_state (KeyFile metadata) throws Error {
        metadata.set_boolean ("Master System", "FM Audio", enable_fm_audio);
    }

    public override async void load_state (KeyFile metadata) throws Error {
        if (metadata.has_group ("Master System") && metadata.has_key ("Master System", "FM Audio"))
            enable_fm_audio = metadata.get_boolean ("Master System", "FM Audio");
        else
            enable_fm_audio = true;

        var proxy = runner.platform_proxy as Proxy;
        yield proxy.set_enable_fm_audio (enable_fm_audio);
    }

    public override Gtk.Widget? create_header_widget () {
        var pause_button = new Gtk.Button () {
            icon_name = "media-playback-pause-symbolic",
            tooltip_text = _("Pause"),
        };

        pause_button.clicked.connect (() => {
            var proxy = runner.platform_proxy as Proxy;
            proxy.press_pause.begin ();
        });

        return pause_button;
    }
}
