// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.NeoGeoPocket.Controls : PlatformControls {
    construct {
        var controller = new PlatformController ("ngp", "");
        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("D-pad"),
            Hs.NeoGeoPocketButton.UP,
            Hs.NeoGeoPocketButton.DOWN,
            Hs.NeoGeoPocketButton.LEFT,
            Hs.NeoGeoPocketButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed,
            "a",      _("A"),      Hs.NeoGeoPocketButton.A,
            "b",      _("B"),      Hs.NeoGeoPocketButton.B,
            "option", _("Option"), Hs.NeoGeoPocketButton.OPTION,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,    "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,  "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,  "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT, "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_S,     "a");
        kbd_mapping.map (Linux.Input.KEY_D,     "b");
        kbd_mapping.map (Linux.Input.KEY_ENTER, "option");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (UP,    "dpad:up");
        gamepad_mapping.map_button (DOWN,  "dpad:down");
        gamepad_mapping.map_button (LEFT,  "dpad:left");
        gamepad_mapping.map_button (RIGHT, "dpad:right");
        gamepad_mapping.map_button (SOUTH, "a");
        gamepad_mapping.map_button (EAST,  "b");
        gamepad_mapping.map_button (START, "option");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);
        default_controller = "ngp";
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->neo_geo_pocket.buttons, button, pressed);
        });
    }
}
