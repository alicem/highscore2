// This file is part of Highscore. License: GPL-3.0+.

namespace Highscore.GameBoy.Palette {
    private static Regex? sgb_regex = null;
    private static Regex? gbc_regex = null;

    private struct SimplePaletteInfo {
        int colors[4];
        string id;
        string name;
    }

    // Values copied from mGBA
    private const SimplePaletteInfo[] SIMPLE_PALETTES = {
        {{ 0xF8F8F8, 0xA8A8A8, 0x505050, 0x000000 }, "grayscale", N_("Grayscale") },
        {{ 0x88A048, 0x486830, 0x284020, 0x182808 }, "dmg",       N_("Game Boy") },
        {{ 0xA0B0A0, 0x809880, 0x486050, 0x182820 }, "pocket",    N_("Game Boy Pocket") },
        {{ 0x78F0F8, 0x58E0E0, 0x18B0A8, 0x007870 }, "light",     N_("Game Boy Light") },
    };

    public enum SimplePalette {
        GRAYSCALE, GAME_BOY, GAME_BOY_POCKET, GAME_BOY_LIGHT;

        public int[] get_colors () {
            return SIMPLE_PALETTES[this].colors;
        }

        public string get_id () {
            return SIMPLE_PALETTES[this].id;
        }

        public string get_name () {
            return SIMPLE_PALETTES[this].name;
        }

        public static SimplePalette[] all () {
            return {
                GRAYSCALE, GAME_BOY, GAME_BOY_POCKET, GAME_BOY_LIGHT,
            };
        }

        public static SimplePalette? parse (string str) {
            for (int i = 0; i < SIMPLE_PALETTES.length; i++) {
                var palette = SIMPLE_PALETTES[i];

                if (str == palette.id)
                    return i;
            }

            return null;
        }
    }

    // Extracted from Super Game Boy screenshots
    private const int[] SGB_PALETTES = {
        0xF8E8C8, 0xD89048, 0xA82820, 0x301850, // 1-A
        0xD8D8C0, 0xC8B070, 0xB05010, 0x000000, // 1-B
        0xF8C0F8, 0xE89850, 0x983860, 0x383898, // 1-C
        0xF8F8A8, 0xC08048, 0xF80000, 0x501800, // 1-D
        0xF8D8B0, 0x78C078, 0x688840, 0x583820, // 1-E
        0xD8E8F8, 0xE08850, 0xA80000, 0x004010, // 1-F
        0x000050, 0x00A0E8, 0x787800, 0xF8F858, // 1-G
        0xF8E8E0, 0xF8B888, 0x804000, 0x301800, // 1-H

        0xF0C8A0, 0xC08848, 0x287800, 0x000000, // 2-A
        0xF8F8F8, 0xF8E850, 0xF83000, 0x500058, // 2-B
        0xF8C0F8, 0xE88888, 0x7830E8, 0x282898, // 2-C
        0xF8F8A0, 0x00F800, 0xF83000, 0x000050, // 2-D
        0xF8C880, 0x90B0E0, 0x281060, 0x100810, // 2-E
        0xD0F8F8, 0xF89050, 0xA00000, 0x180000, // 2-F
        0x68B838, 0xE05040, 0xE0B880, 0x001800, // 2-G
        0xF8F8F8, 0xB8B8B8, 0x707070, 0x000000, // 2-H

        0xF8D098, 0x70C0C0, 0xF86028, 0x304860, // 3-A
        0xD8D8C0, 0xE08020, 0x005000, 0x001010, // 3-B
        0xE0A8C8, 0xF8F878, 0x00B8F8, 0x202058, // 3-C
        0xF0F8B8, 0xE0A878, 0x08C800, 0x000000, // 3-D
        0xF8F8C0, 0xE0B068, 0xB07820, 0x504870, // 3-E
        0x7878C8, 0xF868F8, 0xF8D000, 0x404040, // 3-F
        0x60D850, 0xF8F8F8, 0xC83038, 0x380000, // 3-G
        0xE0F8A0, 0x78C838, 0x488818, 0x081800, // 3-H

        0xF0A868, 0x78A8F8, 0xD000D0, 0x000078, // 4-A
        0xF0E8F0, 0xE8A060, 0x407838, 0x180808, // 4-B
        0xF8E0E0, 0xD8A0D0, 0x98A0E0, 0x080000, // 4-C
        0xF8F8B8, 0x90C8C8, 0x486878, 0x082048, // 4-D
        0xF8D8A8, 0xE0A878, 0x785888, 0x002030, // 4-E
        0xB8D0D0, 0xD880D8, 0x8000A0, 0x380000, // 4-F
        0xB0E018, 0xB82058, 0x281000, 0x008060, // 4-G
        0xF8F8C8, 0xB8C058, 0x808840, 0x405028, // 4-H
    };

    public enum SgbPalette {
        1_A, 1_B, 1_C, 1_D, 1_E, 1_F, 1_G, 1_H,
        2_A, 2_B, 2_C, 2_D, 2_E, 2_F, 2_G, 2_H,
        3_A, 3_B, 3_C, 3_D, 3_E, 3_F, 3_G, 3_H,
        4_A, 4_B, 4_C, 4_D, 4_E, 4_F, 4_G, 4_H;

        public int[] get_colors () {
            return {
                SGB_PALETTES[(int) this * 4],
                SGB_PALETTES[(int) this * 4 + 1],
                SGB_PALETTES[(int) this * 4 + 2],
                SGB_PALETTES[(int) this * 4 + 3]
            };
        }

        public string get_id () {
            var name = get_name ();

            return @"sgb($name)";
        }

        public string get_name () {
            char column = 'A' + (char) this % 8;
            int row = this / 8 + 1;

            return @"$row-$column";
        }

        public static SgbPalette[] all () {
            return {
                1_A, 1_B, 1_C, 1_D, 1_E, 1_F, 1_G, 1_H,
                2_A, 2_B, 2_C, 2_D, 2_E, 2_F, 2_G, 2_H,
                3_A, 3_B, 3_C, 3_D, 3_E, 3_F, 3_G, 3_H,
                4_A, 4_B, 4_C, 4_D, 4_E, 4_F, 4_G, 4_H,
            };
        }

        public static SgbPalette? parse (string str) {
            if (str == "sgb-default")
                return 1_A;

            if (sgb_regex == null) {
                try {
                    sgb_regex = new Regex ("sgb\\(([1-8])-([A-H])\\)");
                } catch (Error e) {
                    error ("Invalid regex: %s", e.message);
                }
            }

            MatchInfo info;
            if (sgb_regex.match (str, 0, out info)) {
                int row = info.fetch (1)[0] - '1';
                int column = info.fetch (2)[0] - 'A';

                return row * 8 + column;
            }

            return null;
        }
    }

    public const SgbPalette DEFAULT_SGB_PALETTE = 1_A;

    // Documentation: https://tcrf.net/Notes:Game_Boy_Color_Bootstrap_ROM
    private const int[] GBC_PALETTES = {
        // 0x00
        0xFFFFFF, 0xADAD84, 0x42737B, 0x000000, // BG
        0xFFFFFF, 0xFF7300, 0x944200, 0x000000, // OBJ0
        0xFFFFFF, 0x5ABDFF, 0xFF0000, 0x0000FF, // OBJ1
        // 0x01
        0xFFFF9C, 0x94B5FF, 0x639473, 0x003A3A, // BG
        0xFFC542, 0xFFD600, 0x943A00, 0x4A0000, // OBJ0
        0xFFFFFF, 0xFF8484, 0x943A3A, 0x000000, // OBJ1
        // 0x02
        0x6BFF00, 0xFFFFFF, 0xFF524A, 0x000000, // BG
        0xFFFFFF, 0xFFFFFF, 0x63A5FF, 0x0000FF, // OBJ0
        0xFFFFFF, 0xFFAD63, 0x843100, 0x000000, // OBJ1
        // 0x03
        0x52DE00, 0xFF8400, 0xFFFF00, 0xFFFFFF, // BG
        0xFFFFFF, 0xFFFFFF, 0x63A5FF, 0x0000FF, // OBJ0
        0xFFFFFF, 0xFF8484, 0x943A3A, 0x000000, // OBJ1
        // 0x04
        0xFFFFFF, 0x7BFF00, 0xB57300, 0x000000, // BG
        0xFFFFFF, 0xFF8484, 0x943A3A, 0x000000, // OBJ0
        0xFFFFFF, 0xFF8484, 0x943A3A, 0x000000, // OBJ1, unused
        // 0x05
        0xFFFFFF, 0x52FF00, 0xFF4200, 0x000000, // BG
        0xFFFFFF, 0xFF8484, 0x943A3A, 0x000000, // OBJ0
        0xFFFFFF, 0x5ABDFF, 0xFF0000, 0x0000FF, // OBJ1
        // 0x06
        0xFFFFFF, 0xFF9C00, 0xFF0000, 0x000000, // BG
        0xFFFFFF, 0xFF8484, 0x943A3A, 0x000000, // OBJ0
        0xFFFFFF, 0x5ABDFF, 0xFF0000, 0x0000FF, // OBJ1
        // 0x07
        0xFFFFFF, 0xFFFF00, 0xFF0000, 0x000000, // BG
        0xFFFFFF, 0x63A5FF, 0x0000FF, 0x000000, // OBJ0, unused
        0xFFFFFF, 0x5ABDFF, 0xFF0000, 0x0000FF, // OBJ1
        // 0x08
        0xA59CFF, 0xFFFF00, 0x006300, 0x000000, // BG
        0xFF6352, 0xD60000, 0x630000, 0x000000, // OBJ0
        0x0000FF, 0xFFFFFF, 0xFFFF7B, 0x0084FF, // OBJ1
        // 0x09
        0xFFFFCE, 0x63EFEF, 0x9C8431, 0x5A5A5A, // BG
        0xFFFFFF, 0xFF7300, 0x944200, 0x000000, // OBJ0
        0xFFFFFF, 0x63A5FF, 0x0000FF, 0x000000, // OBJ1
        // 0x0A
        0xB5B5FF, 0xFFFF94, 0xAD5A42, 0x000000, // BG
        0x000000, 0xFFFFFF, 0xFF8484, 0x943A3A, // OBJ0
        0x000000, 0xFFFFFF, 0xFF8484, 0x943A3A, // OBJ1, unused
        // 0x0B
        0xFFFFFF, 0x63A5FF, 0x0000FF, 0x000000, // BG
        0xFFFFFF, 0xFF8484, 0x943A3A, 0x000000, // OBJ0
        0xFFFFFF, 0xFFFF7B, 0x0084FF, 0xFF0000, // OBJ1
        // 0x0C
        0xFFFFFF, 0x8C8CDE, 0x52528C, 0x000000, // BG
        0xFFC542, 0xFFD600, 0x943A00, 0x4A0000, // OBJ0
        0xFFFFFF, 0x5ABDFF, 0xFF0000, 0x0000FF, // OBJ1
        // 0x0D
        0xFFFFFF, 0x8C8CDE, 0x52528C, 0x000000, // BG
        0xFFFFFF, 0xFF8484, 0x943A3A, 0x000000, // OBJ0
        0xFFFFFF, 0xFFAD63, 0x843100, 0x000000, // OBJ1
        // 0x0E
        0xFFFFFF, 0x7BFF31, 0x008400, 0x000000, // BG
        0xFFFFFF, 0xFF8484, 0x943A3A, 0x000000, // OBJ0
        0xFFFFFF, 0x63A5FF, 0x0000FF, 0x000000, // OBJ1
        // 0x0F
        0xFFFFFF, 0xFFAD63, 0x843100, 0x000000, // BG
        0xFFFFFF, 0x63A5FF, 0x0000FF, 0x000000, // OBJ0
        0xFFFFFF, 0x7BFF31, 0x008400, 0x000000, // OBJ1
        // 0x10
        0xFFFFFF, 0xFF8484, 0x943A3A, 0x000000, // BG
        0xFFFFFF, 0x7BFF31, 0x008400, 0x000000, // OBJ0
        0xFFFFFF, 0x63A5FF, 0x0000FF, 0x000000, // OBJ1
        // 0x11
        0xFFFFFF, 0xFF8484, 0x943A3A, 0x000000, // BG
        0xFFFFFF, 0x00FF00, 0x318400, 0x004A00, // OBJ0
        0xFFFFFF, 0x63A5FF, 0x0000FF, 0x000000, // OBJ1
        // 0x12
        0xFFFFFF, 0xFFAD63, 0x843100, 0x000000, // BG
        0xFFFFFF, 0x7BFF31, 0x008400, 0x000000, // OBJ0
        0xFFFFFF, 0x63A5FF, 0x0000FF, 0x000000, // OBJ1
        // 0x13
        0x000000, 0x008484, 0xFFDE00, 0xFFFFFF, // BG
        0xFFFFFF, 0xFF8484, 0x943A3A, 0x000000, // OBJ0, unused
        0xFFFFFF, 0x7BFF31, 0x008400, 0x000000, // OBJ1, unused
        // 0x14
        0xFFFFFF, 0x63A5FF, 0x0000FF, 0x000000, // BG
        0xFFFF00, 0xFF0000, 0x630000, 0x000000, // OBJ0
        0xFFFFFF, 0x7BFF31, 0x008400, 0x000000, // OBJ1
        // 0x15
        0xFFFFFF, 0xADAD84, 0x42737B, 0x000000, // BG
        0xFFFFFF, 0xFFAD63, 0x843100, 0x000000, // OBJ0
        0xFFFFFF, 0x63A5FF, 0x0000FF, 0x000000, // OBJ1
        // 0x16
        0xFFFFFF, 0xA5A5A5, 0x525252, 0x000000, // BG
        0xFFFFFF, 0xA5A5A5, 0x525252, 0x000000, // OBJ0, unused
        0xFFFFFF, 0xA5A5A5, 0x525252, 0x000000, // OBJ1, unused
        // 0x17
        0xFFFFA5, 0xFF9494, 0x9494FF, 0x000000, // BG
        0xFFFFA5, 0xFF9494, 0x9494FF, 0x000000, // OBJ0
        0xFFFFA5, 0xFF9494, 0x9494FF, 0x000000, // OBJ1
        // 0x18
        0xFFFFFF, 0x63A5FF, 0x0000FF, 0x000000, // BG
        0xFFFFFF, 0xFF8484, 0x943A3A, 0x000000, // OBJ0
        0xFFFFFF, 0x7BFF31, 0x008400, 0x000000, // OBJ1
        // 0x19
        0xFFE6C5, 0xCE9C84, 0x846B29, 0x5A3108, // BG
        0xFFFFFF, 0xFFAD63, 0x843100, 0x000000, // OBJ0
        0xFFFFFF, 0xFFAD63, 0x843100, 0x000000, // OBJ1
        // 0x1A
        0xFFFFFF, 0xFFFF00, 0x7B4A00, 0x000000, // BG
        0xFFFFFF, 0x63A5FF, 0x0000FF, 0x000000, // OBJ0
        0xFFFFFF, 0x7BFF31, 0x008400, 0x000000, // OBJ1
        // 0x1B
        0xFFFFFF, 0xFFCE00, 0x9C6300, 0x000000, // BG
        0xFFFFFF, 0xFFCE00, 0x9C6300, 0x000000, // OBJ0, unused
        0xFFFFFF, 0xFFCE00, 0x9C6300, 0x000000, // OBJ1, unused
        // 0x1C
        0xFFFFFF, 0x7BFF31, 0x0063C5, 0x000000, // BG
        0xFFFFFF, 0xFF8484, 0x943A3A, 0x000000, // OBJ0
        0xFFFFFF, 0x63A5FF, 0x0000FF, 0x000000, // OBJ1
    };

    public const uint8 MAX_GBC_ENTRY = 0x1C;
    public const uint8 MAX_GBC_FLAGS = 5;

    public struct GbcPalette {
        uint8 entry;
        uint8 shuffle_flags;

        public int[] get_colors () {
            if (entry > MAX_GBC_ENTRY || shuffle_flags > MAX_GBC_FLAGS) {
                critical ("Invalid GBC palette: %u / %u", entry, shuffle_flags);
                return SimplePalette.GRAYSCALE.get_colors ();
            }

            int entry_start = entry * 12;

            if (shuffle_flags == 0) {
                return {
                    GBC_PALETTES[entry_start],
                    GBC_PALETTES[entry_start + 1],
                    GBC_PALETTES[entry_start + 2],
                    GBC_PALETTES[entry_start + 3],
                };
            }

            // Always use the first row for background,
            // and vary the rest depending on the shuffle flags
            int bg_index = entry_start;
            int obj0_index = entry_start + (shuffle_flags &  1) * 4;
            int obj1_index = entry_start + (shuffle_flags >> 1) * 4;

            return {
                GBC_PALETTES[bg_index],
                GBC_PALETTES[bg_index + 1],
                GBC_PALETTES[bg_index + 2],
                GBC_PALETTES[bg_index + 3],

                GBC_PALETTES[obj0_index],
                GBC_PALETTES[obj0_index + 1],
                GBC_PALETTES[obj0_index + 2],
                GBC_PALETTES[obj0_index + 3],

                GBC_PALETTES[obj1_index],
                GBC_PALETTES[obj1_index + 1],
                GBC_PALETTES[obj1_index + 2],
                GBC_PALETTES[obj1_index + 3],
            };
        }
    }

    public enum ManualGbcPalette {
        UP,   LEFT,   DOWN,   RIGHT,
        UP_A, LEFT_A, DOWN_A, RIGHT_A,
        UP_B, LEFT_B, DOWN_B, RIGHT_B;

        public GbcPalette get_palette () {
            return MANUAL_GBC_PALETTE_INFO[this].palette;
        }

        public int[] get_colors () {
            return get_palette ().get_colors ();
        }

        public string get_id () {
            string[] directions = { "up", "left", "down", "right" };
            string[] buttons = { "", "-a", "-b" };

            return "gbc(%s%s)".printf (directions[this % 4], buttons[this / 4]);
        }

        public string get_name () {
            string[] directions = { _("Up"), _("Left"), _("Down"), _("Right") };
            string[] buttons = { "", "+A", "+B" };

            return "%s%s".printf (directions[this % 4], buttons[this / 4]);
        }

        public int get_preview_bg_color () {
            return MANUAL_GBC_PALETTE_INFO[this].preview_bg_color;
        }

        public int get_preview_fg_color () {
            return MANUAL_GBC_PALETTE_INFO[this].preview_fg_color;
        }

        public static ManualGbcPalette[] all () {
            return {
                UP,   LEFT,   DOWN,   RIGHT,
                UP_A, LEFT_A, DOWN_A, RIGHT_A,
                UP_B, LEFT_B, DOWN_B, RIGHT_B
            };
        }

        public static ManualGbcPalette? parse (string str) {
            if (str == "gbc-default")
                return RIGHT_A;

            if (gbc_regex == null) {
                try {
                    gbc_regex = new Regex ("gbc\\((up|left|down|right)(-[ab])?\\)");
                } catch (Error e) {
                    error ("Invalid regex: %s", e.message);
                }
            }

            MatchInfo info;
            if (gbc_regex.match (str, 0, out info)) {
                string dir = info.fetch (1);
                string mod = info.fetch (2);

                int dir_index;
                if (dir == "up")
                    dir_index = 0;
                else if (dir == "left")
                    dir_index = 1;
                else if (dir == "down")
                    dir_index = 2;
                else if (dir == "right")
                    dir_index = 3;
                else
                    assert_not_reached ();

                int mod_index;
                if (mod == "")
                    mod_index = 0;
                else if (mod == "-a")
                    mod_index = 1;
                else if (mod == "-b")
                    mod_index = 2;
                else
                    assert_not_reached ();

                return mod_index * 4 + dir_index;
            }

            return null;
        }
    }

    private struct ManualGbcPaletteInfo {
        GbcPalette palette;
        int preview_bg_color;
        int preview_fg_color;
    }

    // Documentation: https://tcrf.net/Notes:Game_Boy_Color_Bootstrap_ROM
    private const ManualGbcPaletteInfo[] MANUAL_GBC_PALETTE_INFO = {
        {{ 0x12, 0 }, 0xF7C5A5, 0x843100 }, // UP
        {{ 0x18, 5 }, 0x63A5FF, 0x0000FF }, // LEFT
        {{ 0x17, 0 }, 0xFFFFA5, 0xFF00FF }, // DOWN
        {{ 0x05, 0 }, 0x7BFF31, 0xFF0000 }, // RIGHT
        {{ 0x10, 5 }, 0xFF8484, 0xE60000 }, // UP_A
        {{ 0x0D, 5 }, 0x8C8CDE, 0x52528C }, // LEFT_A
        {{ 0x07, 0 }, 0xFFFF00, 0xFF0000 }, // DOWN_A
        {{ 0x1C, 3 }, 0x7BFF31, 0x0000FF }, // RIGHT_A
        {{ 0x19, 3 }, 0xA58452, 0x6B5231 }, // UP_B
        {{ 0x16, 0 }, 0x949494, 0x000000 }, // LEFT_B
        {{ 0x1A, 5 }, 0xFFFF3A, 0x3A2900 }, // DOWN_B
        {{ 0x13, 0 }, 0x000000, 0xFFFF00 }, // RIGHT_B
    };

    public const ManualGbcPalette DEFAULT_GBC_PALETTE = RIGHT_A;

    public bool parse (
        string str,
        out int[]? colors = null,
        out SimplePalette? simple = null,
        out ManualGbcPalette? gbc = null,
        out SgbPalette? sgb = null
    ) {
        simple = SimplePalette.parse (str);
        gbc = null;
        sgb = null;

        if (simple != null) {
            colors = simple.get_colors ();
            return true;
        }

        gbc = ManualGbcPalette.parse (str);

        if (gbc != null) {
            colors = gbc.get_colors ();
            return true;
        }

        sgb = SgbPalette.parse (str);

        if (sgb != null) {
            colors = sgb.get_colors ();
            return true;
        }

        colors = SimplePalette.GRAYSCALE.get_colors ();
        return false;
    }
}
