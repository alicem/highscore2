#ifdef VERTEX

void hs_main() {
}

#else // FRAGMENT

uniform bool u_recolor;
uniform vec3 u_palette[12];

vec4 hs_main() {
  if (!u_recolor)
    return hs_texture(uv);

  vec3 rgb = hs_texture(uv).rgb;

  int r = int(rgb.r * 255.0);
  int g = int(rgb.g * 255.0);
  int b = int(rgb.b * 255.0);

  int layer, brightness;

  if (b > 0)
    layer = 0;
  else if (r > 0)
    layer = 1;
  else if (g > 0)
    layer = 2;

  if (layer == 2)
    brightness = g;
  else
    brightness = r;

  if (brightness == 0xFF)
    return vec4(u_palette[layer * 4], 1);

  if (brightness == 0xC6)
    return vec4(u_palette[layer * 4 + 1], 1);

  if (brightness == 0x84)
    return vec4(u_palette[layer * 4 + 2], 1);

  if (brightness == 0x42)
    return vec4(u_palette[layer * 4 + 3], 1);

  return vec4(1, 0, 1, 1);
}

#endif