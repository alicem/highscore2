// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.GameBoy.ScreenSet : Highscore.ScreenSet {
    public const int[] SOURCE_PALETTE = {
        0xFFFFFF, 0xC6C6C6, 0x848484, 0x424242,
        0xFF0000, 0xC60000, 0x840000, 0x420000,
        0x00FF00, 0x00C600, 0x008400, 0x004200,
    };

    public const int SMALL_SCREEN = 1;

    public const int SGB_SCREEN_WIDTH = 256;
    public const int SGB_SCREEN_HEIGHT = 224;
    public const int SGB_BORDER_WIDTH = 48;
    public const int SGB_BORDER_HEIGHT = 40;

    public string palette { get; set; }
    public Hs.GameBoyModel model { get; set; }
    public bool sgb_border { get; set; }

    private Settings settings;
    private Graphene.Vec3 shader_colors[12];

    construct {
        settings = new GameSettings (game).get_platform ();

        notify["palette"].connect (() => {
            var metadata = game.metadata as Metadata;
            var colors = parse_palette_with_overrides (metadata, palette);

            for (int i = 0; i < colors.length; i++) {
                var vec = ColorUtils.rgb_to_vec3 (colors[i]);

                shader_colors[i] = vec;

                if (colors.length == 4) {
                    shader_colors[i + 4] = vec;
                    shader_colors[i + 8] = vec;
                }
            }

            filter_changed ();
        });

        notify["model"].connect (() => {
            update_layout ();
            filter_changed ();
        });

        notify["sgb-border"].connect (() => {
            update_layout ();
        });

        settings.bind ("palette", this, "palette", GET);
        settings.bind ("sgb-border", this, "sgb-border", GET);

        if (snapshot != null) {
            try {
                var metadata = snapshot.get_metadata ();
                var model_str = metadata.get_string ("Game Boy", "Model");

                model = Model.from_string (model_str);
            } catch (Error e) {
                critical ("Failed to parse GB model from snapshot");
                model = GAME_BOY;
            }

            if (runner != null) {
                var delegate = runner.delegate as RunnerDelegate;

                delegate.bind_property ("model", this, "model", DEFAULT);
            }
        } else if (runner != null) {
            var delegate = runner.delegate as RunnerDelegate;

            delegate.bind_property ("model", this, "model", SYNC_CREATE);
        }
    }

    private void update_layout () {
        bool is_sgb = model == SUPER_GAME_BOY || model == SUPER_GAME_BOY_2;

        layout = new SingleLayout (
            is_sgb && !sgb_border ? SMALL_SCREEN : DEFAULT_SCREEN
        );

        layout_changed (false);
    }

    public override Screen[] get_screens () {
        float small_x = (float) SGB_BORDER_WIDTH / SGB_SCREEN_WIDTH;
        float small_y = (float) SGB_BORDER_HEIGHT / SGB_SCREEN_HEIGHT;
        float small_w = (float) (SGB_SCREEN_WIDTH - SGB_BORDER_WIDTH * 2) / SGB_SCREEN_WIDTH;
        float small_h = (float) (SGB_SCREEN_HEIGHT - SGB_BORDER_HEIGHT * 2) / SGB_SCREEN_HEIGHT;

        var whole_screen  = new Screen (DEFAULT_SCREEN, {{ 0, 0 }, { 1, 1 }});
        var small_screen = new Screen (SMALL_SCREEN, {
            { small_x, small_y }, { small_w, small_h }
        });

        return ({ whole_screen, small_screen });
    }

    public override string? get_filter_path () {
        return "/org/gnome/World/Highscore2/platforms/game-boy/game-boy.glsl";
    }

    public override void setup_filter (GLShader shader, int screen_id) {
        bool has_palettes = model == GAME_BOY || model == GAME_BOY_POCKET;

        shader.set_uniform_bool ("u_recolor", has_palettes);
        shader.set_uniform_vec3_array ("u_palette", shader_colors);
    }

    private static int[] parse_palette_with_overrides (Metadata metadata, string str) {
        if (str == "gbc-special") {
            var palette = metadata.get_special_gbc_palette ();

            if (palette == null)
                palette = Palette.DEFAULT_GBC_PALETTE.get_palette ();

            return palette.get_colors ();
        }

        if (str == "sgb-special") {
            var palette = metadata.get_special_sgb_palette ();

            if (palette == null)
                palette = Palette.DEFAULT_SGB_PALETTE;

            return palette.get_colors ();
        }

        int[] colors;
        Palette.parse (str, out colors);

        return colors;
    }
}
