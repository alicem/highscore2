// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.GameBoy.Metadata : Object, GameMetadata {
    public string title { get; set; }
    public bool supports_sgb { get; set; }
    public bool supports_gbc { get; set; }
    public bool requires_gbc { get; set; }
    public bool is_nintendo { get; set; }

    public Metadata (
        string title,
        bool supports_sgb,
        bool supports_gbc,
        bool requires_gbc,
        bool is_nintendo
    ) {
        Object (
            title: title,
            supports_sgb: supports_sgb,
            supports_gbc: supports_gbc,
            requires_gbc: requires_gbc,
            is_nintendo: is_nintendo
        );
    }

    public Palette.GbcPalette? get_special_gbc_palette () {
        if (!is_nintendo)
            return null;

        if (requires_gbc)
            return null;

        return Overrides.find_gbc_palette (title);
    }

    public Palette.SgbPalette? get_special_sgb_palette () {
        if (!is_nintendo)
            return null;

        return Overrides.find_sgb_palette (title);
    }

    public bool is_gba_enhanced () {
        if (!supports_gbc)
            return false;

        return Overrides.is_gba_enhanced (title);
    }

    public bool is_sgb2_enhanced () {
        if (!supports_sgb)
            return false;

        return Overrides.is_sgb2_enhanced (title);
    }

    protected VariantType serialize_type () {
        return new VariantType.tuple ({
            VariantType.BYTESTRING,
            VariantType.BOOLEAN,
            VariantType.BOOLEAN,
            VariantType.BOOLEAN,
            VariantType.BOOLEAN
        });
    }

    protected Variant serialize () {
        return new Variant.tuple ({
            new Variant.bytestring (title),
            supports_sgb,
            supports_gbc,
            requires_gbc,
            is_nintendo,
        });
    }

    protected void deserialize (Variant variant) {
        title = variant.get_child_value (0).get_bytestring ();
        supports_sgb = variant.get_child_value (1).get_boolean ();
        supports_gbc = variant.get_child_value (2).get_boolean ();
        requires_gbc = variant.get_child_value (3).get_boolean ();
        is_nintendo = variant.get_child_value (4).get_boolean ();
    }

    public Hs.GameBoyModel[] get_supported_models () {
        Hs.GameBoyModel[] models = {};

        if (!requires_gbc)
            models += Hs.GameBoyModel.GAME_BOY;

        if (supports_gbc) {
            models += Hs.GameBoyModel.GAME_BOY_COLOR;

            if (is_gba_enhanced ())
                models += Hs.GameBoyModel.GAME_BOY_ADVANCE;
        }

        if (supports_sgb) {
            models += Hs.GameBoyModel.SUPER_GAME_BOY;

            if (is_sgb2_enhanced ()) {
                models += Hs.GameBoyModel.SUPER_GAME_BOY_2;
            }
        }

        return models;
    }
}
