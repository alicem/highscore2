// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.GameBoy.RunnerDelegate : Highscore.RunnerDelegate {
    public Hs.GameBoyModel model { get; private set; }
    public string palette { get; set; }
    public bool has_palettes { get; private set; }

    private Settings settings;

    construct {
        notify["has-palettes"].connect (() => {
            if (has_palettes)
                settings.bind ("palette", this, "palette", DEFAULT);
            else
                Settings.unbind (this, "palette");
        });

        settings = new GameSettings (runner.game).get_platform ();

        if (settings.get_user_value ("model") == null) {
            var model = Model.get_auto_for_game (runner.game);
            settings.set_string ("model", Model.to_string (model));
        }
    }

    public override async void before_reset () throws Error {
        var proxy = runner.platform_proxy as Proxy;

        var settings = new GameSettings (runner.game).get_platform ();
        bool model_set = settings.get_user_value ("model") != null;

        if (model_set) {
            var model_str = settings.get_string ("model");
            model = Model.from_string (model_str);
        } else {
            model = Model.get_auto_for_game (runner.game);
        }

        yield update_palette ();
        yield proxy.set_model (model);
    }

    public override async void save_state (KeyFile metadata) throws Error {
        metadata.set_string ("Game Boy", "Model", Model.to_string (model));
    }

    public override async void load_state (KeyFile metadata) throws Error {
        var proxy = runner.platform_proxy as Proxy;
        var model_str = metadata.get_string ("Game Boy", "Model");

        model = Model.from_string (model_str);

        yield update_palette ();
        yield proxy.set_model (model);
    }

    public override Gtk.Widget? create_header_widget () {
        var metadata = runner.game.metadata as Metadata;
        var switcher = new PaletteSwitcher (metadata);

        bind_property ("has-palettes", switcher, "visible", SYNC_CREATE);
        bind_property ("palette", switcher, "palette", SYNC_CREATE | BIDIRECTIONAL);

        return switcher;
    }

    private async void update_palette () throws Error {
        var proxy = runner.platform_proxy as Proxy;
        var metadata = runner.game.metadata as Metadata;

        switch (model) {
            case GAME_BOY:
            case GAME_BOY_POCKET:
                var gbc_palette = metadata.get_special_gbc_palette ();
                var sgb_palette = metadata.get_special_sgb_palette ();

                if (settings.get_user_value ("palette") == null) {
                    if (gbc_palette != null)
                        settings.set_string ("palette", "gbc-special");
                    else if (sgb_palette != null)
                        settings.set_string ("palette", "sgb-special");
                    else
                        settings.set_string ("palette", "grayscale");
                }

                has_palettes = true;

                proxy.set_palette.begin (ScreenSet.SOURCE_PALETTE);

                break;
            case GAME_BOY_COLOR:
            case GAME_BOY_ADVANCE:
                yield proxy.set_palette (
                    Palette.DEFAULT_GBC_PALETTE.get_colors ()
                );

                has_palettes = false;

                break;
            case SUPER_GAME_BOY:
            case SUPER_GAME_BOY_2:
                var sgb_palette = metadata.get_special_sgb_palette ();
                if (sgb_palette == null)
                    sgb_palette = Palette.DEFAULT_SGB_PALETTE;

                yield proxy.set_palette (sgb_palette.get_colors ());

                has_palettes = false;

                break;
            default:
                assert_not_reached ();
        }
    }
}
