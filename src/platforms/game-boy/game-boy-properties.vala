// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore2/platforms/game-boy/game-boy-properties.ui")]
public class Highscore.GameBoy.Properties : PlatformProperties {
    [GtkChild]
    private unowned Adw.PreferencesGroup model_group;
    [GtkChild]
    private unowned Adw.PreferencesGroup sgb_group;

    private Settings settings;

    public Hs.GameBoyModel model { get; set; }
    public bool sgb_border { get; set; }

    construct {
        settings = new GameSettings (game).get_platform ();

        if (settings.get_user_value ("model") == null) {
            var model = Model.get_auto_for_game (game);
            settings.set_string ("model", Model.to_string (model));
        }

        setup_model_group ();

        notify["model"].connect (() => {
            sgb_group.visible = model == SUPER_GAME_BOY || model == SUPER_GAME_BOY_2;

            dialog.reset_height ();
        });

        settings.bind ("model", this, "model", DEFAULT);
        settings.bind ("sgb-border", this, "sgb-border", DEFAULT);
    }

    static construct {
        install_property_action ("gb.model", "model");
    }

    protected override void map () {
        base.map ();

        dialog.reset_height ();
    }

    private void setup_model_group () {
        var metadata = game.metadata as Metadata;
        var models = metadata.get_supported_models ();

        if (models.length > 1) {
            foreach (var m in models) {
                var radio = new Gtk.CheckButton () {
                    valign = CENTER,
                    action_name = "gb.model",
                    action_target = new Variant.string (Model.to_string (m)),
                };

                var row = new Adw.ActionRow () {
                    title = Model.to_display_name (m),
                    activatable_widget = radio,
                };

                row.add_prefix (radio);

                model_group.add (row);
            }
        } else {
            visible = false;
        }
    }
}
