// This file is part of Highscore. License: GPL-3.0+.

[DBus (name = "org.gnome.World.Highscore2.Runner")]
public interface Highscore.GameBoy.Proxy : Object {
    public abstract async void set_model (Hs.GameBoyModel model) throws Error;

    public abstract async void set_palette (int[] colors) throws Error;

    public abstract async void set_show_sgb_borders (bool show_borders) throws Error;
}
