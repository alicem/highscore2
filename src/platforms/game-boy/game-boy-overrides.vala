// This file is part of Highscore. License: GPL-3.0+.

namespace Highscore.GameBoy.Overrides {
    private struct GbcColorOverride {
        string title;
        Palette.GbcPalette palette;
    }

    // Documentation: https://tcrf.net/Notes:Game_Boy_Color_Bootstrap_ROM#Assigned_Palette_Configurations
    private const GbcColorOverride[] GBC_PALETTE_OVERRIDES = {
        { "ALLEY WAY",         { 0x08, 0 }}, // Alleyway
        { "ASTEROIDS/MISCMD",  { 0x0E, 3 }}, // Arcade Classic No. 1 - Asteroids & Missile Command
        { "BA.TOSHINDEN",      { 0x0F, 5 }}, // Battle Arena Toshinden
        { "BALLOON KID",       { 0x06, 0 }}, // Balloon Kid
        { "BASEBALL",          { 0x03, 5 }}, // Baseball
        { "BOY AND BLOB GB2",  { 0x12, 5 }}, // David Crane's The Rescue of Princess Blobette Starring A Boy and His Blob
        { "BT2RAGNAROKWORLD",  { 0x12, 3 }}, // Battletoads in Ragnarok's World
        { "DEFENDER/JOUST",    { 0x0F, 5 }}, // Arcade Classic No. 4 - Defender & Joust
        { "DMG FOOTBALL",      { 0x0E, 3 }}, // Play Action Football
        { "DONKEY KONG",       { 0x06, 3 }}, // Donkey Kong
        { "DONKEYKONGLAND",    { 0x0C, 5 }}, // Donkey Kong Land
        { "DONKEYKONGLAND 2",  { 0x0C, 5 }}, // Donkey Kong Land 2
        { "DONKEYKONGLAND 3",  { 0x0C, 5 }}, // Donkey Kong Land III
        { "DONKEYKONGLAND95",  { 0x01, 5 }}, // Donkey Kong Land
        { "DR.MARIO",          { 0x0B, 2 }}, // Dr. Mario
        { "DYNABLASTER",       { 0x0F, 3 }}, // Dynablaster
        { "Dr.MARIO",          { 0x08, 5 }}, // Dr. Mario (Beta, via a hash collision with HOSHINOKA-BI)
        { "F1RACE",            { 0x12, 0 }}, // F-1 Race
        { "G&W GALLERY",       { 0x04, 3 }}, // Game & Watch Gallery
        { "GALAGA&GALAXIAN ",  { 0x13, 0 }}, // Arcade Classic No. 3 - Galaga & Galaxian
        { "GAME&WATCH",        { 0x12, 0 }}, // Game Boy Gallery
        { "GAMEBOY GALLERY",   { 0x04, 3 }}, // Game Boy Gallery
        { "GAMEBOY GALLERY2",  { 0x04, 3 }}, // Game Boy Gallery 2
        { "GAMEBOYCAMERA G",   { 0x10, 1 }}, // Game Boy Camera Gold
        { "GBWARS",            { 0x00, 5 }}, // Game Boy Wars
        { "GOLF",              { 0x0E, 3 }}, // Golf
        { "HOSHINOKA-BI",      { 0x08, 5 }}, // Hoshi no Kirby
        { "JAMES  BOND  007",  { 0x1C, 1 }}, // James Bond 007
        { "KAERUNOTAMENI",     { 0x0D, 1 }}, // Kaeru no Tame ni Kane wa Naru
        { "KEN GRIFFEY JR",    { 0x1C, 3 }}, // Ken Griffey Jr. presents Major League Baseball
        { "KID ICARUS",        { 0x0D, 3 }}, // Kid Icarus - Of Myths and Monsters
        { "KILLERINSTINCT95",  { 0x0D, 5 }}, // Killer Instinct
        { "KINGOFTHEZOO",      { 0x0F, 3 }}, // King of the Zoo
        { "KIRAKIRA KIDS",     { 0x12, 0 }}, // Kirby no Kirakira Kids
        { "KIRBY BLOCKBALL",   { 0x08, 5 }}, // Kirby's Block Ball / Kirby no Block Ball
        { "KIRBY DREAM LAND",  { 0x08, 5 }}, // Kirby's Dream Land
        { "KIRBY'S PINBALL",   { 0x08, 3 }}, // Kirby's Pinball / Kirby no Pinball
        { "KIRBY2",            { 0x08, 5 }}, // Kirby's Dream Land 2 / Hoshi no Kirby 2
        { "LOLO2",             { 0x0F, 5 }}, // Adventures of Lolo
        { "MAGNETIC SOCCER",   { 0x0E, 5 }}, // Magnetic Soccer
        { "MANSELL",           { 0x12, 0 }}, // Nigel Mansell's World Championship Racing
        { "MARIO & YOSHI",     { 0x05, 3 }}, // Mario & Yoshi
        { "MARIO'S PICROSS",   { 0x12, 0 }}, // Mario's Picross / Mario no Picross
        { "MARIOLAND2",        { 0x09, 5 }}, // Super Mario Land 2 - 6 Golden Coins / Super Mario Land 2 - 6-tsu no Kinka
        { "MEGA MAN 2",        { 0x0F, 5 }}, // Mega Man II
        { "MEGAMAN",           { 0x0F, 5 }}, // Mega Man - Dr. Wily's Revenge
        { "MEGAMAN3",          { 0x0F, 5 }}, // Mega Man III
        { "METROID2",          { 0x14, 5 }}, // Metroid II - Return of Samus
        { "MILLI/CENTI/PEDE",  { 0x1C, 3 }}, // Arcade Classic No. 2 - Centipede & Millipede
        { "MOGURANYA",         { 0x00, 3 }}, // Mole Mania / Moguranya
        { "MYSTIC QUEST",      { 0x0E, 5 }}, // Mystic Quest
        { "NETTOU KOF 95",     { 0x0F, 5 }}, // King of Fighters '95, The
        { "OTHELLO",           { 0x0E, 5 }}, // Othello
        { "PAC-IN-TIME",       { 0x1C, 5 }}, // Pac-In-Time
        { "PICROSS 2",         { 0x12, 0 }}, // Picross 2
        { "PINOCCHIO",         { 0x0C, 2 }}, // Pinocchio
        { "POCKETCAMERA",      { 0x1B, 0 }}, // Pocket Camera
        { "POKEBOM",           { 0x0C, 3 }}, // Pocket Bomberman
        { "POKEMON BLUE",      { 0x0B, 1 }}, // Pokemon Blue / Pocket Monsters Ao
        { "POKEMON GREEN",     { 0x1C, 1 }}, // Pocket Monsters Midori
        { "POKEMON RED",       { 0x10, 1 }}, // Pokemon Red / Pocket Monsters Aka
        { "POKEMON YELLOW",    { 0x07, 0 }}, // Pokemon Yellow / Pocket Monsters - Pikachu
        { "QIX",               { 0x07, 4 }}, // Qix
        { "RADARMISSION",      { 0x01, 0 }}, // Radar Mission
        { "SOCCER",            { 0x02, 5 }}, // Soccer
        { "SOLARSTRIKER",      { 0x13, 0 }}, // Solar Striker
        { "SPACE INVADERS",    { 0x13, 0 }}, // Space Invaders
        { "STAR STACKER",      { 0x12, 0 }}, // Kirby's Star Stacker
        { "STAR WARS-NOA",     { 0x12, 5 }}, // Star Wars
        { "STREET FIGHTER 2",  { 0x0F, 5 }}, // Street Fighter II
        { "SUPER MARIOLAND",   { 0x0A, 3 }}, // Super Mario Land
        { "SUPER RC PRO-AM",   { 0x0F, 5 }}, // Super R.C. Pro-Am
        { "SUPERDONKEYKONG ",  { 0x01, 5 }}, // Super Donkey Kong GB
        { "SUPERMARIOLAND3",   { 0x00, 5 }}, // Wario Land - Super Mario Land 3
        { "TENNIS",            { 0x02, 5 }}, // Tennis
        { "TETRIS",            { 0x07, 0 }}, // Tetris
        { "TETRIS ATTACK",     { 0x05, 4 }}, // Tetris Attack
        { "TETRIS BLAST",      { 0x06, 0 }}, // Tetris Blast
        { "TETRIS FLASH",      { 0x07, 4 }}, // Tetris Flash
        { "TETRIS PLUS",       { 0x1C, 3 }}, // Tetris Plus
        { "TETRIS2",           { 0x07, 4 }}, // Tetris 2
        { "THE CHESSMASTER",   { 0x0F, 3 }}, // Chessmaster, The
        { "TOPRANKINGTENNIS",  { 0x02, 5 }}, // Top Ranking Tennis
        { "TOPRANKTENNIS",     { 0x02, 5 }}, // Top Rank Tennis
        { "TOY STORY",         { 0x0E, 3 }}, // Toy Story
        { "VEGAS STAKES",      { 0x0E, 5 }}, // Vegas Stakes
        { "WARIO BLAST",       { 0x1C, 3 }}, // Wario Blast Featuring Bomberman!
        { "WARIOLAND2",        { 0x15, 5 }}, // Wario Land II
        { "WAVERACE",          { 0x0B, 5 }}, // Wave Race
        { "WORLD CUP",         { 0x0E, 3 }}, // Nintendo World Cup
        { "X",                 { 0x16, 0 }}, // X
        { "YAKUMAN",           { 0x12, 0 }}, // Yakuman
        { "YOSHI'S COOKIE",    { 0x06, 4 }}, // Yoshi's Cookie
        { "YOSSY NO COOKIE",   { 0x06, 4 }}, // Yoshi no Cookie
        { "YOSSY NO PANEPON",  { 0x05, 4 }}, // Yoshi no Panepon
        { "YOSSY NO TAMAGO",   { 0x05, 3 }}, // Yoshi / Yoshi no Tamago
        { "ZELDA",             { 0x11, 5 }}, // Legend of Zelda, The - Link's Awakening / Zelda no Densetsu - Yume o Miru Shima
    };

    private struct SgbColorOverride {
        string title;
        Palette.SgbPalette palette;
    }

    // Originally in CP1251, see BALLôôN KID
    // Documentation: https://tcrf.net/Notes:Super_Game_Boy#Game_Boy_Internal_Name_Table
    private const SgbColorOverride[] SGB_PALETTE_OVERRIDES = {
        { "ALLEY WAY",        3_F }, // Alleyway
        { "BALLôôN KID",      1_A }, // Balloon Kid (the title is misspelled)
        { "BASEBALL",         2_G }, // Baseball
        { "DR.MARIO",         3_B }, // Dr. Mario
        { "F1RACE",           4_F }, // F-1 Race
        { "GBWARS",           3_E }, // Game Boy Wars
        { "GOLF",             3_H }, // Golf
        { "HOSHINOKA-BI",     2_C }, // Hoshi no Kirby
        { "KAERUNOTAMENI",    2_A }, // Kaeru no Tame ni Kane wa Naru
        { "KID ICARUS",       2_F }, // Kid Icarus - Of Myths and Monsters
        { "KIRBY DREAM LAND", 2_C }, // Kirby's Dream Land
        { "KIRBY'S PINBALL",  1_C }, // Kirby's Pinball / Kirby no Pinball
        { "MARIO & YOSHI",    2_D }, // Mario & Yoshi
        { "MARIOLAND2",       3_D }, // Super Mario Land 2 - 6 Golden Coins / Super Mario Land 2 - 6-tsu no Kinka
        { "METROID2",         4_G }, // Metroid II - Return of Samus
        { "QIX",              4_A }, // Qix
        { "SOLARSTRIKER",     1_G }, // Solar Striker
        { "SUPER MARIOLAND",  1_F }, // Super Mario Land
        { "SUPERMARIOLAND3",  1_B }, // Wario Land - Super Mario Land 3
        { "TENNIS",           3_G }, // Tennis
        { "TETRIS",           3_A }, // Tetris
        { "X",                4_D }, // X
        { "YAKUMAN",          3_C }, // Yakuman
        { "YOSHI'S COOKIE",   1_D }, // Yoshi's Cookie
        { "YOSSY NO COOKIE",  1_D }, // Yoshi no Cookie
        { "YOSSY NO TAMAGO",  2_D }, // Yoshi / Yoshi no Tamago
        { "ZELDA",            1_E }, // Legend of Zelda, The - Link's Awakening / Zelda no Densetsu - Yume o Miru Shima
    };

    private const string[] GBA_ENHANCED_GAMES = {
        "SHANTAE",     // Shantae
        "WENDY",       // Wendy - Every Witch Way
        "ZELDA DIN",   // Legend of Zelda, The - Oracle of Seasons / Zelda no Densetsu - Fushigi no Kinomi - Daichi no Shou
        "ZELDA NAYRU", // Legend of Zelda, The - Oracle of Ages / Zelda no Densetsu - Fushigi no Kinomi - Jikuu no Shou
    };

    private const string[] SGB2_ENHANCED_GAMES = {
        "TETRIS DX", // Tetris DX
    };

    private inline GbcColorOverride? find_gbc_palette_override (string title) {
        void *ret = Posix.bsearch (
            title,
            GBC_PALETTE_OVERRIDES,
            GBC_PALETTE_OVERRIDES.length,
            sizeof (GbcColorOverride),
            (a, b) => {
                var needle = (string) a;
                var override = (GbcColorOverride *) b;
                return strcmp (needle, override.title);
            }
        );

        if (ret == null)
            return null;

        return * ((GbcColorOverride *) ret);
    }

    private inline SgbColorOverride? find_sgb_palette_override (string title) {
        void *ret = Posix.bsearch (
            title,
            SGB_PALETTE_OVERRIDES,
            SGB_PALETTE_OVERRIDES.length,
            sizeof (SgbColorOverride),
            (a, b) => {
                var needle = (string) a;
                var override = (SgbColorOverride *) b;
                return strcmp (needle, override.title);
            }
        );

        if (ret == null)
            return null;

        return * ((SgbColorOverride *) ret);
    }

    public Palette.GbcPalette? find_gbc_palette (string title) {
        return find_gbc_palette_override (title)?.palette;
    }

    public Palette.SgbPalette? find_sgb_palette (string title) {
        return find_sgb_palette_override (title)?.palette;
    }

    public bool is_gba_enhanced (string title) {
        return title in GBA_ENHANCED_GAMES;
    }

    public bool is_sgb2_enhanced (string title) {
        return title in SGB2_ENHANCED_GAMES;
    }
}
