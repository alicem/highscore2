// This file is part of Highscore. License: GPL-3.0+.

private class Highscore.GameBoy.PaletteCard : Gtk.FlowBoxChild {
    public string id { get; private set; }
    private int[] colors;

    public PaletteCard (string id, string name, owned int[]? colors = null) {
        if (colors == null)
            Palette.parse (id, out colors);

        assert (colors != null);
        assert (colors.length == 4 || colors.length == 12);

        this.id = id;
        this.colors = colors;

        tooltip_text = name;

        width_request = 70;
        height_request = 64;
        overflow = HIDDEN;
    }

    private Gdk.RGBA color_to_rgba (int index) {
        int rgb = colors[index];

        float r = ((rgb >> 16) & 0xFF) / 255.0f;
        float g = ((rgb >> 8) & 0xFF) / 255.0f;
        float b = (rgb & 0xFF) / 255.0f;

        return { r, g, b, 1.0f };
    }

    public override void snapshot (Gtk.Snapshot snapshot) {
        int w = get_width ();
        int h = get_height ();

        snapshot.append_color (color_to_rgba (0), {{ 0,     0 },     { w / 2, h / 2 }});
        snapshot.append_color (color_to_rgba (1), {{ 0,     h / 2 }, { w / 2, h / 2 }});
        snapshot.append_color (color_to_rgba (2), {{ w / 2, h / 2 }, { w / 2, h / 2 }});
        snapshot.append_color (color_to_rgba (3), {{ w / 2, 0 },     { w / 2, h / 2 }});

        float r = 8;

        if (colors.length == 4)
            return;

        snapshot.translate ({ w / 4, h / 4 });

        Gsk.RoundedRect rect = {};
        rect.init_from_rect ({{ -r, -r }, { r * 2, r * 2 }}, r);
        snapshot.push_rounded_clip (rect);
        snapshot.append_color (color_to_rgba (4), {{ -r, -r }, { r, r }});
        snapshot.append_color (color_to_rgba (5), {{ -r,  0 }, { r, r }});
        snapshot.append_color (color_to_rgba (6), {{  0,  0 }, { r, r }});
        snapshot.append_color (color_to_rgba (7), {{  0, -r }, { r, r }});
        snapshot.pop ();
        snapshot.append_outset_shadow (rect, color_to_rgba (7), 0, 0, 1, 0);

        snapshot.translate ({ w / 2, h / 2 });
        rect.init_from_rect ({{ -r, -r }, { r * 2, r * 2 }}, r);
        snapshot.push_rounded_clip (rect);
        snapshot.append_color (color_to_rgba (8),  {{ -r, -r }, { r, r }});
        snapshot.append_color (color_to_rgba (9),  {{ -r,  0 }, { r, r }});
        snapshot.append_color (color_to_rgba (10), {{  0,  0 }, { r, r }});
        snapshot.append_color (color_to_rgba (11), {{  0, -r }, { r, r }});
        snapshot.pop ();
        snapshot.append_outset_shadow (rect, color_to_rgba (11), 0, 0, 1, 0);
    }

    public int[] get_colors () {
        return colors;
    }
}

[GtkTemplate (ui = "/org/gnome/World/Highscore2/platforms/game-boy/game-boy-palette-switcher.ui")]
public class Highscore.GameBoy.PaletteSwitcher : Adw.Bin {
    public Metadata metadata { get; construct; }

    private string? _palette;
    public string? palette {
        get { return _palette; }
        set {
            if (value == null) {
                _palette = value;
                sync_selection ();
                return;
            }

            if (value == "gbc-special") {
                if (gbc_special != null) {
                    select_palette (gbc_special);
                    return;
                }

                var card = sgb_flowbox.get_child_at_index (
                    Palette.DEFAULT_GBC_PALETTE
                ) as PaletteCard;
                select_palette (card);
                return;
            }

            if (value == "sgb-special") {
                if (sgb_special != null) {
                    select_palette (sgb_special);
                    return;
                }

                var card = sgb_flowbox.get_child_at_index (
                    Palette.DEFAULT_SGB_PALETTE
                ) as PaletteCard;
                select_palette (card);
                return;
            }

            Palette.SimplePalette? simple;
            Palette.ManualGbcPalette? gbc;
            Palette.SgbPalette? sgb;

            if (!Palette.parse (value, null, out simple, out gbc, out sgb)) {
                _palette = value;
                critical ("Unknown palette ID: %s", value);
                return;
            }

            if (simple != null) {
                var card = simple_flowbox.get_child_at_index (simple) as PaletteCard;
                select_palette (card);
                return;
            }

            if (gbc != null) {
                var card = gbc_flowbox.get_child_at_index (gbc) as PaletteCard;
                select_palette (card);
                return;
            }

            if (sgb != null) {
                var card = sgb_flowbox.get_child_at_index (sgb) as PaletteCard;
                select_palette (card);
                return;
            }

            assert_not_reached ();
        }
    }

    [GtkChild]
    private unowned Gtk.FlowBox simple_flowbox;
    [GtkChild]
    private unowned Gtk.FlowBox special_flowbox;
    [GtkChild]
    private unowned Gtk.FlowBox gbc_flowbox;
    [GtkChild]
    private unowned Gtk.FlowBox sgb_flowbox;

    private PaletteCard gbc_special;
    private PaletteCard sgb_special;

    private PaletteCard current_card;

    public PaletteSwitcher (Metadata metadata) {
        Object (metadata: metadata);
    }

    construct {
        var gbc_palette = metadata.get_special_gbc_palette ();
        var sgb_palette = metadata.get_special_sgb_palette ();

        foreach (var palette in Palette.SimplePalette.all ()) {
            simple_flowbox.append (new PaletteCard (
                palette.get_id (), palette.get_name ()
            ));
        }

        if (gbc_palette != null) {
            special_flowbox.append (gbc_special = new PaletteCard (
                "gbc-special",
                _("Game Boy Color"),
                gbc_palette.get_colors ()
            ));
        }

        if (sgb_palette != null) {
            char column = 'A' + (char) sgb_palette % 8;
            int row = sgb_palette / 8 + 1;

            special_flowbox.append (sgb_special = new PaletteCard (
                "sgb-special",
                _("Super Game Boy (%d-%c)").printf (row, column),
                sgb_palette.get_colors ()
            ));
        }

        special_flowbox.visible = gbc_palette != null || sgb_palette != null;

        foreach (var palette in Palette.ManualGbcPalette.all ()) {
            var name = palette.get_name ();

            if (gbc_palette == null && palette == Palette.DEFAULT_GBC_PALETTE)
                name = _("%s (Default)").printf (name);

            gbc_flowbox.append (new PaletteCard (
                palette.get_id (), name, palette.get_colors ()
            ));
        }

        foreach (var palette in Palette.SgbPalette.all ()) {
            string name = palette.get_name ();

            if (sgb_palette == null && palette == Palette.DEFAULT_SGB_PALETTE)
                name = _("%s (Default)").printf (name);

            sgb_flowbox.append (new PaletteCard (
                palette.get_id (), name, palette.get_colors ()
            ));
        }

        palette = "grayscale";
    }

    [GtkCallback]
    private void child_activated_cb (Gtk.FlowBoxChild child) {
        select_palette (child as PaletteCard);
    }

    private void select_palette (PaletteCard card) {
        current_card = card;
        _palette = card.id;

        sync_selection ();

        notify_property ("palette");
    }

    private void sync_selection () {
        Gtk.FlowBox[] flowboxes = {
            simple_flowbox, special_flowbox, gbc_flowbox, sgb_flowbox
        };

        foreach (var flowbox in flowboxes) {
            if (current_card.parent == flowbox)
                flowbox.select_child (current_card);
            else
                flowbox.unselect_all ();
        }
    }

    public int[] get_colors () {
        return current_card.get_colors ();
    }
}
