// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.PcEngine.Controls : PlatformControls {
    construct {
        // 2 button controller

        var controller = new PlatformController ("pad", _("Pad"));
        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("D-pad"),
            Hs.PcEngineButton.UP,
            Hs.PcEngineButton.DOWN,
            Hs.PcEngineButton.LEFT,
            Hs.PcEngineButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed,
            "i",      _("I"),      Hs.PcEngineButton.I,
            "ii",     _("II"),     Hs.PcEngineButton.II,
            "run",    _("Run"),    Hs.PcEngineButton.RUN,
            "select", _("Select"), Hs.PcEngineButton.SELECT,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,        "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_D,         "i");
        kbd_mapping.map (Linux.Input.KEY_S,         "ii");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "run");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (UP,     "dpad:up");
        gamepad_mapping.map_button (DOWN,   "dpad:down");
        gamepad_mapping.map_button (LEFT,   "dpad:left");
        gamepad_mapping.map_button (RIGHT,  "dpad:right");
        gamepad_mapping.map_button (EAST,   "i");
        gamepad_mapping.map_button (SOUTH,  "ii");
        gamepad_mapping.map_button (START,  "run");
        gamepad_mapping.map_button (SELECT, "select");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);
        default_controller = "pad";

        // 6 button controller

        controller = new PlatformController ("six-button-pad", _("6 Button Pad"));
        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("D-pad"),
            Hs.PcEngineButton.UP,
            Hs.PcEngineButton.DOWN,
            Hs.PcEngineButton.LEFT,
            Hs.PcEngineButton.RIGHT
        );
        ControlHelpers.add_buttons (
            controller, set_pressed,
            "i",      _("I"),      Hs.PcEngineButton.I,
            "ii",     _("II"),     Hs.PcEngineButton.II,
            "iii",    _("III"),    Hs.PcEngineButton.III,
            "iv",     _("IV"),     Hs.PcEngineButton.IV,
            "v",      _("V"),      Hs.PcEngineButton.V,
            "vi",     _("VI"),     Hs.PcEngineButton.VI,
            "run",    _("Run"),    Hs.PcEngineButton.RUN,
            "select", _("Select"), Hs.PcEngineButton.SELECT,
            null
        );

        kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,        "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,      "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,      "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,     "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_D,         "i");
        kbd_mapping.map (Linux.Input.KEY_S,         "ii");
        kbd_mapping.map (Linux.Input.KEY_A,         "iii");
        kbd_mapping.map (Linux.Input.KEY_Q,         "iv");
        kbd_mapping.map (Linux.Input.KEY_W,         "v");
        kbd_mapping.map (Linux.Input.KEY_E,         "vi");
        kbd_mapping.map (Linux.Input.KEY_ENTER,     "run");
        kbd_mapping.map (Linux.Input.KEY_BACKSPACE, "select");
        controller.default_keyboard_mapping = kbd_mapping;

        gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_button (UP,     "dpad:up");
        gamepad_mapping.map_button (DOWN,   "dpad:down");
        gamepad_mapping.map_button (LEFT,   "dpad:left");
        gamepad_mapping.map_button (RIGHT,  "dpad:right");
        gamepad_mapping.map_button (EAST,   "i");
        gamepad_mapping.map_button (SOUTH,  "ii");
        gamepad_mapping.map_button (WEST,   "iii");
        gamepad_mapping.map_button (NORTH,  "iv");
        gamepad_mapping.map_button (L,      "v");
        gamepad_mapping.map_button (R,      "vi");
        gamepad_mapping.map_button (START,  "run");
        gamepad_mapping.map_button (SELECT, "select");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->pc_engine.pad_buttons[player], button, pressed);
        });
    }
}
