// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.PcEngine.RunnerDelegate : Highscore.RunnerDelegate {
    public bool six_button_controller { get; set; }

    private Settings settings;

    construct {
        settings = new GameSettings (runner.game).get_platform ();

        settings.bind ("six-button-controller", this, "six-button-controller", GET);

        notify["six-button-controller"].connect (update_pad_mode);
    }

    private void update_pad_mode () {
        Hs.PcEnginePadMode pad_mode = TWO_BUTTONS;

        if (six_button_controller)
            pad_mode = SIX_BUTTONS;

        for (int i = 0; i < Hs.PC_ENGINE_MAX_PLAYERS; i++) {
            runner.set_controller_type (
                0, six_button_controller ? "six-button-pad" : "pad"
            );
        }

        runner.input_buffer.modify_input_state (state => {
            for (int i = 0; i < Hs.PC_ENGINE_MAX_PLAYERS; i++)
                state->pc_engine.pad_mode[i] = pad_mode;
        });
    }

    public override async void after_load () throws Error {
        update_pad_mode ();
    }
}
