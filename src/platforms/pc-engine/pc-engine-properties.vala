// This file is part of Highscore. License: GPL-3.0+.

[GtkTemplate (ui = "/org/gnome/World/Highscore2/platforms/pc-engine/pc-engine-properties.ui")]
public class Highscore.PcEngine.Properties : PlatformProperties {
    private Settings settings;

    public bool six_button_controller { get; set; }

    construct {
        settings = new GameSettings (game).get_platform ();

        settings.bind ("six-button-controller", this, "six-button-controller", DEFAULT);
    }
}
