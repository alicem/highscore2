// This file is part of Highscore. License: GPL-3.0+.

[DBus (name = "org.gnome.World.Highscore2.Runner")]
public interface Highscore.Nintendo64.Proxy : Object {
    public abstract async uint get_players () throws Error;

    public abstract async void set_controller_present (uint player, bool present) throws Error;
}
