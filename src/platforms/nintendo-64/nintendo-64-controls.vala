// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Nintendo64.Controls : PlatformControls {
    construct {
        var controller = new PlatformController ("pad", "");

        var analog = new AnalogControl ("stick", _("Control Stick"));
        analog.moved.connect (control_stick_moved_cb);
        controller.add_control (analog);

        ControlHelpers.add_dpad (
            controller, set_pressed,
            "dpad", _("D-pad"),
            Hs.Nintendo64Button.UP,
            Hs.Nintendo64Button.DOWN,
            Hs.Nintendo64Button.LEFT,
            Hs.Nintendo64Button.RIGHT
        );

        ControlHelpers.add_buttons (
            controller, set_pressed,
            "a",       _("A"),         Hs.Nintendo64Button.A,
            "b",       _("B"),         Hs.Nintendo64Button.B,
            "c-up",    _("C (Up)"),    Hs.Nintendo64Button.C_UP,
            "c-down",  _("C (Down)"),  Hs.Nintendo64Button.C_DOWN,
            "c-left",  _("C (Left)"),  Hs.Nintendo64Button.C_LEFT,
            "c-right", _("C (Right)"), Hs.Nintendo64Button.C_RIGHT,
            "l",       _("L"),         Hs.Nintendo64Button.L,
            "r",       _("R"),         Hs.Nintendo64Button.R,
            "z",       _("Z"),         Hs.Nintendo64Button.Z,
            "start",   _("Start"),     Hs.Nintendo64Button.START,
            null
        );

        var kbd_mapping = new PlatformKeyboardMapping ();
        kbd_mapping.map (Linux.Input.KEY_UP,         "stick:up");
        kbd_mapping.map (Linux.Input.KEY_DOWN,       "stick:down");
        kbd_mapping.map (Linux.Input.KEY_LEFT,       "stick:left");
        kbd_mapping.map (Linux.Input.KEY_RIGHT,      "stick:right");
        kbd_mapping.map (Linux.Input.KEY_I,          "dpad:up");
        kbd_mapping.map (Linux.Input.KEY_K,          "dpad:down");
        kbd_mapping.map (Linux.Input.KEY_J,          "dpad:left");
        kbd_mapping.map (Linux.Input.KEY_L,          "dpad:right");
        kbd_mapping.map (Linux.Input.KEY_W,          "c-up");
        kbd_mapping.map (Linux.Input.KEY_S,          "c-down");
        kbd_mapping.map (Linux.Input.KEY_A,          "c-left");
        kbd_mapping.map (Linux.Input.KEY_D,          "c-right");
        kbd_mapping.map (Linux.Input.KEY_X,          "a");
        kbd_mapping.map (Linux.Input.KEY_Z,          "b");
        kbd_mapping.map (Linux.Input.KEY_LEFTSHIFT,  "l");
        kbd_mapping.map (Linux.Input.KEY_RIGHTSHIFT, "r");
        kbd_mapping.map (Linux.Input.KEY_SPACE,      "z");
        kbd_mapping.map (Linux.Input.KEY_ENTER,      "start");
        controller.default_keyboard_mapping = kbd_mapping;

        var gamepad_mapping = new PlatformGamepadMapping ();
        gamepad_mapping.map_analog (LEFT,  "stick");
        gamepad_mapping.map_button (UP,    "dpad:up");
        gamepad_mapping.map_button (DOWN,  "dpad:down");
        gamepad_mapping.map_button (LEFT,  "dpad:left");
        gamepad_mapping.map_button (RIGHT, "dpad:right");
        gamepad_mapping.map_button (SOUTH, "a");
        gamepad_mapping.map_button (WEST,  "b");
        gamepad_mapping.map_button (L,     "l");
        gamepad_mapping.map_button (R,     "r");
        gamepad_mapping.map_button (L2,    "z");
        gamepad_mapping.map_button (START, "start");
        gamepad_mapping.map_button_with_modifier (R2, NORTH, "c-up");
        gamepad_mapping.map_button_with_modifier (R2, SOUTH, "c-down");
        gamepad_mapping.map_button_with_modifier (R2, WEST,  "c-left");
        gamepad_mapping.map_button_with_modifier (R2, EAST,  "c-right");
        gamepad_mapping.map_analog_to_buttons (RIGHT, "c-up", "c-down", "c-left", "c-right");
        controller.default_gamepad_mapping = gamepad_mapping;

        add_controller (controller);
        default_controller = "pad";
    }

    private static void set_pressed (Runner runner, uint player, int button, bool pressed) {
        runner.input_buffer.modify_input_state (state => {
            set_bit (ref state->nintendo_64.pad_buttons[player], button, pressed);
        });
    }

    private static void control_stick_moved_cb (Runner runner, uint player, double x, double y) {
        runner.input_buffer.modify_input_state (state => {
            state->nintendo_64.pad_control_stick_x[player] = x;
            state->nintendo_64.pad_control_stick_y[player] = y;
        });
    }
}
