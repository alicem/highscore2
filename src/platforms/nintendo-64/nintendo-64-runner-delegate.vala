// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Nintendo64.RunnerDelegate : Highscore.RunnerDelegate {
    public override async void after_load () throws Error {
        var proxy = runner.platform_proxy as Proxy;

        yield proxy.set_controller_present (0, true);
    }
}
