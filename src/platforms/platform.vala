// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Platform : Object {
    public Hs.Platform platform { get; construct; }
    public string id { get; construct; }
    public string name { get; construct; }

    public string icon_name { get; set; }

    public Type parser_type { get; set; default = Type.NONE; }
    public Type runner_delegate_type { get; set; default = Type.NONE; }
    public Type controls_type { get; set; default = Type.NONE; }
    public Type metadata_type { get; set; default = Type.NONE; }
    public Type properties_type { get; set; default = Type.NONE; }
    public Type screen_set_type { get; set; default = Type.NONE; }

    public Platform parent { get; set; }

    private string[] mime_types;

    public Platform (Hs.Platform platform, string id, string name, string mime_type) {
        Object (platform: platform, id: id, name: name);

        icon_name = @"platform-$id-symbolic";
        mime_types = { mime_type };
    }

    public Platform.with_mime_types (Hs.Platform platform, string id, string name, string[] mime_types) {
        Object (platform: platform, id: id, name: name);

        icon_name = @"platform-$id-symbolic";
        this.mime_types = mime_types;
    }

    public string[] get_mime_types () {
        return mime_types;
    }

    public static uint hash (Platform platform) {
        return int_hash (platform.platform);
    }

    public static bool equal (Platform a, Platform b) {
        return a == b || a.platform == b.platform;
    }

    public static int compare (Platform a, Platform b) {
        return a.name.collate (b.name);
    }
}
