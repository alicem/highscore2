// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.Runner : Object {
    public delegate void AdjustInputStateFunc (Hs.InputState* state);

    private class Frontend : Object, Hs.Frontend {
        private weak Runner runner;

        public Frontend (Runner runner) {
            this.runner = runner;
        }

        public void play_samples (int16[] samples) {
            runner.player.add_samples (samples);
        }

        public Hs.SoftwareContext? create_software_context (uint width, uint height, Hs.PixelFormat format) {
            var renderer = new SoftwareRenderer (
                runner.video_buffer, width, height, format
            );

            runner.renderer = renderer;
            return renderer;
        }

        public Hs.GLContext? create_gl_context (
            Hs.GLProfile profile,
            int major_version,
            int minor_version,
            Hs.GLFlags flags
        ) {
            var renderer = new OpenGLRenderer (
                runner.video_buffer, profile, major_version, minor_version, flags
            );

            runner.renderer = renderer;
            return renderer;
        }

        public string get_cache_path () {
            return Path.build_filename (
                Environment.get_user_cache_dir (),
                "highscore",
                runner.core.name,
                null
            );
        }

        public void log (Hs.LogLevel level, string message) {
            LogLevelFlags flags;

            switch (level) {
                case DEBUG:
                    flags = LEVEL_DEBUG;
                    break;
                case INFO:
                    flags = LEVEL_INFO;
                    break;
                case MESSAGE:
                    flags = LEVEL_MESSAGE;
                    break;
                case WARNING:
                    flags = LEVEL_WARNING;
                    break;
                case CRITICAL:
                    flags = LEVEL_CRITICAL;
                    break;
                default:
                    assert_not_reached ();
            }

            log_structured (runner.core.name, flags, "MESSAGE", message);
        }
    }

    public signal void redraw ();

    public Hs.Core core { get; private set; }
    public bool running { get; private set; }

    public bool flipped { get; private set; }

    private uint main_loop;

    private SharedVideoBuffer video_buffer;
    private SharedInputBuffer input_buffer;
    private Renderer? renderer;

    private PaPlayer player;
    private uint32 sample_rate;
    private uint8 channels;

    private Frontend frontend;

    private List<void*> scheduled_adjusts;

    public Runner (Hs.Core core, SharedVideoBuffer video_buffer, SharedInputBuffer input_buffer) {
        this.core = core;
        this.video_buffer = video_buffer;
        this.input_buffer = input_buffer;

        frontend = new Frontend (this);
        core.frontend = frontend;

        player = new PaPlayer ();

        scheduled_adjusts = new List<void*> ();
    }

    ~Runner () {
        stop ();
    }

    public void load_rom (string rom_path, string save_path) throws Error {
        assert (!running);

        core.load_rom (rom_path, save_path);
    }

    public void start () {
        assert (!running);

        core.start ();

        player.start ();

        channels = (uint8) core.get_channels ();
        sample_rate = (uint32) core.get_sample_rate ();
        player.setup (channels, sample_rate);

        var source = new FrameSource (core.get_frame_rate ());
        source.set_callback (run_frame);

        main_loop = source.attach ();

        running = true;
    }

    public void stop () {
        if (!running)
            return;

        core.stop ();

        if (main_loop > 0) {
            Source.remove (main_loop);
            main_loop = 0;
        }

        player.stop ();

        running = false;
    }

    public void reset () {
        core.reset ();
    }

    private bool run_frame () {
        if (main_loop == 0)
            return false;

        var state = input_buffer.copy_input_state ();

        foreach (var data in scheduled_adjusts) {
            var func = (AdjustInputStateFunc) data;
            func (&state);
        }

        scheduled_adjusts = new List<void*> ();

        if (renderer != null && renderer.lock_during_frame ())
            video_buffer.lock ();

        core.poll_input (state);
        core.run_frame ();

        if (renderer != null) {
            if (!renderer.lock_during_frame ())
                video_buffer.lock ();

            bool updated = renderer.run_frame ();

            if (updated)
                video_buffer.set_aspect_ratio (core.get_aspect_ratio ());

            video_buffer.unlock ();

            if (updated)
                redraw ();
        }

        uint8 channels = (uint8) core.get_channels ();
        uint32 sample_rate = (uint32) core.get_sample_rate ();

        if (channels != this.channels || sample_rate != this.sample_rate) {
            this.channels = channels;
            this.sample_rate = sample_rate;
            player.setup (channels, sample_rate);
        }

        return true;
    }

    public void pause () {
        if (!running || main_loop == 0)
            return;

        Source.remove (main_loop);
        main_loop = 0;

        core.pause ();

        player.paused = true;
    }

    public void resume () {
        if (!running || main_loop > 0)
            return;

        var source = new FrameSource (core.get_frame_rate ());
        source.set_callback (run_frame);

        main_loop = source.attach ();

        player.paused = false;

        core.resume ();
    }

    public File get_save_dir () {
        var data_dir = Environment.get_user_data_dir ();
        var save_dir_path = Path.build_filename (data_dir, "highscore", "saves");

        return File.new_for_path (save_dir_path);
    }

    public void reload_save (string path) throws Error {
        core.reload_save (path);
    }

    public void sync_save () throws Error {
        core.sync_save ();
    }

    public async void save_state (string path) throws Error {
        yield core.save_state (path, null);
    }

    public async void load_state (string path) throws Error {
        yield core.load_state (path, null);
    }

    public void schedule_adjust_input_state (AdjustInputStateFunc func) {
        scheduled_adjusts.append ((void*) func);
    }
}
