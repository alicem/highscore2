// This file is part of Highscore. License: GPL-3.0+.

[Compact (opaque = true)]
public class Highscore.FrameSource : Source {
    private double delay;
    private int64 next_time;
    private double rounding_error;

    public FrameSource (double framerate) {
        delay = 1000000.0 / framerate;

        set_priority (Priority.DEFAULT_IDLE);
        set_ready_time (0);
        set_name ("HighscoreFrameSource");
    }

    protected override bool prepare (out int timeout) {
        int64 delay = next_time - get_monotonic_time ();

        timeout = int.max (0, (int) Math.floor (delay / 1000.0 - 1));

        return delay <= 0;
    }

    protected override bool dispatch (SourceFunc? callback) {
        int64 time = get_time ();

        if (callback == null)
            return Source.REMOVE;

        bool ret = callback ();

        double delay = Math.round (this.delay);

        rounding_error += (delay - this.delay);
        if (rounding_error >= 1) {
            delay--;
            rounding_error--;
        } else if (rounding_error <= -1) {
            delay++;
            rounding_error++;
        }

        if (next_time == 0 || next_time + delay <= time)
            next_time = time;

        next_time += (int64) delay;
        set_ready_time (next_time);

        return ret;
    }
}
