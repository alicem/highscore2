// This file is part of Highscore. License: GPL-3.0+.

public interface Highscore.Renderer : Object {
    public abstract bool run_frame ();

    public abstract bool lock_during_frame ();
}
