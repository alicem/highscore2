// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.SoftwareRenderer : Object, Hs.SoftwareContext, Renderer {
    private SharedVideoBuffer buffer;
    private uint width;
    private uint height;
    private Hs.PixelFormat format;

    private Hs.Rectangle area;
    private uint row_stride;

    public SoftwareRenderer (SharedVideoBuffer buffer, uint width, uint height, Hs.PixelFormat format) {
        this.buffer = buffer;
        this.width = width;
        this.height = height;
        this.format = format;

        this.area.init (0, 0, (int) width, (int) height);
        this.row_stride = width * format.get_pixel_size ();

        buffer.lock ();
        buffer.resize (height * width * format.get_pixel_size ());
        buffer.unlock ();
    }

    public void set_area (Hs.Rectangle area) {
        this.area = area;
    }

    public void set_row_stride (uint row_stride) {
        this.row_stride = row_stride;
    }

    public void* get_framebuffer () {
        return buffer.get_pixels ();
    }

    public bool run_frame () {
        buffer.set_frame_data (format, area, row_stride, false);
        return true;
    }

    public bool lock_during_frame () {
        return true;
    }
}
