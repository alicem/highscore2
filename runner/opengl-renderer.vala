// This file is part of Highscore. License: GPL-3.0+.

using GL;

public class Highscore.OpenGLRenderer : Object, Hs.GLContext, Renderer {
    private SharedVideoBuffer buffer;

    private Hs.GLProfile profile;
    private int major_version;
    private int minor_version;
    private Hs.GLFlags flags;

    private SDL.Video.Window? window;
    private SDL.Video.GL.Context? context;

    private int width;
    private int height;

    private int invalidated;

    public OpenGLRenderer (
        SharedVideoBuffer buffer,
        Hs.GLProfile profile,
        int major_version,
        int minor_version,
        Hs.GLFlags flags
    ) {
        this.buffer = buffer;
        this.profile = profile;
        this.major_version = major_version;
        this.minor_version = minor_version;
        this.flags = flags;
    }

    public void realize () {
        SDL.Video.GL.ProfileType profile_type;

        switch (profile) {
            case CORE:
                profile_type = CORE;
                break;
            case LEGACY:
                profile_type = COMPATIBILITY;
                break;
            case ES:
                profile_type = ES;
                break;
            default:
                assert_not_reached ();
        }

        SDL.Video.GL.set_attribute (CONTEXT_PROFILE_MASK, profile_type);
        SDL.Video.GL.set_attribute (CONTEXT_MAJOR_VERSION, major_version);
        SDL.Video.GL.set_attribute (CONTEXT_MINOR_VERSION, minor_version);

        if ((flags | Hs.GLFlags.DEPTH) > 0)
            SDL.Video.GL.set_attribute (DEPTH_SIZE, 16);
        if ((flags | Hs.GLFlags.STENCIL) > 0)
            SDL.Video.GL.set_attribute (STENCIL_SIZE, 8);

        SDL.init_subsystem (SDL.InitFlag.VIDEO);
    }

    public void set_size (uint width, uint height) {
        if (window != null) {
            window.destroy ();
            window = null;
            context = null;
        }

        window = new SDL.Video.Window (
            "Highscore OpenGL renderer",
            0, 0, (int) width, (int) height,
            SDL.Video.WindowFlags.OPENGL |
            SDL.Video.WindowFlags.HIDDEN |
            SDL.Video.WindowFlags.BORDERLESS
        );

        context = SDL.Video.GL.Context.create (window);
        SDL.Video.GL.make_current (window, context);

        this.width = (int) width;
        this.height = (int) height;

        glClearColor (0, 0, 0, 1);
        glClear (
            GL_COLOR_BUFFER_BIT |
            GL_DEPTH_BUFFER_BIT |
            GL_STENCIL_BUFFER_BIT
        );

        buffer.lock ();
        buffer.resize (height * width * 4);
        buffer.unlock ();
    }

    public void unrealize () {
        window.destroy ();
        window = null;
        context = null;
        SDL.quit_subsystem (SDL.InitFlag.VIDEO);
    }

    public void* get_proc_address (string name) {
        return SDL.Video.GL.get_proc_address (name);
    }

    public void* acquire_framebuffer () {
        buffer.lock ();

        return buffer.get_pixels ();
    }

    public void release_framebuffer () {
        buffer.unlock ();

        AtomicInt.set (ref invalidated, 1);
    }

    public void swap_buffers () {
        if (window == null)
            return;

        int width, height;
        window.get_size (out width, out height);

        if (!(DIRECT_FB_ACCESS in flags)) {
            uint read_buffer = GL_FRONT;
            glGetIntegerv (GL_READ_BUFFER, (GLint[]) &read_buffer);

            if (read_buffer == GL_BACK) {
                var buffer = acquire_framebuffer ();
                glReadPixels (0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, (GL.GLvoid[]) buffer);
                release_framebuffer ();
            } else {
                // The game is likely displaying garbage, so just show black screen
            }
        }

        SDL.Video.GL.swap_window (window);
    }

    public bool run_frame () {
        bool invalidated = AtomicInt.compare_and_exchange (ref this.invalidated, 1, 0);

        if (invalidated) {
            Hs.Rectangle area = { 0, 0, width, height };
            buffer.set_frame_data (XRGB8888, area, width * 4, FLIPPED in flags);
        }

        return invalidated;
    }

    public bool lock_during_frame () {
        return false;
    }
}
