public class Highscore.PaPlayer : Object {
    private const int MAX_QUEUE_LENGTH = 10;

    private PulseAudio.Simple simple;
    private Mutex player_mutex;

    private uint8 channels;
    private uint32 sample_rate;

    public bool paused { get; set; }

    private struct AudioBatch {
        int16[] samples;
        bool stop;
    }

    private Thread<void> thread;
    private AsyncQueue<AudioBatch?> batch_queue;

    public PaPlayer () {
        batch_queue = new AsyncQueue<AudioBatch?> ();
    }

    private void run_audio_thread () {
        while (true) {
            // Some games produce samples faster than we can play them. In that
            // case, play them all at once before continuing
            if (batch_queue.length () > MAX_QUEUE_LENGTH) {
                batch_queue.lock ();

                while (batch_queue.length_unlocked () > 0) {
                    var batch = batch_queue.pop_unlocked ();

                    if (play_batch (batch)) {
                        batch_queue.unlock ();
                        return;
                    }
                }

                batch_queue.unlock ();
                continue;
            }

            var batch = batch_queue.pop ();

            if (play_batch (batch))
                return;
        }
    }

    private bool play_batch (AudioBatch batch) {
        if (batch.stop)
            return true;

        player_mutex.lock ();
        simple.write (
            batch.samples,
            batch.samples.length * sizeof (int16),
            null
        );
        player_mutex.unlock ();

        return false;
    }

    public void start () {
        thread = new Thread<void> (null, run_audio_thread);
    }

    public void stop () {
        AudioBatch batch = { null, true };

        batch_queue.push (batch);

        thread.join ();
    }

    public void setup (uint8 channels, uint32 sample_rate) {
        player_mutex.lock ();

        if (simple != null) {
            simple.drain ();
            simple = null;
        }

        this.channels = channels;
        this.sample_rate = sample_rate;

        int error;

        var sample_spec = PulseAudio.SampleSpec () {
            format = S16NE,
            rate = sample_rate,
            channels = channels,
        };

        simple = new PulseAudio.Simple (null, null, PLAYBACK, null, "",
                                        sample_spec, null, null, out error);

        if (simple == null) {
            if (error != PulseAudio.Error.CONNECTIONREFUSED)
                warning ("No PulseAudio connection, audio will not work");
            else
                critical ("Failed to create audio player: %s",
                          PulseAudio.strerror (error));
        }

        player_mutex.unlock ();
    }

    public void add_samples (int16[] samples) {
        if (paused)
            return;

        AudioBatch batch = { samples, false };

        batch_queue.push (batch);
    }
}
