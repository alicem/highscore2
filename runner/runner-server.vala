// This file is part of Highscore. License: GPL-3.0+.

[DBus (name = "org.gnome.World.Highscore2.Runner")]
public class Highscore.RunnerServer : Object {
    public signal void redraw ();

    private Runner runner;
    private SharedVideoBuffer video_buffer;
    private SharedInputBuffer input_buffer;

    private CoreModule module;

    public RunnerServer (string core_name, string platform_name) {
        var platform = Hs.Platform.get_from_name (platform_name);

        Hs.Core core = null;

        try {
            module = new CoreModule (core_name);
            core = module.get_core (platform);
        } catch (Error e) {
            error ("Failed to load core: %s", e.message);
        }

        video_buffer = new SharedVideoBuffer ();
        input_buffer = new SharedInputBuffer (platform);

        runner = new Runner (core, video_buffer, input_buffer);

        runner.redraw.connect (() => {
            redraw ();
        });
    }

    internal void register_platform (DBusConnection connection) throws Error {
        var platform = runner.core.platform;

        register_platform_server (
            runner, platform, connection,
            "/org/gnome/World/Highscore2/Runner/"
        );

        var base_platform = platform.get_base_platform ();
        if (base_platform != platform) {
            register_platform_server (
                runner, base_platform, connection,
                "/org/gnome/World/Highscore2/Runner/"
            );
        }
    }

    public async void load_rom (string rom_path, string save_path) throws Error {
        runner.load_rom (rom_path, save_path);
    }

    public async void start () throws Error {
        runner.start ();
    }

    public async UnixInputStream get_input_buffer () throws Error {
        return wrap_fd (input_buffer);
    }

    public async UnixInputStream get_video_buffer () throws Error {
        return wrap_fd (video_buffer);
    }

    public async void stop () throws Error {
        runner.stop ();
    }

    public async void reset () throws Error {
        runner.reset ();
    }

    public async void pause () throws Error {
        runner.pause ();
    }

    public async void resume () throws Error {
        runner.resume ();
    }

    public async void reload_save (string path) throws Error {
        runner.reload_save (path);
    }

    public async void sync_save () throws Error {
        runner.sync_save ();
    }

    public async void save_state (string path) throws Error {
        yield runner.save_state (path);
    }

    public async void load_state (string path) throws Error {
        yield runner.load_state (path);
    }
}
