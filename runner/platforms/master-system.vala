// This file is part of Highscore. License: GPL-3.0+.

[DBus (name = "org.gnome.World.Highscore2.Runner")]
public class Highscore.MasterSystem.Server : PlatformServer {
    public async void set_enable_fm_audio (bool enable_fm_audio) throws Error {
        var sms_core = core as Hs.MasterSystemCore;
        sms_core.set_enable_fm_audio (enable_fm_audio);
    }

    public async void press_pause () throws Error {
        runner.schedule_adjust_input_state (state => {
            state->master_system.pause_button = true;
        });
    }
}
