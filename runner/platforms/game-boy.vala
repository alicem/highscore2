// This file is part of Highscore. License: GPL-3.0+.

[DBus (name = "org.gnome.World.Highscore2.Runner")]
public class Highscore.GameBoy.Server : PlatformServer {
    public async void set_model (Hs.GameBoyModel model) throws Error {
        var gb_core = core as Hs.GameBoyCore;
        gb_core.set_model (model);
    }

    public async void set_palette (int[] colors) throws Error {
        var gb_core = core as Hs.GameBoyCore;
        gb_core.set_palette (colors);
    }

    public async void set_show_sgb_borders (bool show_borders) throws Error {
        var gb_core = core as Hs.GameBoyCore;
        gb_core.set_show_sgb_borders (show_borders);
    }
}
