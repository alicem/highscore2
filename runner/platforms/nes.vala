// This file is part of Highscore. License: GPL-3.0+.

[DBus (name = "org.gnome.World.Highscore2.Runner")]
public class Highscore.Nes.Server : PlatformServer {
    public async uint get_players () throws Error {
        var nes_core = core as Hs.NesCore;
        return nes_core.get_players ();
    }

    public async bool get_has_mic () throws Error {
        var nes_core = core as Hs.NesCore;
        return nes_core.get_has_mic ();
    }

    public async Hs.NesAccessory get_accessory () throws Error {
        var nes_core = core as Hs.NesCore;
        return nes_core.get_accessory ();
    }
}
