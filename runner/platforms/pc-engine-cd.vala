// This file is part of Highscore. License: GPL-3.0+.

[DBus (name = "org.gnome.World.Highscore2.Runner")]
public class Highscore.PcEngineCd.Server : PlatformServer {
    public async void set_bios_path (string path) throws Error {
        var pce_cd_core = core as Hs.PcEngineCdCore;
        pce_cd_core.set_bios_path (path);
    }
}
