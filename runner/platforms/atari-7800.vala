// This file is part of Highscore. License: GPL-3.0+.

[DBus (name = "org.gnome.World.Highscore2.Runner")]
public class Highscore.Atari7800.Server : PlatformServer {
    public async Hs.Atari7800Controller get_controller (uint player) throws Error {
        var atari_core = core as Hs.Atari7800Core;
        return atari_core.get_controller (player);
    }

    public async Hs.Atari7800Difficulty get_default_difficulty (uint player) throws Error {
        var atari_core = core as Hs.Atari7800Core;
        return atari_core.get_default_difficulty (player);
    }

    public async void press_pause () throws Error {
        runner.schedule_adjust_input_state (state => {
            state->atari_7800.pause_button = true;
        });
    }

    public async void press_select () throws Error {
        runner.schedule_adjust_input_state (state => {
            state->atari_7800.select_button = true;
        });
    }

    public async void press_reset () throws Error {
        runner.schedule_adjust_input_state (state => {
            state->atari_7800.reset_button = true;
        });
    }
}
