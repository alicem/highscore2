// This file is part of Highscore. License: GPL-3.0+.

[DBus (name = "org.gnome.World.Highscore2.Runner")]
public class Highscore.Nintendo64.Server : PlatformServer {
    public async uint get_players () throws Error {
        var n64_core = core as Hs.Nintendo64Core;
        return n64_core.get_players ();
    }

    public async void set_controller_present (uint player, bool present) throws Error {
        var n64_core = core as Hs.Nintendo64Core;
        n64_core.set_controller_present (player, present);
    }
}
