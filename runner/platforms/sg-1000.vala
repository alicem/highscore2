// This file is part of Highscore. License: GPL-3.0+.

[DBus (name = "org.gnome.World.Highscore2.Runner")]
public class Highscore.Sg1000.Server : PlatformServer {
    public async void press_pause () throws Error {
        runner.schedule_adjust_input_state (state => {
            state->sg1000.pause_button = true;
        });
    }
}
