mime_types = [
  'application/vnd.nintendo.snes.rom',
  'application/x-cue',
  'application/x-fds-disk',
  'application/x-atari-2600-rom',
  'application/x-atari-7800-rom',
  'application/x-gameboy-rom',
  'application/x-gameboy-color-rom',
  'application/x-gamegear-rom',
  'application/x-gba-rom',
  'application/x-n64-rom',
  'application/x-neo-geo-pocket-rom',
  'application/x-neo-geo-pocket-color-rom',
  'application/x-nes-rom',
  'application/x-nintendo-ds-rom',
  'application/x-pc-engine-rom',
  'application/x-sms-rom',
  'application/x-sg1000-rom',
  'application/x-virtual-boy-rom',
  'application/x-wonderswan-rom',
  'application/x-wonderswan-color-rom',
]

desktop_conf = configuration_data()
desktop_conf.set('appid', application_id)
desktop_conf.set('mimetypes', ';'.join(mime_types) + ';')
desktop_file = i18n.merge_file(
  type: 'desktop',
  input: configure_file(
    input: files('org.gnome.World.Highscore2.desktop.in.in'),
    output: 'org.gnome.World.Highscore2.desktop.in',
    configuration: desktop_conf
  ),
  output: '@0@.desktop'.format(application_id),
  po_dir: '../po',
  install: true,
  install_dir: datadir / 'applications'
)

desktop_utils = find_program('desktop-file-validate', required: false)
if desktop_utils.found()
  test('Validate desktop file', desktop_utils, args: [desktop_file])
endif

metainfo_conf = configuration_data()
metainfo_conf.set('appid', application_id)
metainfo_file = i18n.merge_file(
  input: configure_file(
    input: files('org.gnome.World.Highscore2.metainfo.xml.in.in'),
    output: 'org.gnome.World.Highscore2.metainfo.xml.in',
    configuration: metainfo_conf
  ),
  output: '@0@.metainfo.xml'.format(application_id),
  po_dir: '../po',
  install: true,
  install_dir: datadir / 'metainfo'
)

appstreamcli = find_program('appstreamcli', required: false)
if appstreamcli.found()
  test('Validate appstream file', appstreamcli,
    args: ['validate', '--no-net', '--explain', metainfo_file]
  )
endif

install_data('org.gnome.World.Highscore2.gschema.xml',
  install_dir: datadir / 'glib-2.0'/ 'schemas'
)

compile_schemas = find_program('glib-compile-schemas', required: false)
if compile_schemas.found()
  test('Validate schema file',
       compile_schemas,
       args: ['--strict', '--dry-run', meson.current_source_dir()])
endif

service_conf = configuration_data()
service_conf.set('appid', application_id)
service_conf.set('bindir', bindir)
configure_file(
  input: 'org.gnome.World.Highscore2.service.in',
  output: '@0@.service'.format(application_id),
  configuration: service_conf,
  install_dir: datadir / 'dbus-1' / 'services'
)

subdir('icons')
subdir('platforms')