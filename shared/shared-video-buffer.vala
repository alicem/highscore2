// This file is part of Highscore. License: GPL-3.0+.

public class Highscore.SharedVideoBuffer : Object, FileDescriptorBased {
    private int fd;
    private void* data;
    private size_t size;

    private struct Metadata {
        PosixExt.Semaphore semaphore;
        size_t size;

        bool invalidated;

        Hs.PixelFormat format;
        Hs.Rectangle area;
        uint row_stride;

        double aspect_ratio;
        bool flipped;
    }

    private Metadata* metadata;
    private uint8* pixels;

#if RUNNER_PROCESS
    public SharedVideoBuffer () {
        fd = memfd_create ("[highscore-runner video buffer]");

        resize (0);

        if (metadata.semaphore.init (1, 1) != 0)
            critical ("Failed to init semaphore: %s", strerror (Posix.errno));
    }
#else
    public SharedVideoBuffer (UnixInputStream stream) {
        fd = unwrap_fd (stream);

        resize (0);
        resize (metadata.size);
    }
#endif

    ~SharedVideoBuffer () {
#if RUNNER_PROCESS
        if (metadata.semaphore.destroy () != 0)
            critical ("Failed to destroy semaphore: %s", strerror (Posix.errno));
#endif

        Posix.munmap (data, size);
        Posix.close (fd);
    }

#if RUNNER_PROCESS
    public void resize (size_t size) {
#else
    private void resize (size_t size) {
#endif
        size += sizeof (Metadata);

        if (data != null) {
            if (size == this.size)
                return;

            Posix.munmap (data, this.size);
            data = null;
        }

        this.size = size;

        if (Posix.ftruncate (fd, size) != 0)
            critical ("Failed to truncate framebuffer: %s", strerror (Posix.errno));

        data = Posix.mmap (
            null, size,
            Posix.PROT_READ | Posix.PROT_WRITE,
            Posix.MAP_SHARED,
            fd, 0
        );

        metadata = data;
        pixels = &data[sizeof (Metadata)];

        metadata.size = size;
    }

    public int get_fd () {
        return fd;
    }

    public void lock () {
        if (metadata.semaphore.wait () != 0)
            critical ("Failed to lock semaphore: %s", strerror (Posix.errno));
    }

    public void unlock () {
        if (metadata.semaphore.post () != 0)
            critical ("Failed to unlock semaphore: %s", strerror (Posix.errno));
    }

#if UI_PROCESS
    public Hs.PixelFormat get_pixel_format () {
        return metadata.format;
    }

    public Hs.Rectangle get_area () {
        return metadata.area;
    }

    public uint get_row_stride () {
        return metadata.row_stride;
    }

    public double get_aspect_ratio () {
        return metadata.aspect_ratio;
    }

    public bool get_flipped () {
        return metadata.flipped;
    }

    public bool get_invalidated () {
        return metadata.invalidated;
    }

    public bool is_empty () {
        return metadata.size <= sizeof (Metadata);
    }
#endif

    public uint8* get_pixels () {
#if UI_PROCESS
        resize (metadata.size);

        metadata.invalidated = false;
#endif

        return pixels;
    }

#if RUNNER_PROCESS
    public void set_frame_data (
        Hs.PixelFormat format,
        Hs.Rectangle area,
        uint row_stride,
        bool flipped
    ) {
        metadata.invalidated = true;

        metadata.format = format;
        metadata.area = area;
        metadata.row_stride = row_stride;
        metadata.flipped = flipped;
    }

    public void set_aspect_ratio (double aspect_ratio) {
        metadata.aspect_ratio = aspect_ratio;
    }
#endif
}
