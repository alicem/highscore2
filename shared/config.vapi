// This file is part of Highscore. License: GPL-3.0+.

[CCode (cprefix = "", lower_case_cprefix = "", cheader_filename = "config.h")]
namespace Config {
    public const string APPLICATION_ID;
    public const string GETTEXT_PACKAGE;
    public const string GNOMELOCALEDIR;
    public const string VERSION;
    public const string CORES_DIR;
    public const string RUNNER_PATH;
}
