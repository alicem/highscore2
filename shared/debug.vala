// This file is part of Highscore. License: GPL-3.0+.

namespace Highscore.Debug {
    [Flags]
    public enum Flags {
        BACKTRACE,
        VALGRIND;

        public string get_name () {
            switch (this) {
                case BACKTRACE:
                    return "backtrace";
                case VALGRIND:
                    return "valgrind";
                default:
                    assert_not_reached ();
            }
        }

        public string get_description () {
            switch (this) {
                case BACKTRACE:
                    return "Print backtrace when subprocess crashes";
                case VALGRIND:
                    return "Run subprocess in Valgrind";
                default:
                    assert_not_reached ();
            }
        }

        public static Flags? parse_name (string name) {
            if (name == "backtrace")
                return BACKTRACE;
            if (name == "valgrind")
                return VALGRIND;

            return null;
        }

        public static Flags[] all () {
            return { BACKTRACE, VALGRIND };
        }
    }

    private static Flags flags;

    public static Flags get_flags () {
        return flags;
    }

    public void parse (bool silent) {
        var value = Environment.get_variable ("HIGHSCORE_DEBUG");
        bool help = false;

        flags = 0;

        if (value == null)
            return;

        var names = value.split (":");

        foreach (var name in names) {
            if (name == "help") {
                help = true;
                continue;
            }

            var flag = Flags.parse_name (name);

            if (flag != null)
                flags |= flag;
            else if (!silent)
                print ("Unknown value: \"%s\". Try HIGHSCORE_DEBUG=help\n", name);
        }

        if (!silent && help) {
            print ("Supported HIGHSCORE_DEBUG values:\n");

            foreach (var flag in Flags.all ())
                print ("  %-15s %s\n", flag.get_name (), flag.get_description ());

            print ("\n");
            print ("Multiple values can be given, separated by :.\n");
        }
    }
}
